﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean Oculus.VoiceSDK.Utilities.MicPermissionsManager::HasMicPermission()
extern void MicPermissionsManager_HasMicPermission_mF31415D9C2357A82D606BB2161815E5B86A0D34E (void);
// 0x00000002 System.Void Oculus.VoiceSDK.Utilities.MicPermissionsManager::RequestMicPermission()
extern void MicPermissionsManager_RequestMicPermission_m27ACC103114D7C68F437B5EF0E50610F99C53B00 (void);
// 0x00000003 System.Void Oculus.VoiceSDK.Utilities.MicPermissionsManager::.ctor()
extern void MicPermissionsManager__ctor_m1DC02F90CF2013CBE0EC481F9B4037044DE5A1AE (void);
// 0x00000004 System.String[] Oculus.Voice.AppBuiltIns::get_appNames()
extern void AppBuiltIns_get_appNames_m2400F815FBF82542A9151B0F7ED41B717941980F (void);
// 0x00000005 System.Void Oculus.Voice.AppBuiltIns::.cctor()
extern void AppBuiltIns__cctor_m002731AF1E5342B85F76ADA724A895EB7F08D8C9 (void);
// 0x00000006 Facebook.WitAi.Configuration.WitRuntimeConfiguration Oculus.Voice.AppVoiceExperience::get_RuntimeConfiguration()
extern void AppVoiceExperience_get_RuntimeConfiguration_mB4BD9F2821001AC00FDD57A7DE07DF929B5C0FFE (void);
// 0x00000007 System.Void Oculus.Voice.AppVoiceExperience::set_RuntimeConfiguration(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
extern void AppVoiceExperience_set_RuntimeConfiguration_mC08DBCBFF981A22A7230D375215D0DB97FEDB564 (void);
// 0x00000008 System.Boolean Oculus.Voice.AppVoiceExperience::get_Initialized()
extern void AppVoiceExperience_get_Initialized_m99D44C4F745F16524F0CE216E24315E933D22749 (void);
// 0x00000009 System.Void Oculus.Voice.AppVoiceExperience::add_OnInitialized(System.Action)
extern void AppVoiceExperience_add_OnInitialized_m89F8922D26AE6489B734D2E07AC336E20A5761E2 (void);
// 0x0000000A System.Void Oculus.Voice.AppVoiceExperience::remove_OnInitialized(System.Action)
extern void AppVoiceExperience_remove_OnInitialized_m2DCB004DCBE16FCB1441F87173E338158907C343 (void);
// 0x0000000B System.Boolean Oculus.Voice.AppVoiceExperience::get_Active()
extern void AppVoiceExperience_get_Active_mC341C0F8F33BCE80E5DF3E7039BA2ACC8DCDBBF1 (void);
// 0x0000000C System.Boolean Oculus.Voice.AppVoiceExperience::get_IsRequestActive()
extern void AppVoiceExperience_get_IsRequestActive_m0F4A874DE2DB31E5777E4CC71C735D60DD03E8B9 (void);
// 0x0000000D Facebook.WitAi.Interfaces.ITranscriptionProvider Oculus.Voice.AppVoiceExperience::get_TranscriptionProvider()
extern void AppVoiceExperience_get_TranscriptionProvider_mE34032A7D3BB10A7355094ACEB892219E738F3B7 (void);
// 0x0000000E System.Void Oculus.Voice.AppVoiceExperience::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
extern void AppVoiceExperience_set_TranscriptionProvider_mC015A13DEF1A6660100D66BC4FD78732D3DD80BF (void);
// 0x0000000F System.Boolean Oculus.Voice.AppVoiceExperience::get_MicActive()
extern void AppVoiceExperience_get_MicActive_m1EBA5763B32286A9B966F8809632602C87E2DF3C (void);
// 0x00000010 System.Boolean Oculus.Voice.AppVoiceExperience::get_ShouldSendMicData()
extern void AppVoiceExperience_get_ShouldSendMicData_mA1E5CBF279586CAF065F2A4BE087A4E238DA2057 (void);
// 0x00000011 System.Boolean Oculus.Voice.AppVoiceExperience::get_HasPlatformIntegrations()
extern void AppVoiceExperience_get_HasPlatformIntegrations_m7882C9A275163FC894F0A863402C2C19FE9A2BB0 (void);
// 0x00000012 System.Boolean Oculus.Voice.AppVoiceExperience::get_EnableConsoleLogging()
extern void AppVoiceExperience_get_EnableConsoleLogging_m65A225774ABCE34CBC9CA6E7DFFCD54EE1F791C0 (void);
// 0x00000013 System.Boolean Oculus.Voice.AppVoiceExperience::get_UsePlatformIntegrations()
extern void AppVoiceExperience_get_UsePlatformIntegrations_mB759EC3C200BE883096375DE04E5D3CFC6FE717B (void);
// 0x00000014 System.Void Oculus.Voice.AppVoiceExperience::set_UsePlatformIntegrations(System.Boolean)
extern void AppVoiceExperience_set_UsePlatformIntegrations_m1CE44F6D1EBE5975C22B275B98CB924270ACDB6D (void);
// 0x00000015 System.Void Oculus.Voice.AppVoiceExperience::Activate(System.String,Facebook.WitAi.Configuration.WitRequestOptions)
extern void AppVoiceExperience_Activate_m5C13CD597662A86F3CB1AB47743FA438CCE880B3 (void);
// 0x00000016 System.Void Oculus.Voice.AppVoiceExperience::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
extern void AppVoiceExperience_Activate_m17BC2AA1B01B9A1B179963683F7C7B3A63F40950 (void);
// 0x00000017 System.Void Oculus.Voice.AppVoiceExperience::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
extern void AppVoiceExperience_ActivateImmediately_mCED409AC5C843151934F3FFEC3EF97D3FF2713A4 (void);
// 0x00000018 System.Void Oculus.Voice.AppVoiceExperience::Deactivate()
extern void AppVoiceExperience_Deactivate_m80E16AF383EF6D8F03E8F836A5BB6DE3EF0BDC20 (void);
// 0x00000019 System.Void Oculus.Voice.AppVoiceExperience::DeactivateAndAbortRequest()
extern void AppVoiceExperience_DeactivateAndAbortRequest_mFC7796C97D0878BD2D18244326622D31A53E12D2 (void);
// 0x0000001A System.Void Oculus.Voice.AppVoiceExperience::InitVoiceSDK()
extern void AppVoiceExperience_InitVoiceSDK_m4B20AF2EBAA151BF865DA52538E2C4007F9AFF40 (void);
// 0x0000001B System.Void Oculus.Voice.AppVoiceExperience::RevertToWitUnity()
extern void AppVoiceExperience_RevertToWitUnity_mC1C38663B0170469273299C774EC55ACC448E792 (void);
// 0x0000001C System.Void Oculus.Voice.AppVoiceExperience::OnEnable()
extern void AppVoiceExperience_OnEnable_mB25AD91F296CA2078BC2294CDCDC79D8E156476C (void);
// 0x0000001D System.Void Oculus.Voice.AppVoiceExperience::OnDisable()
extern void AppVoiceExperience_OnDisable_m057261B5023DBB79B7BA0DB4189E62202C25DE8C (void);
// 0x0000001E System.Void Oculus.Voice.AppVoiceExperience::OnApplicationFocus(System.Boolean)
extern void AppVoiceExperience_OnApplicationFocus_m4EBFD96602B9CBA62487C42D3B89D2C5540131E9 (void);
// 0x0000001F System.Void Oculus.Voice.AppVoiceExperience::OnWitResponseListener(Facebook.WitAi.Lib.WitResponseNode)
extern void AppVoiceExperience_OnWitResponseListener_m8FF178150BF62DFB15C70BC685D650C0C0FCC97C (void);
// 0x00000020 System.Void Oculus.Voice.AppVoiceExperience::OnAborted()
extern void AppVoiceExperience_OnAborted_mFA66619DFA1987C37F15CE5B6279A97DA8DD6132 (void);
// 0x00000021 System.Void Oculus.Voice.AppVoiceExperience::OnError(System.String,System.String)
extern void AppVoiceExperience_OnError_m2915637737BB897F5D77BEE8463C4D8477CB0547 (void);
// 0x00000022 System.Void Oculus.Voice.AppVoiceExperience::OnStartedListening()
extern void AppVoiceExperience_OnStartedListening_mF5048DFE735A18FDA6CBC2E29A603A23D8DE96B8 (void);
// 0x00000023 System.Void Oculus.Voice.AppVoiceExperience::OnStoppedListening()
extern void AppVoiceExperience_OnStoppedListening_m1591C1B0D3D616A7E8F3071A197D3916A3943353 (void);
// 0x00000024 System.Void Oculus.Voice.AppVoiceExperience::OnMicDataSent()
extern void AppVoiceExperience_OnMicDataSent_m8688FF5764B8FE7A40DFF05A67F3CBCEBDE3427B (void);
// 0x00000025 System.Void Oculus.Voice.AppVoiceExperience::.ctor()
extern void AppVoiceExperience__ctor_mBFB2447C24C3D149A0DB2A859DF485A8E8077201 (void);
// 0x00000026 System.Void Oculus.Voice.AppVoiceExperience::<InitVoiceSDK>b__38_0()
extern void AppVoiceExperience_U3CInitVoiceSDKU3Eb__38_0_m03CB7E5848E00A89ECEE69ED012C1D3A8A88FADE (void);
// 0x00000027 System.Boolean Oculus.Voice.Interfaces.IPlatformVoiceService::get_PlatformSupportsWit()
// 0x00000028 System.Void Oculus.Voice.Interfaces.IPlatformVoiceService::SetRuntimeConfiguration(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
// 0x00000029 System.Void Oculus.Voice.Bindings.Android.IVCBindingEvents::OnServiceNotAvailable(System.String,System.String)
// 0x0000002A System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::.ctor(UnityEngine.AndroidJavaObject)
extern void VoiceSDKBinding__ctor_m0C5F7509B3025BC053FB16F3FAA968E76824152A (void);
// 0x0000002B System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKBinding::get_Active()
extern void VoiceSDKBinding_get_Active_m09D7CDA4F56323048B65074DB987AA46416CE34D (void);
// 0x0000002C System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKBinding::get_IsRequestActive()
extern void VoiceSDKBinding_get_IsRequestActive_mE270A11A52C1AD4D3CA4A55F7B7E330AA1247342 (void);
// 0x0000002D System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKBinding::get_MicActive()
extern void VoiceSDKBinding_get_MicActive_m45CD8895B212648D801574C20FA76E4C6651E9EC (void);
// 0x0000002E System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKBinding::get_PlatformSupportsWit()
extern void VoiceSDKBinding_get_PlatformSupportsWit_m9E6893583E603941B7351E1008F660C72D515F65 (void);
// 0x0000002F System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::Activate(System.String,Facebook.WitAi.Configuration.WitRequestOptions)
extern void VoiceSDKBinding_Activate_m72DCA2C1A074C91870E499C18F4D75E0BAAD0B1F (void);
// 0x00000030 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
extern void VoiceSDKBinding_Activate_m02C8A8FB3E4F5B06659BF4F19B1F450D7D72424B (void);
// 0x00000031 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
extern void VoiceSDKBinding_ActivateImmediately_m97DE475C9FD0E91BB40272DEB5CCE48EB894C55D (void);
// 0x00000032 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::Deactivate()
extern void VoiceSDKBinding_Deactivate_m7941512C3434A6F4E5AC62A749705AFEE9CECA3F (void);
// 0x00000033 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::DeactivateAndAbortRequest()
extern void VoiceSDKBinding_DeactivateAndAbortRequest_mF57E46AB57DC176E2B45331CC86AEA2BAE380CAD (void);
// 0x00000034 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::SetRuntimeConfiguration(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
extern void VoiceSDKBinding_SetRuntimeConfiguration_mC868CD77571B7F15D1DBA21D550409420521B113 (void);
// 0x00000035 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::SetListener(Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding)
extern void VoiceSDKBinding_SetListener_m7488CC22AAD677683BE9D7D18F6A240CB25D0C80 (void);
// 0x00000036 System.Void Oculus.Voice.Bindings.Android.VoiceSDKBinding::Connect()
extern void VoiceSDKBinding_Connect_mC829E36ABC9F3BA19B2B931B4880FE730ECD5491 (void);
// 0x00000037 System.Void Oculus.Voice.Bindings.Android.VoiceSDKConfigBinding::.ctor(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
extern void VoiceSDKConfigBinding__ctor_mB1CC05F3191CD15BCE36790CFD1134D349CC3493 (void);
// 0x00000038 UnityEngine.AndroidJavaObject Oculus.Voice.Bindings.Android.VoiceSDKConfigBinding::ToJavaObject()
extern void VoiceSDKConfigBinding_ToJavaObject_m865C74F03FD60D025168D6EDEB15885216F971E6 (void);
// 0x00000039 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::.ctor(Facebook.WitAi.IVoiceService)
extern void VoiceSDKImpl__ctor_mB6AA37992649188E2A461D61888A30D91D3A7982 (void);
// 0x0000003A System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKImpl::get_PlatformSupportsWit()
extern void VoiceSDKImpl_get_PlatformSupportsWit_mDF8BC259EA908FBB9C028FBDC1DA0FA1DC0674B1 (void);
// 0x0000003B System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKImpl::get_Active()
extern void VoiceSDKImpl_get_Active_mF49C0FD2D3FE0347A58F095FFF1BBD9D30B5450F (void);
// 0x0000003C System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKImpl::get_IsRequestActive()
extern void VoiceSDKImpl_get_IsRequestActive_m7AEEA12A5EFA94B10D37C7CC04E0C367799A3431 (void);
// 0x0000003D System.Boolean Oculus.Voice.Bindings.Android.VoiceSDKImpl::get_MicActive()
extern void VoiceSDKImpl_get_MicActive_mEE6E3E1CCD9D482D6E13BC2DAE29DAC509823BEA (void);
// 0x0000003E System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::SetRuntimeConfiguration(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
extern void VoiceSDKImpl_SetRuntimeConfiguration_mCDD3F6D82C6AE5E2EF91CB79D1F23FDC9D3BFCE5 (void);
// 0x0000003F Facebook.WitAi.Interfaces.ITranscriptionProvider Oculus.Voice.Bindings.Android.VoiceSDKImpl::get_TranscriptionProvider()
extern void VoiceSDKImpl_get_TranscriptionProvider_m821A9CD25178705F8C6110A7D350D9DBAEB12D78 (void);
// 0x00000040 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
extern void VoiceSDKImpl_set_TranscriptionProvider_m1F4A51974462FA7DC7911236FCA756F311D7595F (void);
// 0x00000041 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::Connect(System.String)
extern void VoiceSDKImpl_Connect_mA6EDBD167CF613E3693EBEE7AF83154147291B11 (void);
// 0x00000042 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::Disconnect()
extern void VoiceSDKImpl_Disconnect_mEAC841BE821EF84B7EF868E27367FB242C42003B (void);
// 0x00000043 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::OnStoppedListening()
extern void VoiceSDKImpl_OnStoppedListening_m6D465BB919DC89C3A509127EAB13908D24ACC042 (void);
// 0x00000044 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::Activate(System.String,Facebook.WitAi.Configuration.WitRequestOptions)
extern void VoiceSDKImpl_Activate_m405EF29B013BFCBD597ED8FAC8C538F7FE157BE3 (void);
// 0x00000045 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
extern void VoiceSDKImpl_Activate_mFAE12AD47E12996D1D767D818E1570AF2D2F103B (void);
// 0x00000046 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
extern void VoiceSDKImpl_ActivateImmediately_mE13BFC633BB07ABB0F1A78EAFECD24E70E7F0D8C (void);
// 0x00000047 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::Deactivate()
extern void VoiceSDKImpl_Deactivate_mDC883CD677AE860FF624DCC2A9D0FFD0819391B4 (void);
// 0x00000048 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::DeactivateAndAbortRequest()
extern void VoiceSDKImpl_DeactivateAndAbortRequest_m85B3E5E1004F896D2A7763F933DAD4C5B168DB99 (void);
// 0x00000049 System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::OnServiceNotAvailable(System.String,System.String)
extern void VoiceSDKImpl_OnServiceNotAvailable_mB188F8EA2CE5BE0A30C4498E3C4404C9ED39D72E (void);
// 0x0000004A Facebook.WitAi.Events.VoiceEvents Oculus.Voice.Bindings.Android.VoiceSDKImpl::get_VoiceEvents()
extern void VoiceSDKImpl_get_VoiceEvents_mEB15B3E14800F28C6E996CCD01C9266097742B19 (void);
// 0x0000004B System.Void Oculus.Voice.Bindings.Android.VoiceSDKImpl::set_VoiceEvents(Facebook.WitAi.Events.VoiceEvents)
extern void VoiceSDKImpl_set_VoiceEvents_mFD9DB971A296C8317D30F34630C2EFFDA154C9DF (void);
// 0x0000004C Facebook.WitAi.Events.VoiceEvents Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::get_VoiceEvents()
extern void VoiceSDKListenerBinding_get_VoiceEvents_m4326A578DC78A053883704536826582A6BA389DA (void);
// 0x0000004D System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::.ctor(Facebook.WitAi.IVoiceService,Oculus.Voice.Bindings.Android.IVCBindingEvents)
extern void VoiceSDKListenerBinding__ctor_mDA0E4C22830921482F8CEAB2B978EFECDAFE8F15 (void);
// 0x0000004E System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onResponse(System.String)
extern void VoiceSDKListenerBinding_onResponse_m2272D0B78526424993A5A996725011AE0ADA4A45 (void);
// 0x0000004F System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onPartialResponse(System.String)
extern void VoiceSDKListenerBinding_onPartialResponse_mFFE64E3EFDCEF67B421EC7A1EE2294CE8DEC3FBD (void);
// 0x00000050 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onError(System.String,System.String,System.String)
extern void VoiceSDKListenerBinding_onError_mE7535E7E934B976AB0C8473043D136A8A74697D4 (void);
// 0x00000051 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onAborted()
extern void VoiceSDKListenerBinding_onAborted_m191CD839F75D9CE699784D50D128FD257B5DD066 (void);
// 0x00000052 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onRequestCompleted()
extern void VoiceSDKListenerBinding_onRequestCompleted_m1B3BFADC48CBD48F90B810A3A1A8F794E32CA2BA (void);
// 0x00000053 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onMicLevelChanged(System.Single)
extern void VoiceSDKListenerBinding_onMicLevelChanged_mA4349AF78736CE24A1B51F58153101CE7EF5D7F3 (void);
// 0x00000054 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onRequestCreated()
extern void VoiceSDKListenerBinding_onRequestCreated_m1C33AD7D1CC2AA1129982B98353B2343BA6A473E (void);
// 0x00000055 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onStartListening()
extern void VoiceSDKListenerBinding_onStartListening_m523E1CC754E8B53CA05905FB3330706F5B8D672F (void);
// 0x00000056 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onStoppedListening(System.Int32)
extern void VoiceSDKListenerBinding_onStoppedListening_mF14FC489E0969A665AE8C87DD003779F7B38FABD (void);
// 0x00000057 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onMicDataSent()
extern void VoiceSDKListenerBinding_onMicDataSent_m1E0452B60223AB9EDF966EDA1294D3F700A39619 (void);
// 0x00000058 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onMinimumWakeThresholdHit()
extern void VoiceSDKListenerBinding_onMinimumWakeThresholdHit_mB69ABF4066BFDE473434E0ED207516B95C087B8B (void);
// 0x00000059 System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onPartialTranscription(System.String)
extern void VoiceSDKListenerBinding_onPartialTranscription_mCEB971BD66D82F4ED0B76092A8DF98AD89FD165E (void);
// 0x0000005A System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onFullTranscription(System.String)
extern void VoiceSDKListenerBinding_onFullTranscription_m8AB08997A1E6C934848309012808437FC4CCAA03 (void);
// 0x0000005B System.Void Oculus.Voice.Bindings.Android.VoiceSDKListenerBinding::onServiceNotAvailable(System.String,System.String)
extern void VoiceSDKListenerBinding_onServiceNotAvailable_m73F6D98B4DFF0817F4774DF1D4B50C76B43E094F (void);
static Il2CppMethodPointer s_methodPointers[91] = 
{
	MicPermissionsManager_HasMicPermission_mF31415D9C2357A82D606BB2161815E5B86A0D34E,
	MicPermissionsManager_RequestMicPermission_m27ACC103114D7C68F437B5EF0E50610F99C53B00,
	MicPermissionsManager__ctor_m1DC02F90CF2013CBE0EC481F9B4037044DE5A1AE,
	AppBuiltIns_get_appNames_m2400F815FBF82542A9151B0F7ED41B717941980F,
	AppBuiltIns__cctor_m002731AF1E5342B85F76ADA724A895EB7F08D8C9,
	AppVoiceExperience_get_RuntimeConfiguration_mB4BD9F2821001AC00FDD57A7DE07DF929B5C0FFE,
	AppVoiceExperience_set_RuntimeConfiguration_mC08DBCBFF981A22A7230D375215D0DB97FEDB564,
	AppVoiceExperience_get_Initialized_m99D44C4F745F16524F0CE216E24315E933D22749,
	AppVoiceExperience_add_OnInitialized_m89F8922D26AE6489B734D2E07AC336E20A5761E2,
	AppVoiceExperience_remove_OnInitialized_m2DCB004DCBE16FCB1441F87173E338158907C343,
	AppVoiceExperience_get_Active_mC341C0F8F33BCE80E5DF3E7039BA2ACC8DCDBBF1,
	AppVoiceExperience_get_IsRequestActive_m0F4A874DE2DB31E5777E4CC71C735D60DD03E8B9,
	AppVoiceExperience_get_TranscriptionProvider_mE34032A7D3BB10A7355094ACEB892219E738F3B7,
	AppVoiceExperience_set_TranscriptionProvider_mC015A13DEF1A6660100D66BC4FD78732D3DD80BF,
	AppVoiceExperience_get_MicActive_m1EBA5763B32286A9B966F8809632602C87E2DF3C,
	AppVoiceExperience_get_ShouldSendMicData_mA1E5CBF279586CAF065F2A4BE087A4E238DA2057,
	AppVoiceExperience_get_HasPlatformIntegrations_m7882C9A275163FC894F0A863402C2C19FE9A2BB0,
	AppVoiceExperience_get_EnableConsoleLogging_m65A225774ABCE34CBC9CA6E7DFFCD54EE1F791C0,
	AppVoiceExperience_get_UsePlatformIntegrations_mB759EC3C200BE883096375DE04E5D3CFC6FE717B,
	AppVoiceExperience_set_UsePlatformIntegrations_m1CE44F6D1EBE5975C22B275B98CB924270ACDB6D,
	AppVoiceExperience_Activate_m5C13CD597662A86F3CB1AB47743FA438CCE880B3,
	AppVoiceExperience_Activate_m17BC2AA1B01B9A1B179963683F7C7B3A63F40950,
	AppVoiceExperience_ActivateImmediately_mCED409AC5C843151934F3FFEC3EF97D3FF2713A4,
	AppVoiceExperience_Deactivate_m80E16AF383EF6D8F03E8F836A5BB6DE3EF0BDC20,
	AppVoiceExperience_DeactivateAndAbortRequest_mFC7796C97D0878BD2D18244326622D31A53E12D2,
	AppVoiceExperience_InitVoiceSDK_m4B20AF2EBAA151BF865DA52538E2C4007F9AFF40,
	AppVoiceExperience_RevertToWitUnity_mC1C38663B0170469273299C774EC55ACC448E792,
	AppVoiceExperience_OnEnable_mB25AD91F296CA2078BC2294CDCDC79D8E156476C,
	AppVoiceExperience_OnDisable_m057261B5023DBB79B7BA0DB4189E62202C25DE8C,
	AppVoiceExperience_OnApplicationFocus_m4EBFD96602B9CBA62487C42D3B89D2C5540131E9,
	AppVoiceExperience_OnWitResponseListener_m8FF178150BF62DFB15C70BC685D650C0C0FCC97C,
	AppVoiceExperience_OnAborted_mFA66619DFA1987C37F15CE5B6279A97DA8DD6132,
	AppVoiceExperience_OnError_m2915637737BB897F5D77BEE8463C4D8477CB0547,
	AppVoiceExperience_OnStartedListening_mF5048DFE735A18FDA6CBC2E29A603A23D8DE96B8,
	AppVoiceExperience_OnStoppedListening_m1591C1B0D3D616A7E8F3071A197D3916A3943353,
	AppVoiceExperience_OnMicDataSent_m8688FF5764B8FE7A40DFF05A67F3CBCEBDE3427B,
	AppVoiceExperience__ctor_mBFB2447C24C3D149A0DB2A859DF485A8E8077201,
	AppVoiceExperience_U3CInitVoiceSDKU3Eb__38_0_m03CB7E5848E00A89ECEE69ED012C1D3A8A88FADE,
	NULL,
	NULL,
	NULL,
	VoiceSDKBinding__ctor_m0C5F7509B3025BC053FB16F3FAA968E76824152A,
	VoiceSDKBinding_get_Active_m09D7CDA4F56323048B65074DB987AA46416CE34D,
	VoiceSDKBinding_get_IsRequestActive_mE270A11A52C1AD4D3CA4A55F7B7E330AA1247342,
	VoiceSDKBinding_get_MicActive_m45CD8895B212648D801574C20FA76E4C6651E9EC,
	VoiceSDKBinding_get_PlatformSupportsWit_m9E6893583E603941B7351E1008F660C72D515F65,
	VoiceSDKBinding_Activate_m72DCA2C1A074C91870E499C18F4D75E0BAAD0B1F,
	VoiceSDKBinding_Activate_m02C8A8FB3E4F5B06659BF4F19B1F450D7D72424B,
	VoiceSDKBinding_ActivateImmediately_m97DE475C9FD0E91BB40272DEB5CCE48EB894C55D,
	VoiceSDKBinding_Deactivate_m7941512C3434A6F4E5AC62A749705AFEE9CECA3F,
	VoiceSDKBinding_DeactivateAndAbortRequest_mF57E46AB57DC176E2B45331CC86AEA2BAE380CAD,
	VoiceSDKBinding_SetRuntimeConfiguration_mC868CD77571B7F15D1DBA21D550409420521B113,
	VoiceSDKBinding_SetListener_m7488CC22AAD677683BE9D7D18F6A240CB25D0C80,
	VoiceSDKBinding_Connect_mC829E36ABC9F3BA19B2B931B4880FE730ECD5491,
	VoiceSDKConfigBinding__ctor_mB1CC05F3191CD15BCE36790CFD1134D349CC3493,
	VoiceSDKConfigBinding_ToJavaObject_m865C74F03FD60D025168D6EDEB15885216F971E6,
	VoiceSDKImpl__ctor_mB6AA37992649188E2A461D61888A30D91D3A7982,
	VoiceSDKImpl_get_PlatformSupportsWit_mDF8BC259EA908FBB9C028FBDC1DA0FA1DC0674B1,
	VoiceSDKImpl_get_Active_mF49C0FD2D3FE0347A58F095FFF1BBD9D30B5450F,
	VoiceSDKImpl_get_IsRequestActive_m7AEEA12A5EFA94B10D37C7CC04E0C367799A3431,
	VoiceSDKImpl_get_MicActive_mEE6E3E1CCD9D482D6E13BC2DAE29DAC509823BEA,
	VoiceSDKImpl_SetRuntimeConfiguration_mCDD3F6D82C6AE5E2EF91CB79D1F23FDC9D3BFCE5,
	VoiceSDKImpl_get_TranscriptionProvider_m821A9CD25178705F8C6110A7D350D9DBAEB12D78,
	VoiceSDKImpl_set_TranscriptionProvider_m1F4A51974462FA7DC7911236FCA756F311D7595F,
	VoiceSDKImpl_Connect_mA6EDBD167CF613E3693EBEE7AF83154147291B11,
	VoiceSDKImpl_Disconnect_mEAC841BE821EF84B7EF868E27367FB242C42003B,
	VoiceSDKImpl_OnStoppedListening_m6D465BB919DC89C3A509127EAB13908D24ACC042,
	VoiceSDKImpl_Activate_m405EF29B013BFCBD597ED8FAC8C538F7FE157BE3,
	VoiceSDKImpl_Activate_mFAE12AD47E12996D1D767D818E1570AF2D2F103B,
	VoiceSDKImpl_ActivateImmediately_mE13BFC633BB07ABB0F1A78EAFECD24E70E7F0D8C,
	VoiceSDKImpl_Deactivate_mDC883CD677AE860FF624DCC2A9D0FFD0819391B4,
	VoiceSDKImpl_DeactivateAndAbortRequest_m85B3E5E1004F896D2A7763F933DAD4C5B168DB99,
	VoiceSDKImpl_OnServiceNotAvailable_mB188F8EA2CE5BE0A30C4498E3C4404C9ED39D72E,
	VoiceSDKImpl_get_VoiceEvents_mEB15B3E14800F28C6E996CCD01C9266097742B19,
	VoiceSDKImpl_set_VoiceEvents_mFD9DB971A296C8317D30F34630C2EFFDA154C9DF,
	VoiceSDKListenerBinding_get_VoiceEvents_m4326A578DC78A053883704536826582A6BA389DA,
	VoiceSDKListenerBinding__ctor_mDA0E4C22830921482F8CEAB2B978EFECDAFE8F15,
	VoiceSDKListenerBinding_onResponse_m2272D0B78526424993A5A996725011AE0ADA4A45,
	VoiceSDKListenerBinding_onPartialResponse_mFFE64E3EFDCEF67B421EC7A1EE2294CE8DEC3FBD,
	VoiceSDKListenerBinding_onError_mE7535E7E934B976AB0C8473043D136A8A74697D4,
	VoiceSDKListenerBinding_onAborted_m191CD839F75D9CE699784D50D128FD257B5DD066,
	VoiceSDKListenerBinding_onRequestCompleted_m1B3BFADC48CBD48F90B810A3A1A8F794E32CA2BA,
	VoiceSDKListenerBinding_onMicLevelChanged_mA4349AF78736CE24A1B51F58153101CE7EF5D7F3,
	VoiceSDKListenerBinding_onRequestCreated_m1C33AD7D1CC2AA1129982B98353B2343BA6A473E,
	VoiceSDKListenerBinding_onStartListening_m523E1CC754E8B53CA05905FB3330706F5B8D672F,
	VoiceSDKListenerBinding_onStoppedListening_mF14FC489E0969A665AE8C87DD003779F7B38FABD,
	VoiceSDKListenerBinding_onMicDataSent_m1E0452B60223AB9EDF966EDA1294D3F700A39619,
	VoiceSDKListenerBinding_onMinimumWakeThresholdHit_mB69ABF4066BFDE473434E0ED207516B95C087B8B,
	VoiceSDKListenerBinding_onPartialTranscription_mCEB971BD66D82F4ED0B76092A8DF98AD89FD165E,
	VoiceSDKListenerBinding_onFullTranscription_m8AB08997A1E6C934848309012808437FC4CCAA03,
	VoiceSDKListenerBinding_onServiceNotAvailable_m73F6D98B4DFF0817F4774DF1D4B50C76B43E094F,
};
static const int32_t s_InvokerIndices[91] = 
{
	5637,
	5676,
	3709,
	5654,
	5676,
	3645,
	3049,
	3583,
	3049,
	3049,
	3583,
	3583,
	3645,
	3049,
	3583,
	3583,
	3583,
	3583,
	3583,
	2980,
	1864,
	3049,
	3049,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	2980,
	3049,
	3709,
	1864,
	3709,
	3709,
	3709,
	3709,
	3709,
	3583,
	3049,
	1864,
	3049,
	3583,
	3583,
	3583,
	3583,
	1864,
	3049,
	3049,
	3709,
	3709,
	3049,
	3049,
	3709,
	3049,
	3645,
	3049,
	3583,
	3583,
	3583,
	3583,
	3049,
	3645,
	3049,
	3049,
	3709,
	3709,
	1864,
	3049,
	3049,
	3709,
	3709,
	1864,
	3645,
	3049,
	3645,
	1864,
	3049,
	3049,
	1204,
	3709,
	3709,
	3079,
	3709,
	3709,
	3027,
	3709,
	3709,
	3049,
	3049,
	1864,
};
extern const CustomAttributesCacheGenerator g_VoiceSDK_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_VoiceSDK_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_VoiceSDK_Runtime_CodeGenModule = 
{
	"VoiceSDK.Runtime.dll",
	91,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_VoiceSDK_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
