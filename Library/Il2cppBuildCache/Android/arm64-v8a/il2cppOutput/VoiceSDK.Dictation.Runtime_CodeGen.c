﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Vector2 Facebook.WitAi.Configuration.WitDictationRuntimeConfiguration::get_RecordingTimeRange()
extern void WitDictationRuntimeConfiguration_get_RecordingTimeRange_m589BD3D30882A933211494FE2B6B4B1B9532E8BC (void);
// 0x00000002 System.Void Facebook.WitAi.Configuration.WitDictationRuntimeConfiguration::.ctor()
extern void WitDictationRuntimeConfiguration__ctor_m37330A6D660400B0504D8630067DFBDB538FE191 (void);
// 0x00000003 System.Boolean Oculus.VoiceSDK.Dictation.Utilities.MicPermissionsManager::HasMicPermission()
extern void MicPermissionsManager_HasMicPermission_mAA332F4C5D23B9A5DBEDDF9A9FAEC0AC07753EFE (void);
// 0x00000004 System.Void Oculus.VoiceSDK.Dictation.Utilities.MicPermissionsManager::RequestMicPermission()
extern void MicPermissionsManager_RequestMicPermission_m5B1988DF51EC8CCACBB1769B3F99045B3D485466 (void);
// 0x00000005 System.Void Oculus.VoiceSDK.Dictation.Utilities.MicPermissionsManager::.ctor()
extern void MicPermissionsManager__ctor_mBF3E5998D6F9745D00A718A4B9C36F8E5D709F38 (void);
// 0x00000006 Facebook.WitAi.Configuration.WitDictationRuntimeConfiguration Oculus.Voice.Dictation.AppDictationExperience::get_RuntimeConfiguration()
extern void AppDictationExperience_get_RuntimeConfiguration_m548E09CAB17ABCC38BF4209F8FADB8E3E7BDDBE7 (void);
// 0x00000007 System.Void Oculus.Voice.Dictation.AppDictationExperience::set_RuntimeConfiguration(Facebook.WitAi.Configuration.WitDictationRuntimeConfiguration)
extern void AppDictationExperience_set_RuntimeConfiguration_m7CBB89E858D1837D9FF89C5AE95D023F889FCDD6 (void);
// 0x00000008 System.Void Oculus.Voice.Dictation.AppDictationExperience::add_OnInitialized(System.Action)
extern void AppDictationExperience_add_OnInitialized_m16968932EF12E3046A6943A3DB8CAE3D4A892AAA (void);
// 0x00000009 System.Void Oculus.Voice.Dictation.AppDictationExperience::remove_OnInitialized(System.Action)
extern void AppDictationExperience_remove_OnInitialized_m4C905D5B608AA21630F9D85400E8AA1AFF60FAB3 (void);
// 0x0000000A System.Boolean Oculus.Voice.Dictation.AppDictationExperience::get_HasPlatformIntegrations()
extern void AppDictationExperience_get_HasPlatformIntegrations_mE0A6F3711A843B074FF2FF29C6BD32165B681F6F (void);
// 0x0000000B System.Boolean Oculus.Voice.Dictation.AppDictationExperience::get_UsePlatformIntegrations()
extern void AppDictationExperience_get_UsePlatformIntegrations_m69582728E773426BE12D8FCBF12C017D77321D84 (void);
// 0x0000000C System.Void Oculus.Voice.Dictation.AppDictationExperience::set_UsePlatformIntegrations(System.Boolean)
extern void AppDictationExperience_set_UsePlatformIntegrations_m84324F3672DFC83EF0E69FC66E549BFF2E798CB2 (void);
// 0x0000000D System.Void Oculus.Voice.Dictation.AppDictationExperience::InitDictation()
extern void AppDictationExperience_InitDictation_mA21FBBADFB78C8B19A6291BA50CEBEB228E243C3 (void);
// 0x0000000E System.Void Oculus.Voice.Dictation.AppDictationExperience::RevertToWitDictation()
extern void AppDictationExperience_RevertToWitDictation_mDD0AE20C95BB6C10B1E19C9BA8A5B8C5669D18A0 (void);
// 0x0000000F System.Void Oculus.Voice.Dictation.AppDictationExperience::OnEnable()
extern void AppDictationExperience_OnEnable_m874CCF1265ECDB4457CF9DBF5032E5BD57B3B05B (void);
// 0x00000010 System.Void Oculus.Voice.Dictation.AppDictationExperience::OnDisable()
extern void AppDictationExperience_OnDisable_m4CF196D898CFDAD5426A4F88777B493D69752A41 (void);
// 0x00000011 System.Boolean Oculus.Voice.Dictation.AppDictationExperience::get_Active()
extern void AppDictationExperience_get_Active_mF872FEFF9B5153607E58A9EDDFF51B0362D7DFE1 (void);
// 0x00000012 System.Boolean Oculus.Voice.Dictation.AppDictationExperience::get_IsRequestActive()
extern void AppDictationExperience_get_IsRequestActive_m2FFDB2C89024FCCFC05B4CDE8960363D5B30D32E (void);
// 0x00000013 Facebook.WitAi.Interfaces.ITranscriptionProvider Oculus.Voice.Dictation.AppDictationExperience::get_TranscriptionProvider()
extern void AppDictationExperience_get_TranscriptionProvider_mB482F4DA4834D8B8C88A610AE18D45146F359065 (void);
// 0x00000014 System.Void Oculus.Voice.Dictation.AppDictationExperience::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
extern void AppDictationExperience_set_TranscriptionProvider_m7ED865F96F35D6AF79FBFDFE342D825342A9E3D3 (void);
// 0x00000015 System.Boolean Oculus.Voice.Dictation.AppDictationExperience::get_MicActive()
extern void AppDictationExperience_get_MicActive_m47193B2AEA4C6084454D57113EC4062152FC5F5E (void);
// 0x00000016 System.Boolean Oculus.Voice.Dictation.AppDictationExperience::get_ShouldSendMicData()
extern void AppDictationExperience_get_ShouldSendMicData_m17A16FD16747EF15FE5E55C883097194DBB7ED09 (void);
// 0x00000017 System.Void Oculus.Voice.Dictation.AppDictationExperience::Activate()
extern void AppDictationExperience_Activate_mFE634E059096594F1BA54E1846FFA3945A447ABD (void);
// 0x00000018 System.Void Oculus.Voice.Dictation.AppDictationExperience::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
extern void AppDictationExperience_Activate_m39FAC7E22C27AF5E71093B7D04AB95DF0DE59A6C (void);
// 0x00000019 System.Void Oculus.Voice.Dictation.AppDictationExperience::ActivateImmediately()
extern void AppDictationExperience_ActivateImmediately_mA7C84C7CEFC41EBC2F7EA1F4A8BBA01DE96FD165 (void);
// 0x0000001A System.Void Oculus.Voice.Dictation.AppDictationExperience::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
extern void AppDictationExperience_ActivateImmediately_m22B7F73B229BE7F493F6AE6F131E49D26A24C2BB (void);
// 0x0000001B System.Void Oculus.Voice.Dictation.AppDictationExperience::Deactivate()
extern void AppDictationExperience_Deactivate_m95274EF71D05DCF2A445B95DEAF41D3620D04B6A (void);
// 0x0000001C System.Void Oculus.Voice.Dictation.AppDictationExperience::Cancel()
extern void AppDictationExperience_Cancel_m4A4004638259887EFF3A14F7FF52C7B754D73E96 (void);
// 0x0000001D System.Void Oculus.Voice.Dictation.AppDictationExperience::OnWitResponseListener(Facebook.WitAi.Lib.WitResponseNode)
extern void AppDictationExperience_OnWitResponseListener_m296041F73D8182B7D2B51DC485A9DBE0070EF0A2 (void);
// 0x0000001E System.Void Oculus.Voice.Dictation.AppDictationExperience::OnError(System.String,System.String)
extern void AppDictationExperience_OnError_m2EC8C9553C97D12486713877FF1B77AD3DA42ECE (void);
// 0x0000001F System.Void Oculus.Voice.Dictation.AppDictationExperience::OnStarted()
extern void AppDictationExperience_OnStarted_mC12266EE60C391CC5CF65CE5D2F8E1C89B6B1F5C (void);
// 0x00000020 System.Void Oculus.Voice.Dictation.AppDictationExperience::OnStopped()
extern void AppDictationExperience_OnStopped_mA46807BB912E469942C5717CE6ED86FE4CD07C94 (void);
// 0x00000021 System.Void Oculus.Voice.Dictation.AppDictationExperience::OnDictationSessionStarted(Facebook.WitAi.Dictation.Data.DictationSession)
extern void AppDictationExperience_OnDictationSessionStarted_m2AEC3F18E4D787A943E92B25C6E3A387D08153BA (void);
// 0x00000022 System.Void Oculus.Voice.Dictation.AppDictationExperience::.ctor()
extern void AppDictationExperience__ctor_mA66C605EF4E256875B633E1646252A09F2A01EAB (void);
// 0x00000023 System.Void Oculus.Voice.Dictation.Bindings.Android.DictationConfigurationBinding::.ctor(Facebook.WitAi.Configuration.WitDictationRuntimeConfiguration)
extern void DictationConfigurationBinding__ctor_m528B003C2FAE6E34DC78018C5B4526C5BCD74A5F (void);
// 0x00000024 UnityEngine.AndroidJavaObject Oculus.Voice.Dictation.Bindings.Android.DictationConfigurationBinding::ToJavaObject()
extern void DictationConfigurationBinding_ToJavaObject_m6075DF79CE6D955C4194D256AFC36F048481B541 (void);
// 0x00000025 Facebook.WitAi.Dictation.Events.DictationEvents Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::get_DictationEvents()
extern void DictationListenerBinding_get_DictationEvents_mA12FCFAAB0F9081AC168CF0E7DF3774A7A722849 (void);
// 0x00000026 System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::.ctor(Facebook.WitAi.Dictation.IDictationService,Oculus.Voice.Dictation.Bindings.Android.IServiceEvents)
extern void DictationListenerBinding__ctor_m57C72936ACA71A05D29F578402388AFD5B24501E (void);
// 0x00000027 System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onStart(System.String)
extern void DictationListenerBinding_onStart_mDFFD8B2A7F8DA35C2A8CC77BED4CAEFA1040274F (void);
// 0x00000028 System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onMicAudioLevel(System.String,System.Int32)
extern void DictationListenerBinding_onMicAudioLevel_m7A3006107FC73C03238D816479BF5A3058F89F38 (void);
// 0x00000029 System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onPartialTranscription(System.String,System.String)
extern void DictationListenerBinding_onPartialTranscription_m4C6F92B2851B8CDA9269E4A603B4D12ADF26B645 (void);
// 0x0000002A System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onFinalTranscription(System.String,System.String)
extern void DictationListenerBinding_onFinalTranscription_mC4F30479D61F5E57AF844B5425478CB8ADB5B5DA (void);
// 0x0000002B System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onError(System.String,System.String,System.String)
extern void DictationListenerBinding_onError_mA4602039E55579CBC8E6776C6F8D575FE749296D (void);
// 0x0000002C System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onStopped(System.String)
extern void DictationListenerBinding_onStopped_mAEEE15ECB1590148AC0A5E7C08D502B57D1ABE77 (void);
// 0x0000002D System.Void Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding::onServiceNotAvailable(System.String,System.String)
extern void DictationListenerBinding_onServiceNotAvailable_m2535CEF04067114D2D4AFF21455C4837453A4FE2 (void);
// 0x0000002E System.Void Oculus.Voice.Dictation.Bindings.Android.IServiceEvents::OnServiceNotAvailable(System.String,System.String)
// 0x0000002F System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::.ctor(Facebook.WitAi.Dictation.IDictationService)
extern void PlatformDictationImpl__ctor_m0706D7B2339D420DE33EF48B53639FBEB511CF6C (void);
// 0x00000030 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::get_PlatformSupportsDictation()
extern void PlatformDictationImpl_get_PlatformSupportsDictation_m05B0835DB8D3A09495BD92BF7D383339697164B0 (void);
// 0x00000031 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::get_Active()
extern void PlatformDictationImpl_get_Active_m605D245244B6F7F65559E137DF1350AD701AE8D7 (void);
// 0x00000032 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::get_IsRequestActive()
extern void PlatformDictationImpl_get_IsRequestActive_mCA084E6418344B4063A75C53CE56227D6F77CCF5 (void);
// 0x00000033 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::get_MicActive()
extern void PlatformDictationImpl_get_MicActive_m0E085A526876B5C329C64D4F48D694154B98D38A (void);
// 0x00000034 Facebook.WitAi.Interfaces.ITranscriptionProvider Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::get_TranscriptionProvider()
extern void PlatformDictationImpl_get_TranscriptionProvider_m7C7B74E06B21AE8D214559A0098E29C210C8EE08 (void);
// 0x00000035 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
extern void PlatformDictationImpl_set_TranscriptionProvider_m94D6C974E4BDFD4467EF25AF89B10E4D5E6711E3 (void);
// 0x00000036 Facebook.WitAi.Dictation.Events.DictationEvents Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::get_DictationEvents()
extern void PlatformDictationImpl_get_DictationEvents_m1601001030603129775E0CABC3BBF21B014D8C01 (void);
// 0x00000037 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::set_DictationEvents(Facebook.WitAi.Dictation.Events.DictationEvents)
extern void PlatformDictationImpl_set_DictationEvents_mE61CF8402CB9806F64517F79E62B7A9A464950DD (void);
// 0x00000038 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::Connect(System.String)
extern void PlatformDictationImpl_Connect_m29730A81BE375C02736957863877DFB57042B668 (void);
// 0x00000039 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::Disconnect()
extern void PlatformDictationImpl_Disconnect_mDA5C80D29A5EF2291D9B5AD7216A363D04C305D9 (void);
// 0x0000003A System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::SetDictationRuntimeConfiguration(Facebook.WitAi.Configuration.WitDictationRuntimeConfiguration)
extern void PlatformDictationImpl_SetDictationRuntimeConfiguration_m3A11F8F325E4EF6CC13AE453CE20A808414D5667 (void);
// 0x0000003B System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::Activate()
extern void PlatformDictationImpl_Activate_mBBF98A488FC26E2072B867AC64BA0989F2EA45C1 (void);
// 0x0000003C System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
extern void PlatformDictationImpl_Activate_m31644B5005233BC7FE606622A7B59CE6098CE335 (void);
// 0x0000003D System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::ActivateImmediately()
extern void PlatformDictationImpl_ActivateImmediately_m185F24DCAEA9E700500057A1F78F60BF5BFF8607 (void);
// 0x0000003E System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
extern void PlatformDictationImpl_ActivateImmediately_m993D4F6822131E2E39F23E11F7352D238FC3AEF7 (void);
// 0x0000003F System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::Deactivate()
extern void PlatformDictationImpl_Deactivate_mDD8582C3373FECD88131701AAD4AC230BA2F4FDA (void);
// 0x00000040 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationImpl::OnServiceNotAvailable(System.String,System.String)
extern void PlatformDictationImpl_OnServiceNotAvailable_mEC2B0A73497ABDD64E13AE0DE3DD2B53F0AF4AAA (void);
// 0x00000041 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::get_Active()
extern void PlatformDictationSDKBinding_get_Active_mAA65C94F54A819042564DB4CC27520721BD756A1 (void);
// 0x00000042 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::get_IsRequestActive()
extern void PlatformDictationSDKBinding_get_IsRequestActive_m0D6EC59884A2F4B3141803740F4887C4E39E68ED (void);
// 0x00000043 System.Boolean Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::get_IsSupported()
extern void PlatformDictationSDKBinding_get_IsSupported_m3F3B117D09C348F600DEF881F58ACF348FB85F8E (void);
// 0x00000044 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::.ctor(UnityEngine.AndroidJavaObject)
extern void PlatformDictationSDKBinding__ctor_m13AAB7C78C8F4A98AD3F23B6FC08B77CAF46A376 (void);
// 0x00000045 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::StartDictation(Oculus.Voice.Dictation.Bindings.Android.DictationConfigurationBinding)
extern void PlatformDictationSDKBinding_StartDictation_m66119D05E22161091C4662DFF15880E468BFBE12 (void);
// 0x00000046 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::StopDictation()
extern void PlatformDictationSDKBinding_StopDictation_mBC2A5FD4C582D829777C5F38C066B2A2D00D70E9 (void);
// 0x00000047 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSDKBinding::SetListener(Oculus.Voice.Dictation.Bindings.Android.DictationListenerBinding)
extern void PlatformDictationSDKBinding_SetListener_mE7A713A1CE5940C856C00994564F986A44DB4D70 (void);
// 0x00000048 System.Void Oculus.Voice.Dictation.Bindings.Android.PlatformDictationSession::.ctor()
extern void PlatformDictationSession__ctor_m7BE6CBBFF34DC64B9DF6E03BC145BCC3CF5D4917 (void);
// 0x00000049 System.Void Oculus.Voice.Dictation.Listeners.DictationListener::OnStart(Oculus.Voice.Dictation.Listeners.DictationListener)
// 0x0000004A System.Void Oculus.Voice.Dictation.Listeners.DictationListener::OnMicAudioLevel(System.Single)
// 0x0000004B System.Void Oculus.Voice.Dictation.Listeners.DictationListener::OnPartialTranscription(System.String)
// 0x0000004C System.Void Oculus.Voice.Dictation.Listeners.DictationListener::OnFinalTranscription(System.String)
// 0x0000004D System.Void Oculus.Voice.Dictation.Listeners.DictationListener::OnError(System.String,System.String)
// 0x0000004E System.Void Oculus.Voice.Dictation.Listeners.DictationListener::OnStopped(Oculus.Voice.Dictation.Listeners.DictationListener)
// 0x0000004F System.Void Oculus.Voice.Dictation.Configuration.DictationConfiguration::.ctor()
extern void DictationConfiguration__ctor_mC439BC70822509BA0D736C7860786074C93569AD (void);
static Il2CppMethodPointer s_methodPointers[79] = 
{
	WitDictationRuntimeConfiguration_get_RecordingTimeRange_m589BD3D30882A933211494FE2B6B4B1B9532E8BC,
	WitDictationRuntimeConfiguration__ctor_m37330A6D660400B0504D8630067DFBDB538FE191,
	MicPermissionsManager_HasMicPermission_mAA332F4C5D23B9A5DBEDDF9A9FAEC0AC07753EFE,
	MicPermissionsManager_RequestMicPermission_m5B1988DF51EC8CCACBB1769B3F99045B3D485466,
	MicPermissionsManager__ctor_mBF3E5998D6F9745D00A718A4B9C36F8E5D709F38,
	AppDictationExperience_get_RuntimeConfiguration_m548E09CAB17ABCC38BF4209F8FADB8E3E7BDDBE7,
	AppDictationExperience_set_RuntimeConfiguration_m7CBB89E858D1837D9FF89C5AE95D023F889FCDD6,
	AppDictationExperience_add_OnInitialized_m16968932EF12E3046A6943A3DB8CAE3D4A892AAA,
	AppDictationExperience_remove_OnInitialized_m4C905D5B608AA21630F9D85400E8AA1AFF60FAB3,
	AppDictationExperience_get_HasPlatformIntegrations_mE0A6F3711A843B074FF2FF29C6BD32165B681F6F,
	AppDictationExperience_get_UsePlatformIntegrations_m69582728E773426BE12D8FCBF12C017D77321D84,
	AppDictationExperience_set_UsePlatformIntegrations_m84324F3672DFC83EF0E69FC66E549BFF2E798CB2,
	AppDictationExperience_InitDictation_mA21FBBADFB78C8B19A6291BA50CEBEB228E243C3,
	AppDictationExperience_RevertToWitDictation_mDD0AE20C95BB6C10B1E19C9BA8A5B8C5669D18A0,
	AppDictationExperience_OnEnable_m874CCF1265ECDB4457CF9DBF5032E5BD57B3B05B,
	AppDictationExperience_OnDisable_m4CF196D898CFDAD5426A4F88777B493D69752A41,
	AppDictationExperience_get_Active_mF872FEFF9B5153607E58A9EDDFF51B0362D7DFE1,
	AppDictationExperience_get_IsRequestActive_m2FFDB2C89024FCCFC05B4CDE8960363D5B30D32E,
	AppDictationExperience_get_TranscriptionProvider_mB482F4DA4834D8B8C88A610AE18D45146F359065,
	AppDictationExperience_set_TranscriptionProvider_m7ED865F96F35D6AF79FBFDFE342D825342A9E3D3,
	AppDictationExperience_get_MicActive_m47193B2AEA4C6084454D57113EC4062152FC5F5E,
	AppDictationExperience_get_ShouldSendMicData_m17A16FD16747EF15FE5E55C883097194DBB7ED09,
	AppDictationExperience_Activate_mFE634E059096594F1BA54E1846FFA3945A447ABD,
	AppDictationExperience_Activate_m39FAC7E22C27AF5E71093B7D04AB95DF0DE59A6C,
	AppDictationExperience_ActivateImmediately_mA7C84C7CEFC41EBC2F7EA1F4A8BBA01DE96FD165,
	AppDictationExperience_ActivateImmediately_m22B7F73B229BE7F493F6AE6F131E49D26A24C2BB,
	AppDictationExperience_Deactivate_m95274EF71D05DCF2A445B95DEAF41D3620D04B6A,
	AppDictationExperience_Cancel_m4A4004638259887EFF3A14F7FF52C7B754D73E96,
	AppDictationExperience_OnWitResponseListener_m296041F73D8182B7D2B51DC485A9DBE0070EF0A2,
	AppDictationExperience_OnError_m2EC8C9553C97D12486713877FF1B77AD3DA42ECE,
	AppDictationExperience_OnStarted_mC12266EE60C391CC5CF65CE5D2F8E1C89B6B1F5C,
	AppDictationExperience_OnStopped_mA46807BB912E469942C5717CE6ED86FE4CD07C94,
	AppDictationExperience_OnDictationSessionStarted_m2AEC3F18E4D787A943E92B25C6E3A387D08153BA,
	AppDictationExperience__ctor_mA66C605EF4E256875B633E1646252A09F2A01EAB,
	DictationConfigurationBinding__ctor_m528B003C2FAE6E34DC78018C5B4526C5BCD74A5F,
	DictationConfigurationBinding_ToJavaObject_m6075DF79CE6D955C4194D256AFC36F048481B541,
	DictationListenerBinding_get_DictationEvents_mA12FCFAAB0F9081AC168CF0E7DF3774A7A722849,
	DictationListenerBinding__ctor_m57C72936ACA71A05D29F578402388AFD5B24501E,
	DictationListenerBinding_onStart_mDFFD8B2A7F8DA35C2A8CC77BED4CAEFA1040274F,
	DictationListenerBinding_onMicAudioLevel_m7A3006107FC73C03238D816479BF5A3058F89F38,
	DictationListenerBinding_onPartialTranscription_m4C6F92B2851B8CDA9269E4A603B4D12ADF26B645,
	DictationListenerBinding_onFinalTranscription_mC4F30479D61F5E57AF844B5425478CB8ADB5B5DA,
	DictationListenerBinding_onError_mA4602039E55579CBC8E6776C6F8D575FE749296D,
	DictationListenerBinding_onStopped_mAEEE15ECB1590148AC0A5E7C08D502B57D1ABE77,
	DictationListenerBinding_onServiceNotAvailable_m2535CEF04067114D2D4AFF21455C4837453A4FE2,
	NULL,
	PlatformDictationImpl__ctor_m0706D7B2339D420DE33EF48B53639FBEB511CF6C,
	PlatformDictationImpl_get_PlatformSupportsDictation_m05B0835DB8D3A09495BD92BF7D383339697164B0,
	PlatformDictationImpl_get_Active_m605D245244B6F7F65559E137DF1350AD701AE8D7,
	PlatformDictationImpl_get_IsRequestActive_mCA084E6418344B4063A75C53CE56227D6F77CCF5,
	PlatformDictationImpl_get_MicActive_m0E085A526876B5C329C64D4F48D694154B98D38A,
	PlatformDictationImpl_get_TranscriptionProvider_m7C7B74E06B21AE8D214559A0098E29C210C8EE08,
	PlatformDictationImpl_set_TranscriptionProvider_m94D6C974E4BDFD4467EF25AF89B10E4D5E6711E3,
	PlatformDictationImpl_get_DictationEvents_m1601001030603129775E0CABC3BBF21B014D8C01,
	PlatformDictationImpl_set_DictationEvents_mE61CF8402CB9806F64517F79E62B7A9A464950DD,
	PlatformDictationImpl_Connect_m29730A81BE375C02736957863877DFB57042B668,
	PlatformDictationImpl_Disconnect_mDA5C80D29A5EF2291D9B5AD7216A363D04C305D9,
	PlatformDictationImpl_SetDictationRuntimeConfiguration_m3A11F8F325E4EF6CC13AE453CE20A808414D5667,
	PlatformDictationImpl_Activate_mBBF98A488FC26E2072B867AC64BA0989F2EA45C1,
	PlatformDictationImpl_Activate_m31644B5005233BC7FE606622A7B59CE6098CE335,
	PlatformDictationImpl_ActivateImmediately_m185F24DCAEA9E700500057A1F78F60BF5BFF8607,
	PlatformDictationImpl_ActivateImmediately_m993D4F6822131E2E39F23E11F7352D238FC3AEF7,
	PlatformDictationImpl_Deactivate_mDD8582C3373FECD88131701AAD4AC230BA2F4FDA,
	PlatformDictationImpl_OnServiceNotAvailable_mEC2B0A73497ABDD64E13AE0DE3DD2B53F0AF4AAA,
	PlatformDictationSDKBinding_get_Active_mAA65C94F54A819042564DB4CC27520721BD756A1,
	PlatformDictationSDKBinding_get_IsRequestActive_m0D6EC59884A2F4B3141803740F4887C4E39E68ED,
	PlatformDictationSDKBinding_get_IsSupported_m3F3B117D09C348F600DEF881F58ACF348FB85F8E,
	PlatformDictationSDKBinding__ctor_m13AAB7C78C8F4A98AD3F23B6FC08B77CAF46A376,
	PlatformDictationSDKBinding_StartDictation_m66119D05E22161091C4662DFF15880E468BFBE12,
	PlatformDictationSDKBinding_StopDictation_mBC2A5FD4C582D829777C5F38C066B2A2D00D70E9,
	PlatformDictationSDKBinding_SetListener_mE7A713A1CE5940C856C00994564F986A44DB4D70,
	PlatformDictationSession__ctor_m7BE6CBBFF34DC64B9DF6E03BC145BCC3CF5D4917,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DictationConfiguration__ctor_mC439BC70822509BA0D736C7860786074C93569AD,
};
static const int32_t s_InvokerIndices[79] = 
{
	3704,
	3709,
	5637,
	5676,
	3709,
	3645,
	3049,
	3049,
	3049,
	3583,
	3583,
	2980,
	3709,
	3709,
	3709,
	3709,
	3583,
	3583,
	3645,
	3049,
	3583,
	3583,
	3709,
	3049,
	3709,
	3049,
	3709,
	3709,
	3049,
	1864,
	3709,
	3709,
	3049,
	3709,
	3049,
	3645,
	3645,
	1864,
	3049,
	1860,
	1864,
	1864,
	1204,
	3049,
	1864,
	1864,
	3049,
	3583,
	3583,
	3583,
	3583,
	3645,
	3049,
	3645,
	3049,
	3049,
	3709,
	3049,
	3709,
	3049,
	3709,
	3049,
	3709,
	1864,
	3583,
	3583,
	3583,
	3049,
	3049,
	3709,
	3049,
	3709,
	3049,
	3079,
	3049,
	3049,
	1864,
	3049,
	3709,
};
extern const CustomAttributesCacheGenerator g_VoiceSDK_Dictation_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_VoiceSDK_Dictation_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_VoiceSDK_Dictation_Runtime_CodeGenModule = 
{
	"VoiceSDK.Dictation.Runtime.dll",
	79,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_VoiceSDK_Dictation_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
