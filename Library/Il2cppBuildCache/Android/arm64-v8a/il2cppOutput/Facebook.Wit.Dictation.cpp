﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.String>
struct Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3;
// System.Action`1<Facebook.WitAi.WitRequest>
struct Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2;
// System.Collections.Concurrent.ConcurrentQueue`1<System.Byte[]>
struct ConcurrentQueue_1_tB28282707BE3B3A36759A443792B982678CC9571;
// System.Collections.Concurrent.ConcurrentQueue`1<System.Action>
struct ConcurrentQueue_1_tA29C1E7102CD564F57064BA3A2560608053994FE;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5;
// System.Func`1<System.String>
struct Func_1_t2F3325DADD1F420568A48646BFC825E9F29472B1;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229;
// System.Collections.Generic.HashSet`1<Facebook.WitAi.WitRequest>
struct HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// Facebook.WitAi.Data.RingBuffer`1/Marker<System.Byte>
struct Marker_tA491115B38EBB1FED9D61541569F1A7254D5E42A;
// System.Predicate`1<Facebook.WitAi.Dictation.DictationService>
struct Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421;
// System.Predicate`1<System.Object>
struct Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t00EE92422CBB066CEAB95CDDBF901E2967EC7B1A;
// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB;
// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647;
// UnityEngine.Events.UnityAction`1<Facebook.WitAi.Lib.WitResponseNode>
struct UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001;
// UnityEngine.Events.UnityAction`2<System.Object,System.Object>
struct UnityAction_2_tEA79D6DFB08A416619D920D80581B3A7C1376CCD;
// UnityEngine.Events.UnityAction`2<System.String,System.String>
struct UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC;
// UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Dictation.Data.DictationSession>
struct UnityEvent_1_t8B99F019C2E27198664DEEC6FE760B111EC0CADA;
// UnityEngine.Events.UnityEvent`1<System.Object>
struct UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F;
// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC;
// UnityEngine.Events.UnityEvent`1<System.String>
struct UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0;
// UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Lib.WitResponseNode>
struct UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055;
// UnityEngine.Events.UnityEvent`2<System.Object,System.Object>
struct UnityEvent_2_t28592AD5CBF18EB6ED3BE1B15D588E132DA53582;
// UnityEngine.Events.UnityEvent`2<System.String,System.String>
struct UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// Facebook.WitAi.Dictation.DictationService[]
struct DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36;
// Facebook.WitAi.Interfaces.IDynamicEntitiesProvider[]
struct IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107;
// Facebook.WitAi.Events.IWitByteDataReadyHandler[]
struct IWitByteDataReadyHandlerU5BU5D_tDAF18FFFEDA7AD9375B6460D05A3F76B7B129180;
// Facebook.WitAi.Events.IWitByteDataSentHandler[]
struct IWitByteDataSentHandlerU5BU5D_t50A06A7E11B774416F56D27DCF1E85671DA8D804;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// Facebook.WitAi.Data.Entities.WitEntity[]
struct WitEntityU5BU5D_tA95536CFAD78C68FB33FDF0B3BFA1BD73302E66D;
// Facebook.WitAi.Data.Intents.WitIntent[]
struct WitIntentU5BU5D_t41058B7A90DE88E8ED6B651A68997887FF21586C;
// Facebook.WitAi.Data.Traits.WitTrait[]
struct WitTraitU5BU5D_tF4A72012100EA4C0AAB6B63A20D37197CA259702;
// Facebook.WitAi.WitRequest/QueryParam[]
struct QueryParamU5BU5D_t599632D16DA1C62459C16B3C1A9206C89C61D61B;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// Facebook.WitAi.Data.AudioEncoding
struct AudioEncoding_tFFD240C59D2334B46F80D43E8E486019CACD2516;
// Facebook.WitAi.Events.UnityEventListeners.AudioEventListener
struct AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78;
// Facebook.WitAi.ServiceReferences.AudioInputServiceReference
struct AudioInputServiceReference_tB04E44B3DFE15FB3AAE393359CE8F56BADD2C063;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// Facebook.WitAi.Interfaces.CustomTranscriptionProvider
struct CustomTranscriptionProvider_tF690178502A6AC2682EDE3E3DDC067B4574A5F82;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Facebook.WitAi.Dictation.Events.DictationEvents
struct DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C;
// Facebook.WitAi.Dictation.DictationService
struct DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA;
// Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference
struct DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869;
// Facebook.WitAi.Dictation.Data.DictationSession
struct DictationSession_t5CC00AD98644841C3D6434B14273ADA40D0F6684;
// Facebook.WitAi.Dictation.Events.DictationSessionEvent
struct DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F;
// Facebook.WitAi.Events.EventRegistry
struct EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.Net.HttpWebRequest
struct HttpWebRequest_tDF8F794F1E3A8A19A63C2B57C1A28A42698BF07A;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Facebook.WitAi.Interfaces.IAudioInputEvents
struct IAudioInputEvents_t68D522FBF506982800DCC060BE08DE9E8439B439;
// Facebook.WitAi.Dictation.IDictationService
struct IDictationService_t5B2694AC06AC23F44798DC9F10D78FC3290DD3CF;
// Facebook.WitAi.Interfaces.IDynamicEntitiesProvider
struct IDynamicEntitiesProvider_tBF31A50AA3B53E8052F4CD0CF081EBF8670D0BEA;
// Facebook.WitAi.Interfaces.ITranscriptionEvent
struct ITranscriptionEvent_t47B16EA38544487BDB1D6CD2D4C20B594B93CCA2;
// Facebook.WitAi.Interfaces.ITranscriptionProvider
struct ITranscriptionProvider_tC607BE81017791B4ED85025471847899CE24E30C;
// Facebook.WitAi.IVoiceEventProvider
struct IVoiceEventProvider_t6B278DE031A329588899A64447D6030B6369A536;
// Facebook.WitAi.Interfaces.IWitRequestProvider
struct IWitRequestProvider_tD1F0E3249732753D542F88AD89510EF7AD27ACAE;
// Facebook.WitAi.IWitRuntimeConfigProvider
struct IWitRuntimeConfigProvider_tDCB7116C69FB4BA12B8EDBE3E51AAFB0F526BB65;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// Facebook.WitAi.Dictation.MultiRequestTranscription
struct MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener
struct TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// Facebook.WitAi.Events.VoiceEvents
struct VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D;
// Facebook.WitAi.VoiceService
struct VoiceService_t2A6345E70AC7B0298BFDB6CE48D0DF8E01D83400;
// Facebook.WitAi.Data.VoiceSession
struct VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Facebook.WitAi.Data.Configuration.WitApplication
struct WitApplication_t7A7F18E3FBD810923842318ADB5935CC9BBA2E89;
// Facebook.WitAi.Events.WitByteDataEvent
struct WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6;
// Facebook.WitAi.Data.Configuration.WitConfiguration
struct WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631;
// Facebook.WitAi.Dictation.WitDictation
struct WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866;
// Facebook.WitAi.Configuration.WitEndpointConfig
struct WitEndpointConfig_t6974155BF4F518549C3D435F2DA4BC4077706731;
// Facebook.WitAi.Events.WitErrorEvent
struct WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571;
// Facebook.WitAi.Events.WitMicLevelChangedEvent
struct WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B;
// Facebook.WitAi.WitRequest
struct WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622;
// Facebook.WitAi.Events.WitRequestCreatedEvent
struct WitRequestCreatedEvent_tC0FA819B0DDD5D9FBDB63FF11D5FEBC287C4BF82;
// Facebook.WitAi.Configuration.WitRequestOptions
struct WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F;
// Facebook.WitAi.Events.WitRequestOptionsEvent
struct WitRequestOptionsEvent_tF1DE1255AFB9C97934E6E1BA0258A7E229CC1504;
// Facebook.WitAi.Events.WitResponseEvent
struct WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4;
// Facebook.WitAi.Lib.WitResponseNode
struct WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F;
// Facebook.WitAi.Configuration.WitRuntimeConfiguration
struct WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24;
// Facebook.WitAi.WitService
struct WitService_t860537723698CF0607466342346F3B1FECA68DCB;
// Facebook.WitAi.Events.WitTranscriptionEvent
struct WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E;
// Facebook.WitAi.Events.WitValidationEvent
struct WitValidationEvent_t8705CFB7B3DF1330BB36ED4C1CB91AA530048237;
// Facebook.WitAi.Utilities.CoroutineUtility/CoroutinePerformer
struct CoroutinePerformer_tC5FB63434AAC929928A28F47AA557336E361C675;
// Facebook.WitAi.Utilities.DictationServiceReference/<>c
struct U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B;
// Facebook.WitAi.WitRequest/OnCustomizeUriEvent
struct OnCustomizeUriEvent_t9D13E53AB8681A345A39DAA3B800A45F146199FF;
// Facebook.WitAi.WitRequest/OnProvideCustomHeadersEvent
struct OnProvideCustomHeadersEvent_tFB4F29CECA9500A91331B43CEBADB0F51DDEC4D0;
// Facebook.WitAi.WitRequest/PreSendRequestDelegate
struct PreSendRequestDelegate_t59C335BC80A012FB63121965FD64BB1A8F937955;

IL2CPP_EXTERN_C RuntimeClass* DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Find_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mF24CEF963E25E3FBD9911167D4C6AECF3B5F769A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_m46F4C63848A93702DBA41F4E4881F698B7A03B01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m7E226A153B7C6780B3BCDA0C387355914CD83B2B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_mFA61385BD0582696C51D31B3024801A2A801A527_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m6A4BBAA3243EF2BF1E35E4A2C0E1187379E5A35B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_AddComponent_TisWitService_t860537723698CF0607466342346F3B1FECA68DCB_mD5C2810AD85ACA4F42F27CE5813E283611FE0A9F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisWitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_m4A0F4B51FFEF50647C9280DBB5707561E01A1931_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Predicate_1__ctor_mD497F949F2BED7397F05D0D036703EDCBCBEE67B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_FindObjectsOfTypeAll_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mBEEC450FD16C11556668039B77E33D516147B175_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3Cget_DictationServiceU3Eb__2_0_mCC5F3C71561157B5E4F8EC4E3F1349DD9AFE1AA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_mC862B0487562E93445C65F24FE68CD55D2236A9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_Invoke_m65140CE9B1391F17461B85941BAD199745AD67D6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_RemoveListener_m3AD600DB38F3A6E8D846AEAF3A6127393E209BC4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1__ctor_m09B74417C0662618691C12E55F1FC11718629511_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_2_AddListener_m3E022579578FE256EB77167A33F3003A4FC63B06_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_2_Invoke_m5E08B438F5EC94224B4DC570221B05F8CD17ADE1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_2_RemoveListener_m23EE42492565C29932813AA8242EC48E5C5CCF73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36;
struct IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t4373D187D0974E4ADCEE799B0A721E646920BC85 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// Facebook.WitAi.Events.EventRegistry
struct EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Facebook.WitAi.Events.EventRegistry::_overriddenCallbacks
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ____overriddenCallbacks_0;
	// System.Collections.Generic.HashSet`1<System.String> Facebook.WitAi.Events.EventRegistry::_overriddenCallbacksHash
	HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * ____overriddenCallbacksHash_1;

public:
	inline static int32_t get_offset_of__overriddenCallbacks_0() { return static_cast<int32_t>(offsetof(EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A, ____overriddenCallbacks_0)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get__overriddenCallbacks_0() const { return ____overriddenCallbacks_0; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of__overriddenCallbacks_0() { return &____overriddenCallbacks_0; }
	inline void set__overriddenCallbacks_0(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		____overriddenCallbacks_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____overriddenCallbacks_0), (void*)value);
	}

	inline static int32_t get_offset_of__overriddenCallbacksHash_1() { return static_cast<int32_t>(offsetof(EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A, ____overriddenCallbacksHash_1)); }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * get__overriddenCallbacksHash_1() const { return ____overriddenCallbacksHash_1; }
	inline HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 ** get_address_of__overriddenCallbacksHash_1() { return &____overriddenCallbacksHash_1; }
	inline void set__overriddenCallbacksHash_1(HashSet_1_t45F75268054D01D9E70EB33D7F6D2FA609DB9229 * value)
	{
		____overriddenCallbacksHash_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____overriddenCallbacksHash_1), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Facebook.WitAi.Data.VoiceSession
struct VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8  : public RuntimeObject
{
public:
	// Facebook.WitAi.VoiceService Facebook.WitAi.Data.VoiceSession::service
	VoiceService_t2A6345E70AC7B0298BFDB6CE48D0DF8E01D83400 * ___service_0;
	// Facebook.WitAi.Lib.WitResponseNode Facebook.WitAi.Data.VoiceSession::response
	WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * ___response_1;
	// System.Boolean Facebook.WitAi.Data.VoiceSession::validResponse
	bool ___validResponse_2;

public:
	inline static int32_t get_offset_of_service_0() { return static_cast<int32_t>(offsetof(VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8, ___service_0)); }
	inline VoiceService_t2A6345E70AC7B0298BFDB6CE48D0DF8E01D83400 * get_service_0() const { return ___service_0; }
	inline VoiceService_t2A6345E70AC7B0298BFDB6CE48D0DF8E01D83400 ** get_address_of_service_0() { return &___service_0; }
	inline void set_service_0(VoiceService_t2A6345E70AC7B0298BFDB6CE48D0DF8E01D83400 * value)
	{
		___service_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___service_0), (void*)value);
	}

	inline static int32_t get_offset_of_response_1() { return static_cast<int32_t>(offsetof(VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8, ___response_1)); }
	inline WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * get_response_1() const { return ___response_1; }
	inline WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F ** get_address_of_response_1() { return &___response_1; }
	inline void set_response_1(WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * value)
	{
		___response_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___response_1), (void*)value);
	}

	inline static int32_t get_offset_of_validResponse_2() { return static_cast<int32_t>(offsetof(VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8, ___validResponse_2)); }
	inline bool get_validResponse_2() const { return ___validResponse_2; }
	inline bool* get_address_of_validResponse_2() { return &___validResponse_2; }
	inline void set_validResponse_2(bool value)
	{
		___validResponse_2 = value;
	}
};


// Facebook.WitAi.Configuration.WitRequestOptions
struct WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F  : public RuntimeObject
{
public:
	// Facebook.WitAi.Interfaces.IDynamicEntitiesProvider Facebook.WitAi.Configuration.WitRequestOptions::dynamicEntities
	RuntimeObject* ___dynamicEntities_0;
	// System.Int32 Facebook.WitAi.Configuration.WitRequestOptions::nBestIntents
	int32_t ___nBestIntents_1;
	// System.String Facebook.WitAi.Configuration.WitRequestOptions::tag
	String_t* ___tag_2;
	// System.String Facebook.WitAi.Configuration.WitRequestOptions::requestID
	String_t* ___requestID_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.WitAi.Configuration.WitRequestOptions::additionalParameters
	Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * ___additionalParameters_4;
	// System.Action`1<Facebook.WitAi.WitRequest> Facebook.WitAi.Configuration.WitRequestOptions::onResponse
	Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * ___onResponse_5;

public:
	inline static int32_t get_offset_of_dynamicEntities_0() { return static_cast<int32_t>(offsetof(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F, ___dynamicEntities_0)); }
	inline RuntimeObject* get_dynamicEntities_0() const { return ___dynamicEntities_0; }
	inline RuntimeObject** get_address_of_dynamicEntities_0() { return &___dynamicEntities_0; }
	inline void set_dynamicEntities_0(RuntimeObject* value)
	{
		___dynamicEntities_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dynamicEntities_0), (void*)value);
	}

	inline static int32_t get_offset_of_nBestIntents_1() { return static_cast<int32_t>(offsetof(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F, ___nBestIntents_1)); }
	inline int32_t get_nBestIntents_1() const { return ___nBestIntents_1; }
	inline int32_t* get_address_of_nBestIntents_1() { return &___nBestIntents_1; }
	inline void set_nBestIntents_1(int32_t value)
	{
		___nBestIntents_1 = value;
	}

	inline static int32_t get_offset_of_tag_2() { return static_cast<int32_t>(offsetof(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F, ___tag_2)); }
	inline String_t* get_tag_2() const { return ___tag_2; }
	inline String_t** get_address_of_tag_2() { return &___tag_2; }
	inline void set_tag_2(String_t* value)
	{
		___tag_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tag_2), (void*)value);
	}

	inline static int32_t get_offset_of_requestID_3() { return static_cast<int32_t>(offsetof(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F, ___requestID_3)); }
	inline String_t* get_requestID_3() const { return ___requestID_3; }
	inline String_t** get_address_of_requestID_3() { return &___requestID_3; }
	inline void set_requestID_3(String_t* value)
	{
		___requestID_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___requestID_3), (void*)value);
	}

	inline static int32_t get_offset_of_additionalParameters_4() { return static_cast<int32_t>(offsetof(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F, ___additionalParameters_4)); }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * get_additionalParameters_4() const { return ___additionalParameters_4; }
	inline Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 ** get_address_of_additionalParameters_4() { return &___additionalParameters_4; }
	inline void set_additionalParameters_4(Dictionary_2_tDE3227CA5E7A32F5070BD24C69F42204A3ADE9D5 * value)
	{
		___additionalParameters_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalParameters_4), (void*)value);
	}

	inline static int32_t get_offset_of_onResponse_5() { return static_cast<int32_t>(offsetof(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F, ___onResponse_5)); }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * get_onResponse_5() const { return ___onResponse_5; }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 ** get_address_of_onResponse_5() { return &___onResponse_5; }
	inline void set_onResponse_5(Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * value)
	{
		___onResponse_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onResponse_5), (void*)value);
	}
};


// Facebook.WitAi.Lib.WitResponseNode
struct WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F  : public RuntimeObject
{
public:

public:
};


// Facebook.WitAi.Configuration.WitRuntimeConfiguration
struct WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24  : public RuntimeObject
{
public:
	// Facebook.WitAi.Data.Configuration.WitConfiguration Facebook.WitAi.Configuration.WitRuntimeConfiguration::witConfiguration
	WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * ___witConfiguration_0;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::minKeepAliveVolume
	float ___minKeepAliveVolume_1;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::minKeepAliveTimeInSeconds
	float ___minKeepAliveTimeInSeconds_2;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::minTranscriptionKeepAliveTimeInSeconds
	float ___minTranscriptionKeepAliveTimeInSeconds_3;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::maxRecordingTime
	float ___maxRecordingTime_4;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::soundWakeThreshold
	float ___soundWakeThreshold_5;
	// System.Int32 Facebook.WitAi.Configuration.WitRuntimeConfiguration::sampleLengthInMs
	int32_t ___sampleLengthInMs_6;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::micBufferLengthInSeconds
	float ___micBufferLengthInSeconds_7;
	// System.Int32 Facebook.WitAi.Configuration.WitRuntimeConfiguration::maxConcurrentRequests
	int32_t ___maxConcurrentRequests_8;
	// System.Boolean Facebook.WitAi.Configuration.WitRuntimeConfiguration::sendAudioToWit
	bool ___sendAudioToWit_9;
	// Facebook.WitAi.Interfaces.CustomTranscriptionProvider Facebook.WitAi.Configuration.WitRuntimeConfiguration::customTranscriptionProvider
	CustomTranscriptionProvider_tF690178502A6AC2682EDE3E3DDC067B4574A5F82 * ___customTranscriptionProvider_10;
	// System.Boolean Facebook.WitAi.Configuration.WitRuntimeConfiguration::alwaysRecord
	bool ___alwaysRecord_11;
	// System.Single Facebook.WitAi.Configuration.WitRuntimeConfiguration::preferredActivationOffset
	float ___preferredActivationOffset_12;

public:
	inline static int32_t get_offset_of_witConfiguration_0() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___witConfiguration_0)); }
	inline WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * get_witConfiguration_0() const { return ___witConfiguration_0; }
	inline WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 ** get_address_of_witConfiguration_0() { return &___witConfiguration_0; }
	inline void set_witConfiguration_0(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * value)
	{
		___witConfiguration_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___witConfiguration_0), (void*)value);
	}

	inline static int32_t get_offset_of_minKeepAliveVolume_1() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___minKeepAliveVolume_1)); }
	inline float get_minKeepAliveVolume_1() const { return ___minKeepAliveVolume_1; }
	inline float* get_address_of_minKeepAliveVolume_1() { return &___minKeepAliveVolume_1; }
	inline void set_minKeepAliveVolume_1(float value)
	{
		___minKeepAliveVolume_1 = value;
	}

	inline static int32_t get_offset_of_minKeepAliveTimeInSeconds_2() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___minKeepAliveTimeInSeconds_2)); }
	inline float get_minKeepAliveTimeInSeconds_2() const { return ___minKeepAliveTimeInSeconds_2; }
	inline float* get_address_of_minKeepAliveTimeInSeconds_2() { return &___minKeepAliveTimeInSeconds_2; }
	inline void set_minKeepAliveTimeInSeconds_2(float value)
	{
		___minKeepAliveTimeInSeconds_2 = value;
	}

	inline static int32_t get_offset_of_minTranscriptionKeepAliveTimeInSeconds_3() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___minTranscriptionKeepAliveTimeInSeconds_3)); }
	inline float get_minTranscriptionKeepAliveTimeInSeconds_3() const { return ___minTranscriptionKeepAliveTimeInSeconds_3; }
	inline float* get_address_of_minTranscriptionKeepAliveTimeInSeconds_3() { return &___minTranscriptionKeepAliveTimeInSeconds_3; }
	inline void set_minTranscriptionKeepAliveTimeInSeconds_3(float value)
	{
		___minTranscriptionKeepAliveTimeInSeconds_3 = value;
	}

	inline static int32_t get_offset_of_maxRecordingTime_4() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___maxRecordingTime_4)); }
	inline float get_maxRecordingTime_4() const { return ___maxRecordingTime_4; }
	inline float* get_address_of_maxRecordingTime_4() { return &___maxRecordingTime_4; }
	inline void set_maxRecordingTime_4(float value)
	{
		___maxRecordingTime_4 = value;
	}

	inline static int32_t get_offset_of_soundWakeThreshold_5() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___soundWakeThreshold_5)); }
	inline float get_soundWakeThreshold_5() const { return ___soundWakeThreshold_5; }
	inline float* get_address_of_soundWakeThreshold_5() { return &___soundWakeThreshold_5; }
	inline void set_soundWakeThreshold_5(float value)
	{
		___soundWakeThreshold_5 = value;
	}

	inline static int32_t get_offset_of_sampleLengthInMs_6() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___sampleLengthInMs_6)); }
	inline int32_t get_sampleLengthInMs_6() const { return ___sampleLengthInMs_6; }
	inline int32_t* get_address_of_sampleLengthInMs_6() { return &___sampleLengthInMs_6; }
	inline void set_sampleLengthInMs_6(int32_t value)
	{
		___sampleLengthInMs_6 = value;
	}

	inline static int32_t get_offset_of_micBufferLengthInSeconds_7() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___micBufferLengthInSeconds_7)); }
	inline float get_micBufferLengthInSeconds_7() const { return ___micBufferLengthInSeconds_7; }
	inline float* get_address_of_micBufferLengthInSeconds_7() { return &___micBufferLengthInSeconds_7; }
	inline void set_micBufferLengthInSeconds_7(float value)
	{
		___micBufferLengthInSeconds_7 = value;
	}

	inline static int32_t get_offset_of_maxConcurrentRequests_8() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___maxConcurrentRequests_8)); }
	inline int32_t get_maxConcurrentRequests_8() const { return ___maxConcurrentRequests_8; }
	inline int32_t* get_address_of_maxConcurrentRequests_8() { return &___maxConcurrentRequests_8; }
	inline void set_maxConcurrentRequests_8(int32_t value)
	{
		___maxConcurrentRequests_8 = value;
	}

	inline static int32_t get_offset_of_sendAudioToWit_9() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___sendAudioToWit_9)); }
	inline bool get_sendAudioToWit_9() const { return ___sendAudioToWit_9; }
	inline bool* get_address_of_sendAudioToWit_9() { return &___sendAudioToWit_9; }
	inline void set_sendAudioToWit_9(bool value)
	{
		___sendAudioToWit_9 = value;
	}

	inline static int32_t get_offset_of_customTranscriptionProvider_10() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___customTranscriptionProvider_10)); }
	inline CustomTranscriptionProvider_tF690178502A6AC2682EDE3E3DDC067B4574A5F82 * get_customTranscriptionProvider_10() const { return ___customTranscriptionProvider_10; }
	inline CustomTranscriptionProvider_tF690178502A6AC2682EDE3E3DDC067B4574A5F82 ** get_address_of_customTranscriptionProvider_10() { return &___customTranscriptionProvider_10; }
	inline void set_customTranscriptionProvider_10(CustomTranscriptionProvider_tF690178502A6AC2682EDE3E3DDC067B4574A5F82 * value)
	{
		___customTranscriptionProvider_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customTranscriptionProvider_10), (void*)value);
	}

	inline static int32_t get_offset_of_alwaysRecord_11() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___alwaysRecord_11)); }
	inline bool get_alwaysRecord_11() const { return ___alwaysRecord_11; }
	inline bool* get_address_of_alwaysRecord_11() { return &___alwaysRecord_11; }
	inline void set_alwaysRecord_11(bool value)
	{
		___alwaysRecord_11 = value;
	}

	inline static int32_t get_offset_of_preferredActivationOffset_12() { return static_cast<int32_t>(offsetof(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24, ___preferredActivationOffset_12)); }
	inline float get_preferredActivationOffset_12() const { return ___preferredActivationOffset_12; }
	inline float* get_address_of_preferredActivationOffset_12() { return &___preferredActivationOffset_12; }
	inline void set_preferredActivationOffset_12(float value)
	{
		___preferredActivationOffset_12 = value;
	}
};


// Facebook.WitAi.Utilities.DictationServiceReference/<>c
struct U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields
{
public:
	// Facebook.WitAi.Utilities.DictationServiceReference/<>c Facebook.WitAi.Utilities.DictationServiceReference/<>c::<>9
	U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * ___U3CU3E9_0;
	// System.Predicate`1<Facebook.WitAi.Dictation.DictationService> Facebook.WitAi.Utilities.DictationServiceReference/<>c::<>9__2_0
	Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * ___U3CU3E9__2_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__2_0_1), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Dictation.Data.DictationSession>
struct UnityEvent_1_t8B99F019C2E27198664DEEC6FE760B111EC0CADA  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t8B99F019C2E27198664DEEC6FE760B111EC0CADA, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Single>
struct UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.String>
struct UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Lib.WitResponseNode>
struct UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`2<System.String,System.String>
struct UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// Facebook.WitAi.Dictation.Events.DictationEvents
struct DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C  : public EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A
{
public:
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::onPartialTranscription
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onPartialTranscription_6;
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::onFullTranscription
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onFullTranscription_7;
	// Facebook.WitAi.Events.WitResponseEvent Facebook.WitAi.Dictation.Events.DictationEvents::onResponse
	WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * ___onResponse_8;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Dictation.Events.DictationEvents::onStart
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onStart_9;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Dictation.Events.DictationEvents::onStopped
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onStopped_10;
	// Facebook.WitAi.Events.WitErrorEvent Facebook.WitAi.Dictation.Events.DictationEvents::onError
	WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * ___onError_11;
	// Facebook.WitAi.Dictation.Events.DictationSessionEvent Facebook.WitAi.Dictation.Events.DictationEvents::onDictationSessionStarted
	DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * ___onDictationSessionStarted_12;
	// Facebook.WitAi.Dictation.Events.DictationSessionEvent Facebook.WitAi.Dictation.Events.DictationEvents::onDictationSessionStopped
	DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * ___onDictationSessionStopped_13;
	// Facebook.WitAi.Events.WitMicLevelChangedEvent Facebook.WitAi.Dictation.Events.DictationEvents::onMicAudioLevel
	WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * ___onMicAudioLevel_14;

public:
	inline static int32_t get_offset_of_onPartialTranscription_6() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onPartialTranscription_6)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onPartialTranscription_6() const { return ___onPartialTranscription_6; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onPartialTranscription_6() { return &___onPartialTranscription_6; }
	inline void set_onPartialTranscription_6(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onPartialTranscription_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPartialTranscription_6), (void*)value);
	}

	inline static int32_t get_offset_of_onFullTranscription_7() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onFullTranscription_7)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onFullTranscription_7() const { return ___onFullTranscription_7; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onFullTranscription_7() { return &___onFullTranscription_7; }
	inline void set_onFullTranscription_7(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onFullTranscription_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onFullTranscription_7), (void*)value);
	}

	inline static int32_t get_offset_of_onResponse_8() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onResponse_8)); }
	inline WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * get_onResponse_8() const { return ___onResponse_8; }
	inline WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 ** get_address_of_onResponse_8() { return &___onResponse_8; }
	inline void set_onResponse_8(WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * value)
	{
		___onResponse_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onResponse_8), (void*)value);
	}

	inline static int32_t get_offset_of_onStart_9() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onStart_9)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onStart_9() const { return ___onStart_9; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onStart_9() { return &___onStart_9; }
	inline void set_onStart_9(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onStart_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_9), (void*)value);
	}

	inline static int32_t get_offset_of_onStopped_10() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onStopped_10)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onStopped_10() const { return ___onStopped_10; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onStopped_10() { return &___onStopped_10; }
	inline void set_onStopped_10(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onStopped_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStopped_10), (void*)value);
	}

	inline static int32_t get_offset_of_onError_11() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onError_11)); }
	inline WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * get_onError_11() const { return ___onError_11; }
	inline WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 ** get_address_of_onError_11() { return &___onError_11; }
	inline void set_onError_11(WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * value)
	{
		___onError_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onError_11), (void*)value);
	}

	inline static int32_t get_offset_of_onDictationSessionStarted_12() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onDictationSessionStarted_12)); }
	inline DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * get_onDictationSessionStarted_12() const { return ___onDictationSessionStarted_12; }
	inline DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F ** get_address_of_onDictationSessionStarted_12() { return &___onDictationSessionStarted_12; }
	inline void set_onDictationSessionStarted_12(DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * value)
	{
		___onDictationSessionStarted_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onDictationSessionStarted_12), (void*)value);
	}

	inline static int32_t get_offset_of_onDictationSessionStopped_13() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onDictationSessionStopped_13)); }
	inline DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * get_onDictationSessionStopped_13() const { return ___onDictationSessionStopped_13; }
	inline DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F ** get_address_of_onDictationSessionStopped_13() { return &___onDictationSessionStopped_13; }
	inline void set_onDictationSessionStopped_13(DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * value)
	{
		___onDictationSessionStopped_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onDictationSessionStopped_13), (void*)value);
	}

	inline static int32_t get_offset_of_onMicAudioLevel_14() { return static_cast<int32_t>(offsetof(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C, ___onMicAudioLevel_14)); }
	inline WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * get_onMicAudioLevel_14() const { return ___onMicAudioLevel_14; }
	inline WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B ** get_address_of_onMicAudioLevel_14() { return &___onMicAudioLevel_14; }
	inline void set_onMicAudioLevel_14(WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * value)
	{
		___onMicAudioLevel_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onMicAudioLevel_14), (void*)value);
	}
};


// Facebook.WitAi.Utilities.DictationServiceReference
struct DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 
{
public:
	// Facebook.WitAi.Dictation.DictationService Facebook.WitAi.Utilities.DictationServiceReference::dictationService
	DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * ___dictationService_0;

public:
	inline static int32_t get_offset_of_dictationService_0() { return static_cast<int32_t>(offsetof(DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411, ___dictationService_0)); }
	inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * get_dictationService_0() const { return ___dictationService_0; }
	inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA ** get_address_of_dictationService_0() { return &___dictationService_0; }
	inline void set_dictationService_0(DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * value)
	{
		___dictationService_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictationService_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Facebook.WitAi.Utilities.DictationServiceReference
struct DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_pinvoke
{
	DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * ___dictationService_0;
};
// Native definition for COM marshalling of Facebook.WitAi.Utilities.DictationServiceReference
struct DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_com
{
	DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * ___dictationService_0;
};

// Facebook.WitAi.Dictation.Data.DictationSession
struct DictationSession_t5CC00AD98644841C3D6434B14273ADA40D0F6684  : public VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8
{
public:
	// Facebook.WitAi.Dictation.IDictationService Facebook.WitAi.Dictation.Data.DictationSession::dictationService
	RuntimeObject* ___dictationService_3;
	// System.String[] Facebook.WitAi.Dictation.Data.DictationSession::clientRequestId
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___clientRequestId_4;
	// System.String Facebook.WitAi.Dictation.Data.DictationSession::sessionId
	String_t* ___sessionId_5;

public:
	inline static int32_t get_offset_of_dictationService_3() { return static_cast<int32_t>(offsetof(DictationSession_t5CC00AD98644841C3D6434B14273ADA40D0F6684, ___dictationService_3)); }
	inline RuntimeObject* get_dictationService_3() const { return ___dictationService_3; }
	inline RuntimeObject** get_address_of_dictationService_3() { return &___dictationService_3; }
	inline void set_dictationService_3(RuntimeObject* value)
	{
		___dictationService_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictationService_3), (void*)value);
	}

	inline static int32_t get_offset_of_clientRequestId_4() { return static_cast<int32_t>(offsetof(DictationSession_t5CC00AD98644841C3D6434B14273ADA40D0F6684, ___clientRequestId_4)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_clientRequestId_4() const { return ___clientRequestId_4; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_clientRequestId_4() { return &___clientRequestId_4; }
	inline void set_clientRequestId_4(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___clientRequestId_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientRequestId_4), (void*)value);
	}

	inline static int32_t get_offset_of_sessionId_5() { return static_cast<int32_t>(offsetof(DictationSession_t5CC00AD98644841C3D6434B14273ADA40D0F6684, ___sessionId_5)); }
	inline String_t* get_sessionId_5() const { return ___sessionId_5; }
	inline String_t** get_address_of_sessionId_5() { return &___sessionId_5; }
	inline void set_sessionId_5(String_t* value)
	{
		___sessionId_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sessionId_5), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// Facebook.WitAi.Events.VoiceEvents
struct VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D  : public EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A
{
public:
	// Facebook.WitAi.Events.WitResponseEvent Facebook.WitAi.Events.VoiceEvents::OnResponse
	WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * ___OnResponse_7;
	// Facebook.WitAi.Events.WitResponseEvent Facebook.WitAi.Events.VoiceEvents::OnPartialResponse
	WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * ___OnPartialResponse_8;
	// Facebook.WitAi.Events.WitValidationEvent Facebook.WitAi.Events.VoiceEvents::OnValidatePartialResponse
	WitValidationEvent_t8705CFB7B3DF1330BB36ED4C1CB91AA530048237 * ___OnValidatePartialResponse_9;
	// Facebook.WitAi.Events.WitErrorEvent Facebook.WitAi.Events.VoiceEvents::OnError
	WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * ___OnError_10;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnAborting
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnAborting_11;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnAborted
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnAborted_12;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnRequestCompleted
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnRequestCompleted_13;
	// Facebook.WitAi.Events.WitMicLevelChangedEvent Facebook.WitAi.Events.VoiceEvents::OnMicLevelChanged
	WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * ___OnMicLevelChanged_14;
	// Facebook.WitAi.Events.WitRequestOptionsEvent Facebook.WitAi.Events.VoiceEvents::OnRequestOptionSetup
	WitRequestOptionsEvent_tF1DE1255AFB9C97934E6E1BA0258A7E229CC1504 * ___OnRequestOptionSetup_15;
	// Facebook.WitAi.Events.WitRequestCreatedEvent Facebook.WitAi.Events.VoiceEvents::OnRequestCreated
	WitRequestCreatedEvent_tC0FA819B0DDD5D9FBDB63FF11D5FEBC287C4BF82 * ___OnRequestCreated_16;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnStartListening
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStartListening_17;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnStoppedListening
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStoppedListening_18;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnStoppedListeningDueToInactivity
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStoppedListeningDueToInactivity_19;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnStoppedListeningDueToTimeout
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStoppedListeningDueToTimeout_20;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnStoppedListeningDueToDeactivation
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStoppedListeningDueToDeactivation_21;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnMicDataSent
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnMicDataSent_22;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.VoiceEvents::OnMinimumWakeThresholdHit
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnMinimumWakeThresholdHit_23;
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Events.VoiceEvents::onPartialTranscription
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onPartialTranscription_24;
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Events.VoiceEvents::onFullTranscription
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onFullTranscription_25;
	// Facebook.WitAi.Events.WitByteDataEvent Facebook.WitAi.Events.VoiceEvents::OnByteDataReady
	WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 * ___OnByteDataReady_26;
	// Facebook.WitAi.Events.WitByteDataEvent Facebook.WitAi.Events.VoiceEvents::OnByteDataSent
	WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 * ___OnByteDataSent_27;

public:
	inline static int32_t get_offset_of_OnResponse_7() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnResponse_7)); }
	inline WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * get_OnResponse_7() const { return ___OnResponse_7; }
	inline WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 ** get_address_of_OnResponse_7() { return &___OnResponse_7; }
	inline void set_OnResponse_7(WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * value)
	{
		___OnResponse_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnResponse_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnPartialResponse_8() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnPartialResponse_8)); }
	inline WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * get_OnPartialResponse_8() const { return ___OnPartialResponse_8; }
	inline WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 ** get_address_of_OnPartialResponse_8() { return &___OnPartialResponse_8; }
	inline void set_OnPartialResponse_8(WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * value)
	{
		___OnPartialResponse_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPartialResponse_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnValidatePartialResponse_9() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnValidatePartialResponse_9)); }
	inline WitValidationEvent_t8705CFB7B3DF1330BB36ED4C1CB91AA530048237 * get_OnValidatePartialResponse_9() const { return ___OnValidatePartialResponse_9; }
	inline WitValidationEvent_t8705CFB7B3DF1330BB36ED4C1CB91AA530048237 ** get_address_of_OnValidatePartialResponse_9() { return &___OnValidatePartialResponse_9; }
	inline void set_OnValidatePartialResponse_9(WitValidationEvent_t8705CFB7B3DF1330BB36ED4C1CB91AA530048237 * value)
	{
		___OnValidatePartialResponse_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnValidatePartialResponse_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnError_10() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnError_10)); }
	inline WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * get_OnError_10() const { return ___OnError_10; }
	inline WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 ** get_address_of_OnError_10() { return &___OnError_10; }
	inline void set_OnError_10(WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * value)
	{
		___OnError_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnError_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnAborting_11() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnAborting_11)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnAborting_11() const { return ___OnAborting_11; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnAborting_11() { return &___OnAborting_11; }
	inline void set_OnAborting_11(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnAborting_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAborting_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnAborted_12() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnAborted_12)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnAborted_12() const { return ___OnAborted_12; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnAborted_12() { return &___OnAborted_12; }
	inline void set_OnAborted_12(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnAborted_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnAborted_12), (void*)value);
	}

	inline static int32_t get_offset_of_OnRequestCompleted_13() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnRequestCompleted_13)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnRequestCompleted_13() const { return ___OnRequestCompleted_13; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnRequestCompleted_13() { return &___OnRequestCompleted_13; }
	inline void set_OnRequestCompleted_13(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnRequestCompleted_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRequestCompleted_13), (void*)value);
	}

	inline static int32_t get_offset_of_OnMicLevelChanged_14() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnMicLevelChanged_14)); }
	inline WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * get_OnMicLevelChanged_14() const { return ___OnMicLevelChanged_14; }
	inline WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B ** get_address_of_OnMicLevelChanged_14() { return &___OnMicLevelChanged_14; }
	inline void set_OnMicLevelChanged_14(WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * value)
	{
		___OnMicLevelChanged_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMicLevelChanged_14), (void*)value);
	}

	inline static int32_t get_offset_of_OnRequestOptionSetup_15() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnRequestOptionSetup_15)); }
	inline WitRequestOptionsEvent_tF1DE1255AFB9C97934E6E1BA0258A7E229CC1504 * get_OnRequestOptionSetup_15() const { return ___OnRequestOptionSetup_15; }
	inline WitRequestOptionsEvent_tF1DE1255AFB9C97934E6E1BA0258A7E229CC1504 ** get_address_of_OnRequestOptionSetup_15() { return &___OnRequestOptionSetup_15; }
	inline void set_OnRequestOptionSetup_15(WitRequestOptionsEvent_tF1DE1255AFB9C97934E6E1BA0258A7E229CC1504 * value)
	{
		___OnRequestOptionSetup_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRequestOptionSetup_15), (void*)value);
	}

	inline static int32_t get_offset_of_OnRequestCreated_16() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnRequestCreated_16)); }
	inline WitRequestCreatedEvent_tC0FA819B0DDD5D9FBDB63FF11D5FEBC287C4BF82 * get_OnRequestCreated_16() const { return ___OnRequestCreated_16; }
	inline WitRequestCreatedEvent_tC0FA819B0DDD5D9FBDB63FF11D5FEBC287C4BF82 ** get_address_of_OnRequestCreated_16() { return &___OnRequestCreated_16; }
	inline void set_OnRequestCreated_16(WitRequestCreatedEvent_tC0FA819B0DDD5D9FBDB63FF11D5FEBC287C4BF82 * value)
	{
		___OnRequestCreated_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRequestCreated_16), (void*)value);
	}

	inline static int32_t get_offset_of_OnStartListening_17() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnStartListening_17)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStartListening_17() const { return ___OnStartListening_17; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStartListening_17() { return &___OnStartListening_17; }
	inline void set_OnStartListening_17(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStartListening_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStartListening_17), (void*)value);
	}

	inline static int32_t get_offset_of_OnStoppedListening_18() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnStoppedListening_18)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStoppedListening_18() const { return ___OnStoppedListening_18; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStoppedListening_18() { return &___OnStoppedListening_18; }
	inline void set_OnStoppedListening_18(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStoppedListening_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStoppedListening_18), (void*)value);
	}

	inline static int32_t get_offset_of_OnStoppedListeningDueToInactivity_19() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnStoppedListeningDueToInactivity_19)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStoppedListeningDueToInactivity_19() const { return ___OnStoppedListeningDueToInactivity_19; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStoppedListeningDueToInactivity_19() { return &___OnStoppedListeningDueToInactivity_19; }
	inline void set_OnStoppedListeningDueToInactivity_19(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStoppedListeningDueToInactivity_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStoppedListeningDueToInactivity_19), (void*)value);
	}

	inline static int32_t get_offset_of_OnStoppedListeningDueToTimeout_20() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnStoppedListeningDueToTimeout_20)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStoppedListeningDueToTimeout_20() const { return ___OnStoppedListeningDueToTimeout_20; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStoppedListeningDueToTimeout_20() { return &___OnStoppedListeningDueToTimeout_20; }
	inline void set_OnStoppedListeningDueToTimeout_20(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStoppedListeningDueToTimeout_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStoppedListeningDueToTimeout_20), (void*)value);
	}

	inline static int32_t get_offset_of_OnStoppedListeningDueToDeactivation_21() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnStoppedListeningDueToDeactivation_21)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStoppedListeningDueToDeactivation_21() const { return ___OnStoppedListeningDueToDeactivation_21; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStoppedListeningDueToDeactivation_21() { return &___OnStoppedListeningDueToDeactivation_21; }
	inline void set_OnStoppedListeningDueToDeactivation_21(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStoppedListeningDueToDeactivation_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStoppedListeningDueToDeactivation_21), (void*)value);
	}

	inline static int32_t get_offset_of_OnMicDataSent_22() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnMicDataSent_22)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnMicDataSent_22() const { return ___OnMicDataSent_22; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnMicDataSent_22() { return &___OnMicDataSent_22; }
	inline void set_OnMicDataSent_22(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnMicDataSent_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMicDataSent_22), (void*)value);
	}

	inline static int32_t get_offset_of_OnMinimumWakeThresholdHit_23() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnMinimumWakeThresholdHit_23)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnMinimumWakeThresholdHit_23() const { return ___OnMinimumWakeThresholdHit_23; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnMinimumWakeThresholdHit_23() { return &___OnMinimumWakeThresholdHit_23; }
	inline void set_OnMinimumWakeThresholdHit_23(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnMinimumWakeThresholdHit_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMinimumWakeThresholdHit_23), (void*)value);
	}

	inline static int32_t get_offset_of_onPartialTranscription_24() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___onPartialTranscription_24)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onPartialTranscription_24() const { return ___onPartialTranscription_24; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onPartialTranscription_24() { return &___onPartialTranscription_24; }
	inline void set_onPartialTranscription_24(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onPartialTranscription_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPartialTranscription_24), (void*)value);
	}

	inline static int32_t get_offset_of_onFullTranscription_25() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___onFullTranscription_25)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onFullTranscription_25() const { return ___onFullTranscription_25; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onFullTranscription_25() { return &___onFullTranscription_25; }
	inline void set_onFullTranscription_25(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onFullTranscription_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onFullTranscription_25), (void*)value);
	}

	inline static int32_t get_offset_of_OnByteDataReady_26() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnByteDataReady_26)); }
	inline WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 * get_OnByteDataReady_26() const { return ___OnByteDataReady_26; }
	inline WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 ** get_address_of_OnByteDataReady_26() { return &___OnByteDataReady_26; }
	inline void set_OnByteDataReady_26(WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 * value)
	{
		___OnByteDataReady_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnByteDataReady_26), (void*)value);
	}

	inline static int32_t get_offset_of_OnByteDataSent_27() { return static_cast<int32_t>(offsetof(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D, ___OnByteDataSent_27)); }
	inline WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 * get_OnByteDataSent_27() const { return ___OnByteDataSent_27; }
	inline WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 ** get_address_of_OnByteDataSent_27() { return &___OnByteDataSent_27; }
	inline void set_OnByteDataSent_27(WitByteDataEvent_tD413F19EC04F5167EE0767133C3770C049B0F7F6 * value)
	{
		___OnByteDataSent_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnByteDataSent_27), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Facebook.WitAi.Dictation.Events.DictationSessionEvent
struct DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F  : public UnityEvent_1_t8B99F019C2E27198664DEEC6FE760B111EC0CADA
{
public:

public:
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Facebook.WitAi.Events.WitErrorEvent
struct WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571  : public UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4
{
public:

public:
};


// Facebook.WitAi.Events.WitMicLevelChangedEvent
struct WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B  : public UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC
{
public:

public:
};


// Facebook.WitAi.WitRequest
struct WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622  : public RuntimeObject
{
public:
	// Facebook.WitAi.Data.Configuration.WitConfiguration Facebook.WitAi.WitRequest::configuration
	WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * ___configuration_20;
	// System.String Facebook.WitAi.WitRequest::command
	String_t* ___command_21;
	// System.String Facebook.WitAi.WitRequest::path
	String_t* ___path_22;
	// Facebook.WitAi.WitRequest/QueryParam[] Facebook.WitAi.WitRequest::queryParams
	QueryParamU5BU5D_t599632D16DA1C62459C16B3C1A9206C89C61D61B* ___queryParams_23;
	// System.Net.HttpWebRequest Facebook.WitAi.WitRequest::_request
	HttpWebRequest_tDF8F794F1E3A8A19A63C2B57C1A28A42698BF07A * ____request_24;
	// System.IO.Stream Facebook.WitAi.WitRequest::_writeStream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ____writeStream_25;
	// Facebook.WitAi.Lib.WitResponseNode Facebook.WitAi.WitRequest::responseData
	WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * ___responseData_26;
	// System.Boolean Facebook.WitAi.WitRequest::isActive
	bool ___isActive_27;
	// System.Boolean Facebook.WitAi.WitRequest::responseStarted
	bool ___responseStarted_28;
	// System.Byte[] Facebook.WitAi.WitRequest::postData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___postData_29;
	// System.String Facebook.WitAi.WitRequest::postContentType
	String_t* ___postContentType_30;
	// System.String Facebook.WitAi.WitRequest::requestId
	String_t* ___requestId_31;
	// System.Object Facebook.WitAi.WitRequest::streamLock
	RuntimeObject * ___streamLock_32;
	// System.Int32 Facebook.WitAi.WitRequest::bytesWritten
	int32_t ___bytesWritten_33;
	// System.Boolean Facebook.WitAi.WitRequest::requestRequiresBody
	bool ___requestRequiresBody_34;
	// System.Action`1<Facebook.WitAi.WitRequest> Facebook.WitAi.WitRequest::onPartialResponse
	Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * ___onPartialResponse_35;
	// System.Action`1<Facebook.WitAi.WitRequest> Facebook.WitAi.WitRequest::onResponse
	Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * ___onResponse_36;
	// System.Action`1<Facebook.WitAi.WitRequest> Facebook.WitAi.WitRequest::onInputStreamReady
	Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * ___onInputStreamReady_37;
	// System.Action`1<System.String> Facebook.WitAi.WitRequest::onRawResponse
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___onRawResponse_38;
	// System.Action`1<System.String> Facebook.WitAi.WitRequest::onPartialTranscription
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___onPartialTranscription_39;
	// System.Action`1<System.String> Facebook.WitAi.WitRequest::onFullTranscription
	Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * ___onFullTranscription_40;
	// Facebook.WitAi.WitRequest/OnCustomizeUriEvent Facebook.WitAi.WitRequest::onCustomizeUri
	OnCustomizeUriEvent_t9D13E53AB8681A345A39DAA3B800A45F146199FF * ___onCustomizeUri_42;
	// Facebook.WitAi.WitRequest/OnProvideCustomHeadersEvent Facebook.WitAi.WitRequest::onProvideCustomHeaders
	OnProvideCustomHeadersEvent_tFB4F29CECA9500A91331B43CEBADB0F51DDEC4D0 * ___onProvideCustomHeaders_43;
	// Facebook.WitAi.Data.AudioEncoding Facebook.WitAi.WitRequest::audioEncoding
	AudioEncoding_tFFD240C59D2334B46F80D43E8E486019CACD2516 * ___audioEncoding_44;
	// System.Int32 Facebook.WitAi.WitRequest::statusCode
	int32_t ___statusCode_45;
	// System.String Facebook.WitAi.WitRequest::statusDescription
	String_t* ___statusDescription_46;
	// System.Boolean Facebook.WitAi.WitRequest::isRequestStreamActive
	bool ___isRequestStreamActive_47;
	// System.Boolean Facebook.WitAi.WitRequest::isServerAuthRequired
	bool ___isServerAuthRequired_48;
	// System.Boolean Facebook.WitAi.WitRequest::configurationRequired
	bool ___configurationRequired_49;
	// System.String Facebook.WitAi.WitRequest::serverToken
	String_t* ___serverToken_50;
	// System.String Facebook.WitAi.WitRequest::callingStackTrace
	String_t* ___callingStackTrace_51;
	// System.DateTime Facebook.WitAi.WitRequest::requestStartTime
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___requestStartTime_52;
	// System.Collections.Concurrent.ConcurrentQueue`1<System.Byte[]> Facebook.WitAi.WitRequest::writeBuffer
	ConcurrentQueue_1_tB28282707BE3B3A36759A443792B982678CC9571 * ___writeBuffer_53;
	// Facebook.WitAi.Utilities.CoroutineUtility/CoroutinePerformer Facebook.WitAi.WitRequest::_performer
	CoroutinePerformer_tC5FB63434AAC929928A28F47AA557336E361C675 * ____performer_59;
	// System.Collections.Concurrent.ConcurrentQueue`1<System.Action> Facebook.WitAi.WitRequest::_mainThreadCallbacks
	ConcurrentQueue_1_tA29C1E7102CD564F57064BA3A2560608053994FE * ____mainThreadCallbacks_60;

public:
	inline static int32_t get_offset_of_configuration_20() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___configuration_20)); }
	inline WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * get_configuration_20() const { return ___configuration_20; }
	inline WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 ** get_address_of_configuration_20() { return &___configuration_20; }
	inline void set_configuration_20(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * value)
	{
		___configuration_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___configuration_20), (void*)value);
	}

	inline static int32_t get_offset_of_command_21() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___command_21)); }
	inline String_t* get_command_21() const { return ___command_21; }
	inline String_t** get_address_of_command_21() { return &___command_21; }
	inline void set_command_21(String_t* value)
	{
		___command_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___command_21), (void*)value);
	}

	inline static int32_t get_offset_of_path_22() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___path_22)); }
	inline String_t* get_path_22() const { return ___path_22; }
	inline String_t** get_address_of_path_22() { return &___path_22; }
	inline void set_path_22(String_t* value)
	{
		___path_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___path_22), (void*)value);
	}

	inline static int32_t get_offset_of_queryParams_23() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___queryParams_23)); }
	inline QueryParamU5BU5D_t599632D16DA1C62459C16B3C1A9206C89C61D61B* get_queryParams_23() const { return ___queryParams_23; }
	inline QueryParamU5BU5D_t599632D16DA1C62459C16B3C1A9206C89C61D61B** get_address_of_queryParams_23() { return &___queryParams_23; }
	inline void set_queryParams_23(QueryParamU5BU5D_t599632D16DA1C62459C16B3C1A9206C89C61D61B* value)
	{
		___queryParams_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___queryParams_23), (void*)value);
	}

	inline static int32_t get_offset_of__request_24() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ____request_24)); }
	inline HttpWebRequest_tDF8F794F1E3A8A19A63C2B57C1A28A42698BF07A * get__request_24() const { return ____request_24; }
	inline HttpWebRequest_tDF8F794F1E3A8A19A63C2B57C1A28A42698BF07A ** get_address_of__request_24() { return &____request_24; }
	inline void set__request_24(HttpWebRequest_tDF8F794F1E3A8A19A63C2B57C1A28A42698BF07A * value)
	{
		____request_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____request_24), (void*)value);
	}

	inline static int32_t get_offset_of__writeStream_25() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ____writeStream_25)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get__writeStream_25() const { return ____writeStream_25; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of__writeStream_25() { return &____writeStream_25; }
	inline void set__writeStream_25(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		____writeStream_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____writeStream_25), (void*)value);
	}

	inline static int32_t get_offset_of_responseData_26() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___responseData_26)); }
	inline WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * get_responseData_26() const { return ___responseData_26; }
	inline WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F ** get_address_of_responseData_26() { return &___responseData_26; }
	inline void set_responseData_26(WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * value)
	{
		___responseData_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___responseData_26), (void*)value);
	}

	inline static int32_t get_offset_of_isActive_27() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___isActive_27)); }
	inline bool get_isActive_27() const { return ___isActive_27; }
	inline bool* get_address_of_isActive_27() { return &___isActive_27; }
	inline void set_isActive_27(bool value)
	{
		___isActive_27 = value;
	}

	inline static int32_t get_offset_of_responseStarted_28() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___responseStarted_28)); }
	inline bool get_responseStarted_28() const { return ___responseStarted_28; }
	inline bool* get_address_of_responseStarted_28() { return &___responseStarted_28; }
	inline void set_responseStarted_28(bool value)
	{
		___responseStarted_28 = value;
	}

	inline static int32_t get_offset_of_postData_29() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___postData_29)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_postData_29() const { return ___postData_29; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_postData_29() { return &___postData_29; }
	inline void set_postData_29(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___postData_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___postData_29), (void*)value);
	}

	inline static int32_t get_offset_of_postContentType_30() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___postContentType_30)); }
	inline String_t* get_postContentType_30() const { return ___postContentType_30; }
	inline String_t** get_address_of_postContentType_30() { return &___postContentType_30; }
	inline void set_postContentType_30(String_t* value)
	{
		___postContentType_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___postContentType_30), (void*)value);
	}

	inline static int32_t get_offset_of_requestId_31() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___requestId_31)); }
	inline String_t* get_requestId_31() const { return ___requestId_31; }
	inline String_t** get_address_of_requestId_31() { return &___requestId_31; }
	inline void set_requestId_31(String_t* value)
	{
		___requestId_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___requestId_31), (void*)value);
	}

	inline static int32_t get_offset_of_streamLock_32() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___streamLock_32)); }
	inline RuntimeObject * get_streamLock_32() const { return ___streamLock_32; }
	inline RuntimeObject ** get_address_of_streamLock_32() { return &___streamLock_32; }
	inline void set_streamLock_32(RuntimeObject * value)
	{
		___streamLock_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___streamLock_32), (void*)value);
	}

	inline static int32_t get_offset_of_bytesWritten_33() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___bytesWritten_33)); }
	inline int32_t get_bytesWritten_33() const { return ___bytesWritten_33; }
	inline int32_t* get_address_of_bytesWritten_33() { return &___bytesWritten_33; }
	inline void set_bytesWritten_33(int32_t value)
	{
		___bytesWritten_33 = value;
	}

	inline static int32_t get_offset_of_requestRequiresBody_34() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___requestRequiresBody_34)); }
	inline bool get_requestRequiresBody_34() const { return ___requestRequiresBody_34; }
	inline bool* get_address_of_requestRequiresBody_34() { return &___requestRequiresBody_34; }
	inline void set_requestRequiresBody_34(bool value)
	{
		___requestRequiresBody_34 = value;
	}

	inline static int32_t get_offset_of_onPartialResponse_35() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onPartialResponse_35)); }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * get_onPartialResponse_35() const { return ___onPartialResponse_35; }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 ** get_address_of_onPartialResponse_35() { return &___onPartialResponse_35; }
	inline void set_onPartialResponse_35(Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * value)
	{
		___onPartialResponse_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPartialResponse_35), (void*)value);
	}

	inline static int32_t get_offset_of_onResponse_36() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onResponse_36)); }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * get_onResponse_36() const { return ___onResponse_36; }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 ** get_address_of_onResponse_36() { return &___onResponse_36; }
	inline void set_onResponse_36(Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * value)
	{
		___onResponse_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onResponse_36), (void*)value);
	}

	inline static int32_t get_offset_of_onInputStreamReady_37() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onInputStreamReady_37)); }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * get_onInputStreamReady_37() const { return ___onInputStreamReady_37; }
	inline Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 ** get_address_of_onInputStreamReady_37() { return &___onInputStreamReady_37; }
	inline void set_onInputStreamReady_37(Action_1_tBD372F2A676F67640ED3AC034AD7895629387ED2 * value)
	{
		___onInputStreamReady_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onInputStreamReady_37), (void*)value);
	}

	inline static int32_t get_offset_of_onRawResponse_38() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onRawResponse_38)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_onRawResponse_38() const { return ___onRawResponse_38; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_onRawResponse_38() { return &___onRawResponse_38; }
	inline void set_onRawResponse_38(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___onRawResponse_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRawResponse_38), (void*)value);
	}

	inline static int32_t get_offset_of_onPartialTranscription_39() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onPartialTranscription_39)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_onPartialTranscription_39() const { return ___onPartialTranscription_39; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_onPartialTranscription_39() { return &___onPartialTranscription_39; }
	inline void set_onPartialTranscription_39(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___onPartialTranscription_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPartialTranscription_39), (void*)value);
	}

	inline static int32_t get_offset_of_onFullTranscription_40() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onFullTranscription_40)); }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * get_onFullTranscription_40() const { return ___onFullTranscription_40; }
	inline Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 ** get_address_of_onFullTranscription_40() { return &___onFullTranscription_40; }
	inline void set_onFullTranscription_40(Action_1_tC0D73E03177C82525D78670CDC2165F7CBF0A9C3 * value)
	{
		___onFullTranscription_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onFullTranscription_40), (void*)value);
	}

	inline static int32_t get_offset_of_onCustomizeUri_42() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onCustomizeUri_42)); }
	inline OnCustomizeUriEvent_t9D13E53AB8681A345A39DAA3B800A45F146199FF * get_onCustomizeUri_42() const { return ___onCustomizeUri_42; }
	inline OnCustomizeUriEvent_t9D13E53AB8681A345A39DAA3B800A45F146199FF ** get_address_of_onCustomizeUri_42() { return &___onCustomizeUri_42; }
	inline void set_onCustomizeUri_42(OnCustomizeUriEvent_t9D13E53AB8681A345A39DAA3B800A45F146199FF * value)
	{
		___onCustomizeUri_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onCustomizeUri_42), (void*)value);
	}

	inline static int32_t get_offset_of_onProvideCustomHeaders_43() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___onProvideCustomHeaders_43)); }
	inline OnProvideCustomHeadersEvent_tFB4F29CECA9500A91331B43CEBADB0F51DDEC4D0 * get_onProvideCustomHeaders_43() const { return ___onProvideCustomHeaders_43; }
	inline OnProvideCustomHeadersEvent_tFB4F29CECA9500A91331B43CEBADB0F51DDEC4D0 ** get_address_of_onProvideCustomHeaders_43() { return &___onProvideCustomHeaders_43; }
	inline void set_onProvideCustomHeaders_43(OnProvideCustomHeadersEvent_tFB4F29CECA9500A91331B43CEBADB0F51DDEC4D0 * value)
	{
		___onProvideCustomHeaders_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onProvideCustomHeaders_43), (void*)value);
	}

	inline static int32_t get_offset_of_audioEncoding_44() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___audioEncoding_44)); }
	inline AudioEncoding_tFFD240C59D2334B46F80D43E8E486019CACD2516 * get_audioEncoding_44() const { return ___audioEncoding_44; }
	inline AudioEncoding_tFFD240C59D2334B46F80D43E8E486019CACD2516 ** get_address_of_audioEncoding_44() { return &___audioEncoding_44; }
	inline void set_audioEncoding_44(AudioEncoding_tFFD240C59D2334B46F80D43E8E486019CACD2516 * value)
	{
		___audioEncoding_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___audioEncoding_44), (void*)value);
	}

	inline static int32_t get_offset_of_statusCode_45() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___statusCode_45)); }
	inline int32_t get_statusCode_45() const { return ___statusCode_45; }
	inline int32_t* get_address_of_statusCode_45() { return &___statusCode_45; }
	inline void set_statusCode_45(int32_t value)
	{
		___statusCode_45 = value;
	}

	inline static int32_t get_offset_of_statusDescription_46() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___statusDescription_46)); }
	inline String_t* get_statusDescription_46() const { return ___statusDescription_46; }
	inline String_t** get_address_of_statusDescription_46() { return &___statusDescription_46; }
	inline void set_statusDescription_46(String_t* value)
	{
		___statusDescription_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___statusDescription_46), (void*)value);
	}

	inline static int32_t get_offset_of_isRequestStreamActive_47() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___isRequestStreamActive_47)); }
	inline bool get_isRequestStreamActive_47() const { return ___isRequestStreamActive_47; }
	inline bool* get_address_of_isRequestStreamActive_47() { return &___isRequestStreamActive_47; }
	inline void set_isRequestStreamActive_47(bool value)
	{
		___isRequestStreamActive_47 = value;
	}

	inline static int32_t get_offset_of_isServerAuthRequired_48() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___isServerAuthRequired_48)); }
	inline bool get_isServerAuthRequired_48() const { return ___isServerAuthRequired_48; }
	inline bool* get_address_of_isServerAuthRequired_48() { return &___isServerAuthRequired_48; }
	inline void set_isServerAuthRequired_48(bool value)
	{
		___isServerAuthRequired_48 = value;
	}

	inline static int32_t get_offset_of_configurationRequired_49() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___configurationRequired_49)); }
	inline bool get_configurationRequired_49() const { return ___configurationRequired_49; }
	inline bool* get_address_of_configurationRequired_49() { return &___configurationRequired_49; }
	inline void set_configurationRequired_49(bool value)
	{
		___configurationRequired_49 = value;
	}

	inline static int32_t get_offset_of_serverToken_50() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___serverToken_50)); }
	inline String_t* get_serverToken_50() const { return ___serverToken_50; }
	inline String_t** get_address_of_serverToken_50() { return &___serverToken_50; }
	inline void set_serverToken_50(String_t* value)
	{
		___serverToken_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverToken_50), (void*)value);
	}

	inline static int32_t get_offset_of_callingStackTrace_51() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___callingStackTrace_51)); }
	inline String_t* get_callingStackTrace_51() const { return ___callingStackTrace_51; }
	inline String_t** get_address_of_callingStackTrace_51() { return &___callingStackTrace_51; }
	inline void set_callingStackTrace_51(String_t* value)
	{
		___callingStackTrace_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callingStackTrace_51), (void*)value);
	}

	inline static int32_t get_offset_of_requestStartTime_52() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___requestStartTime_52)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_requestStartTime_52() const { return ___requestStartTime_52; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_requestStartTime_52() { return &___requestStartTime_52; }
	inline void set_requestStartTime_52(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___requestStartTime_52 = value;
	}

	inline static int32_t get_offset_of_writeBuffer_53() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ___writeBuffer_53)); }
	inline ConcurrentQueue_1_tB28282707BE3B3A36759A443792B982678CC9571 * get_writeBuffer_53() const { return ___writeBuffer_53; }
	inline ConcurrentQueue_1_tB28282707BE3B3A36759A443792B982678CC9571 ** get_address_of_writeBuffer_53() { return &___writeBuffer_53; }
	inline void set_writeBuffer_53(ConcurrentQueue_1_tB28282707BE3B3A36759A443792B982678CC9571 * value)
	{
		___writeBuffer_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___writeBuffer_53), (void*)value);
	}

	inline static int32_t get_offset_of__performer_59() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ____performer_59)); }
	inline CoroutinePerformer_tC5FB63434AAC929928A28F47AA557336E361C675 * get__performer_59() const { return ____performer_59; }
	inline CoroutinePerformer_tC5FB63434AAC929928A28F47AA557336E361C675 ** get_address_of__performer_59() { return &____performer_59; }
	inline void set__performer_59(CoroutinePerformer_tC5FB63434AAC929928A28F47AA557336E361C675 * value)
	{
		____performer_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____performer_59), (void*)value);
	}

	inline static int32_t get_offset_of__mainThreadCallbacks_60() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622, ____mainThreadCallbacks_60)); }
	inline ConcurrentQueue_1_tA29C1E7102CD564F57064BA3A2560608053994FE * get__mainThreadCallbacks_60() const { return ____mainThreadCallbacks_60; }
	inline ConcurrentQueue_1_tA29C1E7102CD564F57064BA3A2560608053994FE ** get_address_of__mainThreadCallbacks_60() { return &____mainThreadCallbacks_60; }
	inline void set__mainThreadCallbacks_60(ConcurrentQueue_1_tA29C1E7102CD564F57064BA3A2560608053994FE * value)
	{
		____mainThreadCallbacks_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainThreadCallbacks_60), (void*)value);
	}
};

struct WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields
{
public:
	// Facebook.WitAi.WitRequest/PreSendRequestDelegate Facebook.WitAi.WitRequest::onPreSendRequest
	PreSendRequestDelegate_t59C335BC80A012FB63121965FD64BB1A8F937955 * ___onPreSendRequest_41;
	// System.String Facebook.WitAi.WitRequest::_operatingSystem
	String_t* ____operatingSystem_54;
	// System.String Facebook.WitAi.WitRequest::_deviceModel
	String_t* ____deviceModel_55;
	// System.String Facebook.WitAi.WitRequest::_appIdentifier
	String_t* ____appIdentifier_56;
	// System.String Facebook.WitAi.WitRequest::_unityVersion
	String_t* ____unityVersion_57;
	// System.Func`1<System.String> Facebook.WitAi.WitRequest::OnProvideCustomUserAgent
	Func_1_t2F3325DADD1F420568A48646BFC825E9F29472B1 * ___OnProvideCustomUserAgent_58;

public:
	inline static int32_t get_offset_of_onPreSendRequest_41() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields, ___onPreSendRequest_41)); }
	inline PreSendRequestDelegate_t59C335BC80A012FB63121965FD64BB1A8F937955 * get_onPreSendRequest_41() const { return ___onPreSendRequest_41; }
	inline PreSendRequestDelegate_t59C335BC80A012FB63121965FD64BB1A8F937955 ** get_address_of_onPreSendRequest_41() { return &___onPreSendRequest_41; }
	inline void set_onPreSendRequest_41(PreSendRequestDelegate_t59C335BC80A012FB63121965FD64BB1A8F937955 * value)
	{
		___onPreSendRequest_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreSendRequest_41), (void*)value);
	}

	inline static int32_t get_offset_of__operatingSystem_54() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields, ____operatingSystem_54)); }
	inline String_t* get__operatingSystem_54() const { return ____operatingSystem_54; }
	inline String_t** get_address_of__operatingSystem_54() { return &____operatingSystem_54; }
	inline void set__operatingSystem_54(String_t* value)
	{
		____operatingSystem_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____operatingSystem_54), (void*)value);
	}

	inline static int32_t get_offset_of__deviceModel_55() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields, ____deviceModel_55)); }
	inline String_t* get__deviceModel_55() const { return ____deviceModel_55; }
	inline String_t** get_address_of__deviceModel_55() { return &____deviceModel_55; }
	inline void set__deviceModel_55(String_t* value)
	{
		____deviceModel_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____deviceModel_55), (void*)value);
	}

	inline static int32_t get_offset_of__appIdentifier_56() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields, ____appIdentifier_56)); }
	inline String_t* get__appIdentifier_56() const { return ____appIdentifier_56; }
	inline String_t** get_address_of__appIdentifier_56() { return &____appIdentifier_56; }
	inline void set__appIdentifier_56(String_t* value)
	{
		____appIdentifier_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____appIdentifier_56), (void*)value);
	}

	inline static int32_t get_offset_of__unityVersion_57() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields, ____unityVersion_57)); }
	inline String_t* get__unityVersion_57() const { return ____unityVersion_57; }
	inline String_t** get_address_of__unityVersion_57() { return &____unityVersion_57; }
	inline void set__unityVersion_57(String_t* value)
	{
		____unityVersion_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____unityVersion_57), (void*)value);
	}

	inline static int32_t get_offset_of_OnProvideCustomUserAgent_58() { return static_cast<int32_t>(offsetof(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622_StaticFields, ___OnProvideCustomUserAgent_58)); }
	inline Func_1_t2F3325DADD1F420568A48646BFC825E9F29472B1 * get_OnProvideCustomUserAgent_58() const { return ___OnProvideCustomUserAgent_58; }
	inline Func_1_t2F3325DADD1F420568A48646BFC825E9F29472B1 ** get_address_of_OnProvideCustomUserAgent_58() { return &___OnProvideCustomUserAgent_58; }
	inline void set_OnProvideCustomUserAgent_58(Func_1_t2F3325DADD1F420568A48646BFC825E9F29472B1 * value)
	{
		___OnProvideCustomUserAgent_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnProvideCustomUserAgent_58), (void*)value);
	}
};


// Facebook.WitAi.Events.WitResponseEvent
struct WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4  : public UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055
{
public:

public:
};


// Facebook.WitAi.Events.WitTranscriptionEvent
struct WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E  : public UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0
{
public:

public:
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.Predicate`1<Facebook.WitAi.Dictation.DictationService>
struct Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<Facebook.WitAi.Lib.WitResponseNode>
struct UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`2<System.String,System.String>
struct UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099  : public MulticastDelegate_t
{
public:

public:
};


// Facebook.WitAi.Data.Configuration.WitConfiguration
struct WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// Facebook.WitAi.Data.Configuration.WitApplication Facebook.WitAi.Data.Configuration.WitConfiguration::application
	WitApplication_t7A7F18E3FBD810923842318ADB5935CC9BBA2E89 * ___application_4;
	// System.String Facebook.WitAi.Data.Configuration.WitConfiguration::configId
	String_t* ___configId_5;
	// System.String Facebook.WitAi.Data.Configuration.WitConfiguration::clientAccessToken
	String_t* ___clientAccessToken_6;
	// System.Int32 Facebook.WitAi.Data.Configuration.WitConfiguration::timeoutMS
	int32_t ___timeoutMS_7;
	// Facebook.WitAi.Configuration.WitEndpointConfig Facebook.WitAi.Data.Configuration.WitConfiguration::endpointConfiguration
	WitEndpointConfig_t6974155BF4F518549C3D435F2DA4BC4077706731 * ___endpointConfiguration_8;
	// Facebook.WitAi.Data.Entities.WitEntity[] Facebook.WitAi.Data.Configuration.WitConfiguration::entities
	WitEntityU5BU5D_tA95536CFAD78C68FB33FDF0B3BFA1BD73302E66D* ___entities_9;
	// Facebook.WitAi.Data.Intents.WitIntent[] Facebook.WitAi.Data.Configuration.WitConfiguration::intents
	WitIntentU5BU5D_t41058B7A90DE88E8ED6B651A68997887FF21586C* ___intents_10;
	// Facebook.WitAi.Data.Traits.WitTrait[] Facebook.WitAi.Data.Configuration.WitConfiguration::traits
	WitTraitU5BU5D_tF4A72012100EA4C0AAB6B63A20D37197CA259702* ___traits_11;
	// System.Boolean Facebook.WitAi.Data.Configuration.WitConfiguration::isDemoOnly
	bool ___isDemoOnly_12;
	// System.Boolean Facebook.WitAi.Data.Configuration.WitConfiguration::useConduit
	bool ___useConduit_13;
	// System.String Facebook.WitAi.Data.Configuration.WitConfiguration::manifestLocalPath
	String_t* ___manifestLocalPath_14;
	// System.Boolean Facebook.WitAi.Data.Configuration.WitConfiguration::autoGenerateManifest
	bool ___autoGenerateManifest_15;
	// System.Boolean Facebook.WitAi.Data.Configuration.WitConfiguration::openManifestOnGeneration
	bool ___openManifestOnGeneration_16;

public:
	inline static int32_t get_offset_of_application_4() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___application_4)); }
	inline WitApplication_t7A7F18E3FBD810923842318ADB5935CC9BBA2E89 * get_application_4() const { return ___application_4; }
	inline WitApplication_t7A7F18E3FBD810923842318ADB5935CC9BBA2E89 ** get_address_of_application_4() { return &___application_4; }
	inline void set_application_4(WitApplication_t7A7F18E3FBD810923842318ADB5935CC9BBA2E89 * value)
	{
		___application_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___application_4), (void*)value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___configId_5), (void*)value);
	}

	inline static int32_t get_offset_of_clientAccessToken_6() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___clientAccessToken_6)); }
	inline String_t* get_clientAccessToken_6() const { return ___clientAccessToken_6; }
	inline String_t** get_address_of_clientAccessToken_6() { return &___clientAccessToken_6; }
	inline void set_clientAccessToken_6(String_t* value)
	{
		___clientAccessToken_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientAccessToken_6), (void*)value);
	}

	inline static int32_t get_offset_of_timeoutMS_7() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___timeoutMS_7)); }
	inline int32_t get_timeoutMS_7() const { return ___timeoutMS_7; }
	inline int32_t* get_address_of_timeoutMS_7() { return &___timeoutMS_7; }
	inline void set_timeoutMS_7(int32_t value)
	{
		___timeoutMS_7 = value;
	}

	inline static int32_t get_offset_of_endpointConfiguration_8() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___endpointConfiguration_8)); }
	inline WitEndpointConfig_t6974155BF4F518549C3D435F2DA4BC4077706731 * get_endpointConfiguration_8() const { return ___endpointConfiguration_8; }
	inline WitEndpointConfig_t6974155BF4F518549C3D435F2DA4BC4077706731 ** get_address_of_endpointConfiguration_8() { return &___endpointConfiguration_8; }
	inline void set_endpointConfiguration_8(WitEndpointConfig_t6974155BF4F518549C3D435F2DA4BC4077706731 * value)
	{
		___endpointConfiguration_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___endpointConfiguration_8), (void*)value);
	}

	inline static int32_t get_offset_of_entities_9() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___entities_9)); }
	inline WitEntityU5BU5D_tA95536CFAD78C68FB33FDF0B3BFA1BD73302E66D* get_entities_9() const { return ___entities_9; }
	inline WitEntityU5BU5D_tA95536CFAD78C68FB33FDF0B3BFA1BD73302E66D** get_address_of_entities_9() { return &___entities_9; }
	inline void set_entities_9(WitEntityU5BU5D_tA95536CFAD78C68FB33FDF0B3BFA1BD73302E66D* value)
	{
		___entities_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entities_9), (void*)value);
	}

	inline static int32_t get_offset_of_intents_10() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___intents_10)); }
	inline WitIntentU5BU5D_t41058B7A90DE88E8ED6B651A68997887FF21586C* get_intents_10() const { return ___intents_10; }
	inline WitIntentU5BU5D_t41058B7A90DE88E8ED6B651A68997887FF21586C** get_address_of_intents_10() { return &___intents_10; }
	inline void set_intents_10(WitIntentU5BU5D_t41058B7A90DE88E8ED6B651A68997887FF21586C* value)
	{
		___intents_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intents_10), (void*)value);
	}

	inline static int32_t get_offset_of_traits_11() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___traits_11)); }
	inline WitTraitU5BU5D_tF4A72012100EA4C0AAB6B63A20D37197CA259702* get_traits_11() const { return ___traits_11; }
	inline WitTraitU5BU5D_tF4A72012100EA4C0AAB6B63A20D37197CA259702** get_address_of_traits_11() { return &___traits_11; }
	inline void set_traits_11(WitTraitU5BU5D_tF4A72012100EA4C0AAB6B63A20D37197CA259702* value)
	{
		___traits_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___traits_11), (void*)value);
	}

	inline static int32_t get_offset_of_isDemoOnly_12() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___isDemoOnly_12)); }
	inline bool get_isDemoOnly_12() const { return ___isDemoOnly_12; }
	inline bool* get_address_of_isDemoOnly_12() { return &___isDemoOnly_12; }
	inline void set_isDemoOnly_12(bool value)
	{
		___isDemoOnly_12 = value;
	}

	inline static int32_t get_offset_of_useConduit_13() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___useConduit_13)); }
	inline bool get_useConduit_13() const { return ___useConduit_13; }
	inline bool* get_address_of_useConduit_13() { return &___useConduit_13; }
	inline void set_useConduit_13(bool value)
	{
		___useConduit_13 = value;
	}

	inline static int32_t get_offset_of_manifestLocalPath_14() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___manifestLocalPath_14)); }
	inline String_t* get_manifestLocalPath_14() const { return ___manifestLocalPath_14; }
	inline String_t** get_address_of_manifestLocalPath_14() { return &___manifestLocalPath_14; }
	inline void set_manifestLocalPath_14(String_t* value)
	{
		___manifestLocalPath_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___manifestLocalPath_14), (void*)value);
	}

	inline static int32_t get_offset_of_autoGenerateManifest_15() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___autoGenerateManifest_15)); }
	inline bool get_autoGenerateManifest_15() const { return ___autoGenerateManifest_15; }
	inline bool* get_address_of_autoGenerateManifest_15() { return &___autoGenerateManifest_15; }
	inline void set_autoGenerateManifest_15(bool value)
	{
		___autoGenerateManifest_15 = value;
	}

	inline static int32_t get_offset_of_openManifestOnGeneration_16() { return static_cast<int32_t>(offsetof(WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631, ___openManifestOnGeneration_16)); }
	inline bool get_openManifestOnGeneration_16() const { return ___openManifestOnGeneration_16; }
	inline bool* get_address_of_openManifestOnGeneration_16() { return &___openManifestOnGeneration_16; }
	inline void set_openManifestOnGeneration_16(bool value)
	{
		___openManifestOnGeneration_16 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// Facebook.WitAi.Events.UnityEventListeners.AudioEventListener
struct AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Facebook.WitAi.Events.WitMicLevelChangedEvent Facebook.WitAi.Events.UnityEventListeners.AudioEventListener::onMicAudioLevelChanged
	WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * ___onMicAudioLevelChanged_4;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.UnityEventListeners.AudioEventListener::onMicStartedListening
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onMicStartedListening_5;
	// UnityEngine.Events.UnityEvent Facebook.WitAi.Events.UnityEventListeners.AudioEventListener::onMicStoppedListening
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___onMicStoppedListening_6;
	// Facebook.WitAi.Interfaces.IAudioInputEvents Facebook.WitAi.Events.UnityEventListeners.AudioEventListener::_events
	RuntimeObject* ____events_7;

public:
	inline static int32_t get_offset_of_onMicAudioLevelChanged_4() { return static_cast<int32_t>(offsetof(AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78, ___onMicAudioLevelChanged_4)); }
	inline WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * get_onMicAudioLevelChanged_4() const { return ___onMicAudioLevelChanged_4; }
	inline WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B ** get_address_of_onMicAudioLevelChanged_4() { return &___onMicAudioLevelChanged_4; }
	inline void set_onMicAudioLevelChanged_4(WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * value)
	{
		___onMicAudioLevelChanged_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onMicAudioLevelChanged_4), (void*)value);
	}

	inline static int32_t get_offset_of_onMicStartedListening_5() { return static_cast<int32_t>(offsetof(AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78, ___onMicStartedListening_5)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onMicStartedListening_5() const { return ___onMicStartedListening_5; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onMicStartedListening_5() { return &___onMicStartedListening_5; }
	inline void set_onMicStartedListening_5(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onMicStartedListening_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onMicStartedListening_5), (void*)value);
	}

	inline static int32_t get_offset_of_onMicStoppedListening_6() { return static_cast<int32_t>(offsetof(AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78, ___onMicStoppedListening_6)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_onMicStoppedListening_6() const { return ___onMicStoppedListening_6; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_onMicStoppedListening_6() { return &___onMicStoppedListening_6; }
	inline void set_onMicStoppedListening_6(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___onMicStoppedListening_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onMicStoppedListening_6), (void*)value);
	}

	inline static int32_t get_offset_of__events_7() { return static_cast<int32_t>(offsetof(AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78, ____events_7)); }
	inline RuntimeObject* get__events_7() const { return ____events_7; }
	inline RuntimeObject** get_address_of__events_7() { return &____events_7; }
	inline void set__events_7(RuntimeObject* value)
	{
		____events_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____events_7), (void*)value);
	}
};


// Facebook.WitAi.ServiceReferences.AudioInputServiceReference
struct AudioInputServiceReference_tB04E44B3DFE15FB3AAE393359CE8F56BADD2C063  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Facebook.WitAi.Dictation.DictationService
struct DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Facebook.WitAi.Dictation.Events.DictationEvents Facebook.WitAi.Dictation.DictationService::dictationEvents
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * ___dictationEvents_4;

public:
	inline static int32_t get_offset_of_dictationEvents_4() { return static_cast<int32_t>(offsetof(DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA, ___dictationEvents_4)); }
	inline DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * get_dictationEvents_4() const { return ___dictationEvents_4; }
	inline DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C ** get_address_of_dictationEvents_4() { return &___dictationEvents_4; }
	inline void set_dictationEvents_4(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * value)
	{
		___dictationEvents_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dictationEvents_4), (void*)value);
	}
};


// Facebook.WitAi.Dictation.MultiRequestTranscription
struct MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Facebook.WitAi.Dictation.WitDictation Facebook.WitAi.Dictation.MultiRequestTranscription::witDictation
	WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * ___witDictation_4;
	// System.Int32 Facebook.WitAi.Dictation.MultiRequestTranscription::linesBetweenActivations
	int32_t ___linesBetweenActivations_5;
	// System.String Facebook.WitAi.Dictation.MultiRequestTranscription::activationSeparator
	String_t* ___activationSeparator_6;
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.MultiRequestTranscription::onTranscriptionUpdated
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onTranscriptionUpdated_7;
	// System.Text.StringBuilder Facebook.WitAi.Dictation.MultiRequestTranscription::_text
	StringBuilder_t * ____text_8;
	// System.String Facebook.WitAi.Dictation.MultiRequestTranscription::_activeText
	String_t* ____activeText_9;
	// System.Boolean Facebook.WitAi.Dictation.MultiRequestTranscription::_newSection
	bool ____newSection_10;
	// System.Text.StringBuilder Facebook.WitAi.Dictation.MultiRequestTranscription::_separator
	StringBuilder_t * ____separator_11;

public:
	inline static int32_t get_offset_of_witDictation_4() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ___witDictation_4)); }
	inline WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * get_witDictation_4() const { return ___witDictation_4; }
	inline WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 ** get_address_of_witDictation_4() { return &___witDictation_4; }
	inline void set_witDictation_4(WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * value)
	{
		___witDictation_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___witDictation_4), (void*)value);
	}

	inline static int32_t get_offset_of_linesBetweenActivations_5() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ___linesBetweenActivations_5)); }
	inline int32_t get_linesBetweenActivations_5() const { return ___linesBetweenActivations_5; }
	inline int32_t* get_address_of_linesBetweenActivations_5() { return &___linesBetweenActivations_5; }
	inline void set_linesBetweenActivations_5(int32_t value)
	{
		___linesBetweenActivations_5 = value;
	}

	inline static int32_t get_offset_of_activationSeparator_6() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ___activationSeparator_6)); }
	inline String_t* get_activationSeparator_6() const { return ___activationSeparator_6; }
	inline String_t** get_address_of_activationSeparator_6() { return &___activationSeparator_6; }
	inline void set_activationSeparator_6(String_t* value)
	{
		___activationSeparator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activationSeparator_6), (void*)value);
	}

	inline static int32_t get_offset_of_onTranscriptionUpdated_7() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ___onTranscriptionUpdated_7)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onTranscriptionUpdated_7() const { return ___onTranscriptionUpdated_7; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onTranscriptionUpdated_7() { return &___onTranscriptionUpdated_7; }
	inline void set_onTranscriptionUpdated_7(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onTranscriptionUpdated_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onTranscriptionUpdated_7), (void*)value);
	}

	inline static int32_t get_offset_of__text_8() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ____text_8)); }
	inline StringBuilder_t * get__text_8() const { return ____text_8; }
	inline StringBuilder_t ** get_address_of__text_8() { return &____text_8; }
	inline void set__text_8(StringBuilder_t * value)
	{
		____text_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____text_8), (void*)value);
	}

	inline static int32_t get_offset_of__activeText_9() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ____activeText_9)); }
	inline String_t* get__activeText_9() const { return ____activeText_9; }
	inline String_t** get_address_of__activeText_9() { return &____activeText_9; }
	inline void set__activeText_9(String_t* value)
	{
		____activeText_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeText_9), (void*)value);
	}

	inline static int32_t get_offset_of__newSection_10() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ____newSection_10)); }
	inline bool get__newSection_10() const { return ____newSection_10; }
	inline bool* get_address_of__newSection_10() { return &____newSection_10; }
	inline void set__newSection_10(bool value)
	{
		____newSection_10 = value;
	}

	inline static int32_t get_offset_of__separator_11() { return static_cast<int32_t>(offsetof(MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293, ____separator_11)); }
	inline StringBuilder_t * get__separator_11() const { return ____separator_11; }
	inline StringBuilder_t ** get_address_of__separator_11() { return &____separator_11; }
	inline void set__separator_11(StringBuilder_t * value)
	{
		____separator_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____separator_11), (void*)value);
	}
};


// Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener
struct TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener::onPartialTranscription
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onPartialTranscription_4;
	// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener::onFullTranscription
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * ___onFullTranscription_5;
	// Facebook.WitAi.Interfaces.ITranscriptionEvent Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener::_events
	RuntimeObject* ____events_6;

public:
	inline static int32_t get_offset_of_onPartialTranscription_4() { return static_cast<int32_t>(offsetof(TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5, ___onPartialTranscription_4)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onPartialTranscription_4() const { return ___onPartialTranscription_4; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onPartialTranscription_4() { return &___onPartialTranscription_4; }
	inline void set_onPartialTranscription_4(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onPartialTranscription_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPartialTranscription_4), (void*)value);
	}

	inline static int32_t get_offset_of_onFullTranscription_5() { return static_cast<int32_t>(offsetof(TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5, ___onFullTranscription_5)); }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * get_onFullTranscription_5() const { return ___onFullTranscription_5; }
	inline WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E ** get_address_of_onFullTranscription_5() { return &___onFullTranscription_5; }
	inline void set_onFullTranscription_5(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * value)
	{
		___onFullTranscription_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onFullTranscription_5), (void*)value);
	}

	inline static int32_t get_offset_of__events_6() { return static_cast<int32_t>(offsetof(TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5, ____events_6)); }
	inline RuntimeObject* get__events_6() const { return ____events_6; }
	inline RuntimeObject** get_address_of__events_6() { return &____events_6; }
	inline void set__events_6(RuntimeObject* value)
	{
		____events_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____events_6), (void*)value);
	}
};


// Facebook.WitAi.WitService
struct WitService_t860537723698CF0607466342346F3B1FECA68DCB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Facebook.WitAi.Configuration.WitRequestOptions Facebook.WitAi.WitService::_currentRequestOptions
	WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ____currentRequestOptions_4;
	// System.Single Facebook.WitAi.WitService::_lastMinVolumeLevelTime
	float ____lastMinVolumeLevelTime_5;
	// Facebook.WitAi.WitRequest Facebook.WitAi.WitService::_recordingRequest
	WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 * ____recordingRequest_6;
	// System.Boolean Facebook.WitAi.WitService::_isSoundWakeActive
	bool ____isSoundWakeActive_7;
	// Facebook.WitAi.Data.RingBuffer`1/Marker<System.Byte> Facebook.WitAi.WitService::_lastSampleMarker
	Marker_tA491115B38EBB1FED9D61541569F1A7254D5E42A * ____lastSampleMarker_8;
	// System.Boolean Facebook.WitAi.WitService::_minKeepAliveWasHit
	bool ____minKeepAliveWasHit_9;
	// System.Boolean Facebook.WitAi.WitService::_isActive
	bool ____isActive_10;
	// System.Int64 Facebook.WitAi.WitService::_minSampleByteCount
	int64_t ____minSampleByteCount_11;
	// Facebook.WitAi.IVoiceEventProvider Facebook.WitAi.WitService::_voiceEventProvider
	RuntimeObject* ____voiceEventProvider_12;
	// Facebook.WitAi.IWitRuntimeConfigProvider Facebook.WitAi.WitService::_runtimeConfigProvider
	RuntimeObject* ____runtimeConfigProvider_13;
	// Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.WitService::_activeTranscriptionProvider
	RuntimeObject* ____activeTranscriptionProvider_14;
	// UnityEngine.Coroutine Facebook.WitAi.WitService::_timeLimitCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____timeLimitCoroutine_15;
	// Facebook.WitAi.Interfaces.IWitRequestProvider Facebook.WitAi.WitService::_witRequestProvider
	RuntimeObject* ____witRequestProvider_16;
	// System.Boolean Facebook.WitAi.WitService::_receivedTranscription
	bool ____receivedTranscription_17;
	// System.Single Facebook.WitAi.WitService::_lastWordTime
	float ____lastWordTime_18;
	// System.Collections.Generic.HashSet`1<Facebook.WitAi.WitRequest> Facebook.WitAi.WitService::_transmitRequests
	HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC * ____transmitRequests_19;
	// System.Collections.Generic.HashSet`1<Facebook.WitAi.WitRequest> Facebook.WitAi.WitService::_queuedRequests
	HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC * ____queuedRequests_20;
	// UnityEngine.Coroutine Facebook.WitAi.WitService::_queueHandler
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____queueHandler_21;
	// Facebook.WitAi.Events.IWitByteDataReadyHandler[] Facebook.WitAi.WitService::_dataReadyHandlers
	IWitByteDataReadyHandlerU5BU5D_tDAF18FFFEDA7AD9375B6460D05A3F76B7B129180* ____dataReadyHandlers_22;
	// Facebook.WitAi.Events.IWitByteDataSentHandler[] Facebook.WitAi.WitService::_dataSentHandlers
	IWitByteDataSentHandlerU5BU5D_t50A06A7E11B774416F56D27DCF1E85671DA8D804* ____dataSentHandlers_23;
	// Facebook.WitAi.Interfaces.IDynamicEntitiesProvider[] Facebook.WitAi.WitService::_dynamicEntityProviders
	IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107* ____dynamicEntityProviders_24;

public:
	inline static int32_t get_offset_of__currentRequestOptions_4() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____currentRequestOptions_4)); }
	inline WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * get__currentRequestOptions_4() const { return ____currentRequestOptions_4; }
	inline WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F ** get_address_of__currentRequestOptions_4() { return &____currentRequestOptions_4; }
	inline void set__currentRequestOptions_4(WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * value)
	{
		____currentRequestOptions_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentRequestOptions_4), (void*)value);
	}

	inline static int32_t get_offset_of__lastMinVolumeLevelTime_5() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____lastMinVolumeLevelTime_5)); }
	inline float get__lastMinVolumeLevelTime_5() const { return ____lastMinVolumeLevelTime_5; }
	inline float* get_address_of__lastMinVolumeLevelTime_5() { return &____lastMinVolumeLevelTime_5; }
	inline void set__lastMinVolumeLevelTime_5(float value)
	{
		____lastMinVolumeLevelTime_5 = value;
	}

	inline static int32_t get_offset_of__recordingRequest_6() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____recordingRequest_6)); }
	inline WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 * get__recordingRequest_6() const { return ____recordingRequest_6; }
	inline WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 ** get_address_of__recordingRequest_6() { return &____recordingRequest_6; }
	inline void set__recordingRequest_6(WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 * value)
	{
		____recordingRequest_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____recordingRequest_6), (void*)value);
	}

	inline static int32_t get_offset_of__isSoundWakeActive_7() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____isSoundWakeActive_7)); }
	inline bool get__isSoundWakeActive_7() const { return ____isSoundWakeActive_7; }
	inline bool* get_address_of__isSoundWakeActive_7() { return &____isSoundWakeActive_7; }
	inline void set__isSoundWakeActive_7(bool value)
	{
		____isSoundWakeActive_7 = value;
	}

	inline static int32_t get_offset_of__lastSampleMarker_8() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____lastSampleMarker_8)); }
	inline Marker_tA491115B38EBB1FED9D61541569F1A7254D5E42A * get__lastSampleMarker_8() const { return ____lastSampleMarker_8; }
	inline Marker_tA491115B38EBB1FED9D61541569F1A7254D5E42A ** get_address_of__lastSampleMarker_8() { return &____lastSampleMarker_8; }
	inline void set__lastSampleMarker_8(Marker_tA491115B38EBB1FED9D61541569F1A7254D5E42A * value)
	{
		____lastSampleMarker_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastSampleMarker_8), (void*)value);
	}

	inline static int32_t get_offset_of__minKeepAliveWasHit_9() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____minKeepAliveWasHit_9)); }
	inline bool get__minKeepAliveWasHit_9() const { return ____minKeepAliveWasHit_9; }
	inline bool* get_address_of__minKeepAliveWasHit_9() { return &____minKeepAliveWasHit_9; }
	inline void set__minKeepAliveWasHit_9(bool value)
	{
		____minKeepAliveWasHit_9 = value;
	}

	inline static int32_t get_offset_of__isActive_10() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____isActive_10)); }
	inline bool get__isActive_10() const { return ____isActive_10; }
	inline bool* get_address_of__isActive_10() { return &____isActive_10; }
	inline void set__isActive_10(bool value)
	{
		____isActive_10 = value;
	}

	inline static int32_t get_offset_of__minSampleByteCount_11() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____minSampleByteCount_11)); }
	inline int64_t get__minSampleByteCount_11() const { return ____minSampleByteCount_11; }
	inline int64_t* get_address_of__minSampleByteCount_11() { return &____minSampleByteCount_11; }
	inline void set__minSampleByteCount_11(int64_t value)
	{
		____minSampleByteCount_11 = value;
	}

	inline static int32_t get_offset_of__voiceEventProvider_12() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____voiceEventProvider_12)); }
	inline RuntimeObject* get__voiceEventProvider_12() const { return ____voiceEventProvider_12; }
	inline RuntimeObject** get_address_of__voiceEventProvider_12() { return &____voiceEventProvider_12; }
	inline void set__voiceEventProvider_12(RuntimeObject* value)
	{
		____voiceEventProvider_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____voiceEventProvider_12), (void*)value);
	}

	inline static int32_t get_offset_of__runtimeConfigProvider_13() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____runtimeConfigProvider_13)); }
	inline RuntimeObject* get__runtimeConfigProvider_13() const { return ____runtimeConfigProvider_13; }
	inline RuntimeObject** get_address_of__runtimeConfigProvider_13() { return &____runtimeConfigProvider_13; }
	inline void set__runtimeConfigProvider_13(RuntimeObject* value)
	{
		____runtimeConfigProvider_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____runtimeConfigProvider_13), (void*)value);
	}

	inline static int32_t get_offset_of__activeTranscriptionProvider_14() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____activeTranscriptionProvider_14)); }
	inline RuntimeObject* get__activeTranscriptionProvider_14() const { return ____activeTranscriptionProvider_14; }
	inline RuntimeObject** get_address_of__activeTranscriptionProvider_14() { return &____activeTranscriptionProvider_14; }
	inline void set__activeTranscriptionProvider_14(RuntimeObject* value)
	{
		____activeTranscriptionProvider_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____activeTranscriptionProvider_14), (void*)value);
	}

	inline static int32_t get_offset_of__timeLimitCoroutine_15() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____timeLimitCoroutine_15)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__timeLimitCoroutine_15() const { return ____timeLimitCoroutine_15; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__timeLimitCoroutine_15() { return &____timeLimitCoroutine_15; }
	inline void set__timeLimitCoroutine_15(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____timeLimitCoroutine_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____timeLimitCoroutine_15), (void*)value);
	}

	inline static int32_t get_offset_of__witRequestProvider_16() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____witRequestProvider_16)); }
	inline RuntimeObject* get__witRequestProvider_16() const { return ____witRequestProvider_16; }
	inline RuntimeObject** get_address_of__witRequestProvider_16() { return &____witRequestProvider_16; }
	inline void set__witRequestProvider_16(RuntimeObject* value)
	{
		____witRequestProvider_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____witRequestProvider_16), (void*)value);
	}

	inline static int32_t get_offset_of__receivedTranscription_17() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____receivedTranscription_17)); }
	inline bool get__receivedTranscription_17() const { return ____receivedTranscription_17; }
	inline bool* get_address_of__receivedTranscription_17() { return &____receivedTranscription_17; }
	inline void set__receivedTranscription_17(bool value)
	{
		____receivedTranscription_17 = value;
	}

	inline static int32_t get_offset_of__lastWordTime_18() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____lastWordTime_18)); }
	inline float get__lastWordTime_18() const { return ____lastWordTime_18; }
	inline float* get_address_of__lastWordTime_18() { return &____lastWordTime_18; }
	inline void set__lastWordTime_18(float value)
	{
		____lastWordTime_18 = value;
	}

	inline static int32_t get_offset_of__transmitRequests_19() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____transmitRequests_19)); }
	inline HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC * get__transmitRequests_19() const { return ____transmitRequests_19; }
	inline HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC ** get_address_of__transmitRequests_19() { return &____transmitRequests_19; }
	inline void set__transmitRequests_19(HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC * value)
	{
		____transmitRequests_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transmitRequests_19), (void*)value);
	}

	inline static int32_t get_offset_of__queuedRequests_20() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____queuedRequests_20)); }
	inline HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC * get__queuedRequests_20() const { return ____queuedRequests_20; }
	inline HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC ** get_address_of__queuedRequests_20() { return &____queuedRequests_20; }
	inline void set__queuedRequests_20(HashSet_1_tFF3E55D5960B4D36FA35D840BD86C262E153D8BC * value)
	{
		____queuedRequests_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____queuedRequests_20), (void*)value);
	}

	inline static int32_t get_offset_of__queueHandler_21() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____queueHandler_21)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__queueHandler_21() const { return ____queueHandler_21; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__queueHandler_21() { return &____queueHandler_21; }
	inline void set__queueHandler_21(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____queueHandler_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____queueHandler_21), (void*)value);
	}

	inline static int32_t get_offset_of__dataReadyHandlers_22() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____dataReadyHandlers_22)); }
	inline IWitByteDataReadyHandlerU5BU5D_tDAF18FFFEDA7AD9375B6460D05A3F76B7B129180* get__dataReadyHandlers_22() const { return ____dataReadyHandlers_22; }
	inline IWitByteDataReadyHandlerU5BU5D_tDAF18FFFEDA7AD9375B6460D05A3F76B7B129180** get_address_of__dataReadyHandlers_22() { return &____dataReadyHandlers_22; }
	inline void set__dataReadyHandlers_22(IWitByteDataReadyHandlerU5BU5D_tDAF18FFFEDA7AD9375B6460D05A3F76B7B129180* value)
	{
		____dataReadyHandlers_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dataReadyHandlers_22), (void*)value);
	}

	inline static int32_t get_offset_of__dataSentHandlers_23() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____dataSentHandlers_23)); }
	inline IWitByteDataSentHandlerU5BU5D_t50A06A7E11B774416F56D27DCF1E85671DA8D804* get__dataSentHandlers_23() const { return ____dataSentHandlers_23; }
	inline IWitByteDataSentHandlerU5BU5D_t50A06A7E11B774416F56D27DCF1E85671DA8D804** get_address_of__dataSentHandlers_23() { return &____dataSentHandlers_23; }
	inline void set__dataSentHandlers_23(IWitByteDataSentHandlerU5BU5D_t50A06A7E11B774416F56D27DCF1E85671DA8D804* value)
	{
		____dataSentHandlers_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dataSentHandlers_23), (void*)value);
	}

	inline static int32_t get_offset_of__dynamicEntityProviders_24() { return static_cast<int32_t>(offsetof(WitService_t860537723698CF0607466342346F3B1FECA68DCB, ____dynamicEntityProviders_24)); }
	inline IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107* get__dynamicEntityProviders_24() const { return ____dynamicEntityProviders_24; }
	inline IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107** get_address_of__dynamicEntityProviders_24() { return &____dynamicEntityProviders_24; }
	inline void set__dynamicEntityProviders_24(IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107* value)
	{
		____dynamicEntityProviders_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicEntityProviders_24), (void*)value);
	}
};


// Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference
struct DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869  : public AudioInputServiceReference_tB04E44B3DFE15FB3AAE393359CE8F56BADD2C063
{
public:
	// Facebook.WitAi.Utilities.DictationServiceReference Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference::_dictationServiceReference
	DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411  ____dictationServiceReference_4;

public:
	inline static int32_t get_offset_of__dictationServiceReference_4() { return static_cast<int32_t>(offsetof(DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869, ____dictationServiceReference_4)); }
	inline DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411  get__dictationServiceReference_4() const { return ____dictationServiceReference_4; }
	inline DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * get_address_of__dictationServiceReference_4() { return &____dictationServiceReference_4; }
	inline void set__dictationServiceReference_4(DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411  value)
	{
		____dictationServiceReference_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____dictationServiceReference_4))->___dictationService_0), (void*)NULL);
	}
};


// Facebook.WitAi.Dictation.WitDictation
struct WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866  : public DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA
{
public:
	// Facebook.WitAi.Configuration.WitRuntimeConfiguration Facebook.WitAi.Dictation.WitDictation::witRuntimeConfiguration
	WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * ___witRuntimeConfiguration_5;
	// Facebook.WitAi.WitService Facebook.WitAi.Dictation.WitDictation::witService
	WitService_t860537723698CF0607466342346F3B1FECA68DCB * ___witService_6;
	// Facebook.WitAi.Events.VoiceEvents Facebook.WitAi.Dictation.WitDictation::voiceEvents
	VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * ___voiceEvents_7;

public:
	inline static int32_t get_offset_of_witRuntimeConfiguration_5() { return static_cast<int32_t>(offsetof(WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866, ___witRuntimeConfiguration_5)); }
	inline WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * get_witRuntimeConfiguration_5() const { return ___witRuntimeConfiguration_5; }
	inline WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 ** get_address_of_witRuntimeConfiguration_5() { return &___witRuntimeConfiguration_5; }
	inline void set_witRuntimeConfiguration_5(WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * value)
	{
		___witRuntimeConfiguration_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___witRuntimeConfiguration_5), (void*)value);
	}

	inline static int32_t get_offset_of_witService_6() { return static_cast<int32_t>(offsetof(WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866, ___witService_6)); }
	inline WitService_t860537723698CF0607466342346F3B1FECA68DCB * get_witService_6() const { return ___witService_6; }
	inline WitService_t860537723698CF0607466342346F3B1FECA68DCB ** get_address_of_witService_6() { return &___witService_6; }
	inline void set_witService_6(WitService_t860537723698CF0607466342346F3B1FECA68DCB * value)
	{
		___witService_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___witService_6), (void*)value);
	}

	inline static int32_t get_offset_of_voiceEvents_7() { return static_cast<int32_t>(offsetof(WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866, ___voiceEvents_7)); }
	inline VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * get_voiceEvents_7() const { return ___voiceEvents_7; }
	inline VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D ** get_address_of_voiceEvents_7() { return &___voiceEvents_7; }
	inline void set_voiceEvents_7(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * value)
	{
		___voiceEvents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voiceEvents_7), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Facebook.WitAi.Dictation.DictationService[]
struct DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * m_Items[1];

public:
	inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Facebook.WitAi.Interfaces.IDynamicEntitiesProvider[]
struct IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

public:
	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Resources::FindObjectsOfTypeAll<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Resources_FindObjectsOfTypeAll_TisRuntimeObject_m8D2DBE0C15FEBF6C7B9706DBF52503080ADFC14F_gshared (const RuntimeMethod* method);
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared (Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 System.Array::Find<System.Object>(!!0[],System.Predicate`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Array_Find_TisRuntimeObject_m5F5DB46E68DD5A65AFC4306E2F3052D000135CB4_gshared (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___array0, Predicate_1_t5C96B81B31A697B11C4C3767E3298773AF25DFEB * ___match1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared (UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m7137356547ADC5089A381F0EC5E9280576983E2E_gshared (const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_mDACAB67F7E76FF788C30CA0E51BF3274666F951E_gshared (UnityAction_1_t00EE92422CBB066CEAB95CDDBF901E2967EC7B1A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_AddListener_m14DAE292BCF77B088359410E4C12071936DB681D_gshared (UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F * __this, UnityAction_1_t00EE92422CBB066CEAB95CDDBF901E2967EC7B1A * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_RemoveListener_m793372F5AF1175F5DD348F908874E7D607B16DBD_gshared (UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F * __this, UnityAction_1_t00EE92422CBB066CEAB95CDDBF901E2967EC7B1A * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Object>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_Invoke_m73C0FE7D4CDD8627332257E8503F2E9862E33C3E_gshared (UnityEvent_1_t32063FE815890FF672DF76288FAC4ABE089B899F * __this, RuntimeObject * ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_gshared (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_gshared (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_2__ctor_m8727842F47B6F77FCB70DE281A21C3E1DD2C7B5E_gshared (UnityAction_2_tEA79D6DFB08A416619D920D80581B3A7C1376CCD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::AddListener(UnityEngine.Events.UnityAction`2<!0,!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_2_AddListener_m03A808706EF8B435537D817F2A43FD453E639D6C_gshared (UnityEvent_2_t28592AD5CBF18EB6ED3BE1B15D588E132DA53582 * __this, UnityAction_2_tEA79D6DFB08A416619D920D80581B3A7C1376CCD * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3_gshared (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::RemoveListener(UnityEngine.Events.UnityAction`2<!0,!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_2_RemoveListener_mE340477D10D41DB3D0011507846998A5369C8E9F_gshared (UnityEvent_2_t28592AD5CBF18EB6ED3BE1B15D588E132DA53582 * __this, UnityAction_2_tEA79D6DFB08A416619D920D80581B3A7C1376CCD * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E_gshared (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, float ___arg00, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`2<System.Object,System.Object>::Invoke(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_2_Invoke_mBF66265092F853A13F5698ED2B62F0ADA48E4F0A_gshared (UnityEvent_2_t28592AD5CBF18EB6ED3BE1B15D588E132DA53582 * __this, RuntimeObject * ___arg00, RuntimeObject * ___arg11, const RuntimeMethod* method);

// System.Void Facebook.WitAi.Events.WitTranscriptionEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitTranscriptionEvent__ctor_m4C7714B0A4C8E8682E853DC56033152EC96A10F2 (WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Events.WitResponseEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitResponseEvent__ctor_mD80B232078883242636A37FE1A09C7832E857189 (WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Events.WitErrorEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitErrorEvent__ctor_mCE476BA3F0DC6EEAA983171C144FA9E3D16CE8F9 (WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Dictation.Events.DictationSessionEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationSessionEvent__ctor_m7768767815824DE024DA764BB9A3FAE03FD90FCF (DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Events.WitMicLevelChangedEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitMicLevelChangedEvent__ctor_m8BFD07A7BACC84E34DE90900669265E41CB9DE6F (WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Events.EventRegistry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegistry__ctor_mEABA3809EFD2BAD4526FBCB3297E9D14BAB184CC (EventRegistry_t30E7E31E8D1DD574DC5B07505206FEF48506DE6A * __this, const RuntimeMethod* method);
// Facebook.WitAi.Dictation.Events.DictationEvents Facebook.WitAi.Dictation.DictationService::get_DictationEvents()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Facebook.WitAi.Events.UnityEventListeners.AudioEventListener>()
inline AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78 * Component_GetComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_m46F4C63848A93702DBA41F4E4881F698B7A03B01 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<Facebook.WitAi.Events.UnityEventListeners.AudioEventListener>()
inline AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78 * GameObject_AddComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_mFA61385BD0582696C51D31B3024801A2A801A527 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener>()
inline TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5 * Component_GetComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m7E226A153B7C6780B3BCDA0C387355914CD83B2B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::AddComponent<Facebook.WitAi.Events.UnityEventListeners.TranscriptionEventListener>()
inline TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5 * GameObject_AddComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m6A4BBAA3243EF2BF1E35E4A2C0E1187379E5A35B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Void Facebook.WitAi.Dictation.Events.DictationEvents::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationEvents__ctor_m6405DC5FA653EAE2F23D4985272DBA57AC31B02C (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// Facebook.WitAi.Dictation.DictationService Facebook.WitAi.Utilities.DictationServiceReference::get_DictationService()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D (DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * __this, const RuntimeMethod* method);
// Facebook.WitAi.Interfaces.IAudioInputEvents Facebook.WitAi.Dictation.DictationService::get_AudioEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DictationService_get_AudioEvents_mF0A1F319ECBCF7728A29C24099424829A2601AF6 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.ServiceReferences.AudioInputServiceReference::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AudioInputServiceReference__ctor_m5E534615D8949DC89AB69E594A3E9D6784716251 (AudioInputServiceReference_tB04E44B3DFE15FB3AAE393359CE8F56BADD2C063 * __this, const RuntimeMethod* method);
// !!0[] UnityEngine.Resources::FindObjectsOfTypeAll<Facebook.WitAi.Dictation.DictationService>()
inline DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* Resources_FindObjectsOfTypeAll_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mBEEC450FD16C11556668039B77E33D516147B175 (const RuntimeMethod* method)
{
	return ((  DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* (*) (const RuntimeMethod*))Resources_FindObjectsOfTypeAll_TisRuntimeObject_m8D2DBE0C15FEBF6C7B9706DBF52503080ADFC14F_gshared)(method);
}
// System.Void System.Predicate`1<Facebook.WitAi.Dictation.DictationService>::.ctor(System.Object,System.IntPtr)
inline void Predicate_1__ctor_mD497F949F2BED7397F05D0D036703EDCBCBEE67B (Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Predicate_1__ctor_m3F41E32C976C3C48B3FC63FBFD3FBBC5B5F23EDD_gshared)(__this, ___object0, ___method1, method);
}
// !!0 System.Array::Find<Facebook.WitAi.Dictation.DictationService>(!!0[],System.Predicate`1<!!0>)
inline DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * Array_Find_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mF24CEF963E25E3FBD9911167D4C6AECF3B5F769A (DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* ___array0, Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * ___match1, const RuntimeMethod* method)
{
	return ((  DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * (*) (DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36*, Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 *, const RuntimeMethod*))Array_Find_TisRuntimeObject_m5F5DB46E68DD5A65AFC4306E2F3052D000135CB4_gshared)(___array0, ___match1, method);
}
// System.Void Facebook.WitAi.Data.VoiceSession::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceSession__ctor_m18C2A045C7547678A82F84FEA47064CD306041E0 (VoiceSession_t31BC7643895FC4F664729F340CCC3CF1CBC463F8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Dictation.Data.DictationSession>::.ctor()
inline void UnityEvent_1__ctor_m09B74417C0662618691C12E55F1FC11718629511 (UnityEvent_1_t8B99F019C2E27198664DEEC6FE760B111EC0CADA * __this, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t8B99F019C2E27198664DEEC6FE760B111EC0CADA *, const RuntimeMethod*))UnityEvent_1__ctor_mD87552C18A41196B69A62A366C8238FC246B151A_gshared)(__this, method);
}
// !!0 UnityEngine.Object::FindObjectOfType<Facebook.WitAi.Dictation.WitDictation>()
inline WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * Object_FindObjectOfType_TisWitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_m4A0F4B51FFEF50647C9280DBB5707561E01A1931 (const RuntimeMethod* method)
{
	return ((  WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m7137356547ADC5089A381F0EC5E9280576983E2E_gshared)(method);
}
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::AppendLine()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_AppendLine_mB5790BC98389118626505708AE683AE9257B91B2 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m57429705D977ACD5EE7E210A858EED6F348C36B3 (String_t* ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// Facebook.WitAi.Events.VoiceEvents Facebook.WitAi.Dictation.WitDictation::get_VoiceEvents()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method);
// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Events.VoiceEvents::get_OnFullTranscription()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * VoiceEvents_get_OnFullTranscription_m2F3454ABA3D28D8BCA75778AC33D5AC2B93EEA28_inline (VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.String>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_mDACAB67F7E76FF788C30CA0E51BF3274666F951E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398 (UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0 * __this, UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0 *, UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *, const RuntimeMethod*))UnityEvent_1_AddListener_m14DAE292BCF77B088359410E4C12071936DB681D_gshared)(__this, ___call0, method);
}
// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Events.VoiceEvents::get_OnPartialTranscription()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * VoiceEvents_get_OnPartialTranscription_m42A410529487FA1E9A6BA3104E4CE6E2063E82A7_inline (VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC (UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0 * __this, UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0 *, UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *, const RuntimeMethod*))UnityEvent_1_RemoveListener_m793372F5AF1175F5DD348F908874E7D607B16DBD_gshared)(__this, ___call0, method);
}
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnTranscriptionUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3 (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method);
// System.Int32 System.Text.StringBuilder::get_Length()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StringBuilder_get_Length_m680500263C59ACFD9582BF2AEEED8E92C87FF5C0 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_m545FFB72A578320B1D6EA3772160353FD62C344F (StringBuilder_t * __this, RuntimeObject * ___value0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Clear_m3D1F9F2F9EBA938807B7667DC2021D882B9B8FA1 (StringBuilder_t * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.String>::Invoke(!0)
inline void UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10 (UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0 * __this, String_t* ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t208A952325F66BFCB1EDEECEFEF5F1C7A16298A0 *, String_t*, const RuntimeMethod*))UnityEvent_1_Invoke_m73C0FE7D4CDD8627332257E8503F2E9862E33C3E_gshared)(__this, ___arg00, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean Facebook.WitAi.WitService::get_Active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitService_get_Active_m2B1C16788ACC3FC51B4132AA3461700F35F64DCF (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// System.Boolean Facebook.WitAi.WitService::get_IsRequestActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitService_get_IsRequestActive_m7DCDC5C172F24DC0E04F70B72A877EF6B228368F (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.WitService::get_TranscriptionProvider()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* WitService_get_TranscriptionProvider_mC46CAEC37C328B150C73681220D20AD06928923E_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_set_TranscriptionProvider_mEEA1B221B7F0B8DEB40680EF4FE8F0FDFCE7BA60 (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Boolean Facebook.WitAi.WitService::get_MicActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitService_get_MicActive_m68C2DAFDA763A6951534CC5F32F7CBB166B8C238 (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// Facebook.WitAi.WitRequest Facebook.WitAi.WitRequestFactory::DictationRequest(Facebook.WitAi.Data.Configuration.WitConfiguration,Facebook.WitAi.Configuration.WitRequestOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 * WitRequestFactory_DictationRequest_m004339AFD5A82B9892D53EDA1F2AA30589429BC6 (WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * ___config0, WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ___requestOptions1, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::Activate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_Activate_m43131CA4582F6D98DF18810AAC33D44030DBBA3F (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_Activate_mDC1F29D19E6286DB385F30758631D14F53232FB4 (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ___requestOptions0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::ActivateImmediately()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_ActivateImmediately_mB89815E8D4F8F369FC268A3BC0B3131559D8F979 (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_ActivateImmediately_mA8E4D8255A1329F05B89619F01D31B7DD15EDAD3 (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ___requestOptions0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_Deactivate_mD4C8DAC287BE9F20EFF59C964A8BAF27D141E30E (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::DeactivateAndAbortRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitService_DeactivateAndAbortRequest_mC72612AF886B4C5247F95BD675ACFEA448A1476F (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Dictation.DictationService::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_Awake_m7D507E8B054AC4BCE0E8AF58AA90887FC1C21749 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::AddComponent<Facebook.WitAi.WitService>()
inline WitService_t860537723698CF0607466342346F3B1FECA68DCB * GameObject_AddComponent_TisWitService_t860537723698CF0607466342346F3B1FECA68DCB_mD5C2810AD85ACA4F42F27CE5813E283611FE0A9F (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  WitService_t860537723698CF0607466342346F3B1FECA68DCB * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_AddComponent_TisRuntimeObject_mBDBD6EC58A4409E35E4C5D08757C36E4938256B1_gshared)(__this, method);
}
// System.Void Facebook.WitAi.WitService::set_VoiceEventProvider(Facebook.WitAi.IVoiceEventProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WitService_set_VoiceEventProvider_m3DFFC7C2FD3C5DBB86407D8C7CB0FA678805E9DC_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::set_ConfigurationProvider(Facebook.WitAi.IWitRuntimeConfigProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WitService_set_ConfigurationProvider_m292F41D9B59A9CF42D986ACBBB4FD38569E06195_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.WitService::set_WitRequestProvider(Facebook.WitAi.Interfaces.IWitRequestProvider)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WitService_set_WitRequestProvider_m08F15095F420CAEB52430245A25B3EA490CF5DF4_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Dictation.DictationService::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_OnEnable_mE1A6601A1BD68433EE1FD579AA127E705F419AD8 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Single>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC *, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *, const RuntimeMethod*))UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_gshared)(__this, ___call0, method);
}
// System.Void UnityEngine.Events.UnityAction`2<System.String,System.String>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627 (UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_2__ctor_m8727842F47B6F77FCB70DE281A21C3E1DD2C7B5E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`2<System.String,System.String>::AddListener(UnityEngine.Events.UnityAction`2<!0,!1>)
inline void UnityEvent_2_AddListener_m3E022579578FE256EB77167A33F3003A4FC63B06 (UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4 * __this, UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4 *, UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC *, const RuntimeMethod*))UnityEvent_2_AddListener_m03A808706EF8B435537D817F2A43FD453E639D6C_gshared)(__this, ___call0, method);
}
// System.Void UnityEngine.Events.UnityAction`1<Facebook.WitAi.Lib.WitResponseNode>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08 (UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_mDACAB67F7E76FF788C30CA0E51BF3274666F951E_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Lib.WitResponseNode>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_AddListener_mC862B0487562E93445C65F24FE68CD55D2236A9E (UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055 * __this, UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055 *, UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 *, const RuntimeMethod*))UnityEvent_1_AddListener_m14DAE292BCF77B088359410E4C12071936DB681D_gshared)(__this, ___call0, method);
}
// System.Void Facebook.WitAi.Dictation.DictationService::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_OnDisable_m348240DD6548954DB720BD8264667E32A05C2DD7 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3 (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC *, UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *, const RuntimeMethod*))UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3_gshared)(__this, ___call0, method);
}
// System.Void UnityEngine.Events.UnityEvent`2<System.String,System.String>::RemoveListener(UnityEngine.Events.UnityAction`2<!0,!1>)
inline void UnityEvent_2_RemoveListener_m23EE42492565C29932813AA8242EC48E5C5CCF73 (UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4 * __this, UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4 *, UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC *, const RuntimeMethod*))UnityEvent_2_RemoveListener_mE340477D10D41DB3D0011507846998A5369C8E9F_gshared)(__this, ___call0, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Lib.WitResponseNode>::RemoveListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_RemoveListener_m3AD600DB38F3A6E8D846AEAF3A6127393E209BC4 (UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055 * __this, UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055 *, UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 *, const RuntimeMethod*))UnityEvent_1_RemoveListener_m793372F5AF1175F5DD348F908874E7D607B16DBD_gshared)(__this, ___call0, method);
}
// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnFullTranscription()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * DictationEvents_get_OnFullTranscription_m2EAF17EFABFB117FF97347672CEDB24631841E19_inline (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method);
// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnPartialTranscription()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * DictationEvents_get_OnPartialTranscription_mABAB3495C693DEBF0D35FE5BA5D43AE0F1521CD2_inline (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Single>::Invoke(!0)
inline void UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC * __this, float ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t84B4EA1A2A00DEAC63B85AFAA89EBF67CA749DBC *, float, const RuntimeMethod*))UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E_gshared)(__this, ___arg00, method);
}
// System.Void UnityEngine.Events.UnityEvent`2<System.String,System.String>::Invoke(!0,!1)
inline void UnityEvent_2_Invoke_m5E08B438F5EC94224B4DC570221B05F8CD17ADE1 (UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4 * __this, String_t* ___arg00, String_t* ___arg11, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_2_tA0D2FB1E8F4286DCAC18EC973743AAC36A2AC3A4 *, String_t*, String_t*, const RuntimeMethod*))UnityEvent_2_Invoke_mBF66265092F853A13F5698ED2B62F0ADA48E4F0A_gshared)(__this, ___arg00, ___arg11, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<Facebook.WitAi.Lib.WitResponseNode>::Invoke(!0)
inline void UnityEvent_1_Invoke_m65140CE9B1391F17461B85941BAD199745AD67D6 (UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055 * __this, WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * ___arg00, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tCC2044C9D8DFBD8AFF4B651013FF7DCAC5CB6055 *, WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F *, const RuntimeMethod*))UnityEvent_1_Invoke_m73C0FE7D4CDD8627332257E8503F2E9862E33C3E_gshared)(__this, ___arg00, method);
}
// System.Void Facebook.WitAi.Events.VoiceEvents::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceEvents__ctor_mF698C67143D1A2F7EF11D9765D0069510CDC6AF1 (VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Dictation.DictationService::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService__ctor_m311B60E17FA0B01A58A48BA66558C8C8B5EDF216 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Utilities.DictationServiceReference/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m395CA89863B5EF6D641973FA980A65961C461D4D (U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.GameObject::get_scene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  GameObject_get_scene_m7EBF95ABB5037FEE6811928F2E83C769C53F86C2 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.SceneManagement.Scene::get_rootCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Scene_get_rootCount_mB2EDA66F8662B93761648F5E88D9D6B74542E2A8 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnPartialTranscription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * DictationEvents_get_OnPartialTranscription_mABAB3495C693DEBF0D35FE5BA5D43AE0F1521CD2 (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public WitTranscriptionEvent OnPartialTranscription => onPartialTranscription;
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = __this->get_onPartialTranscription_6();
		return L_0;
	}
}
// Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnFullTranscription()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * DictationEvents_get_OnFullTranscription_m2EAF17EFABFB117FF97347672CEDB24631841E19 (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public WitTranscriptionEvent OnFullTranscription => onFullTranscription;
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = __this->get_onFullTranscription_7();
		return L_0;
	}
}
// Facebook.WitAi.Events.WitMicLevelChangedEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnMicAudioLevelChanged()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * DictationEvents_get_OnMicAudioLevelChanged_m885ADF7E17D42AF2A7769AB43B772BB7427DE659 (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public WitMicLevelChangedEvent OnMicAudioLevelChanged => onMicAudioLevel;
		WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * L_0 = __this->get_onMicAudioLevel_14();
		return L_0;
	}
}
// UnityEngine.Events.UnityEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnMicStartedListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * DictationEvents_get_OnMicStartedListening_m61B9505971AFE7C78995D888E1947A245AFA8788 (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public UnityEvent OnMicStartedListening => onStart;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_onStart_9();
		return L_0;
	}
}
// UnityEngine.Events.UnityEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnMicStoppedListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * DictationEvents_get_OnMicStoppedListening_m932AB2EF24DEB7081F275B6E88B81CFAED2A06AC (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public UnityEvent OnMicStoppedListening => onStopped;
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_0 = __this->get_onStopped_10();
		return L_0;
	}
}
// System.Void Facebook.WitAi.Dictation.Events.DictationEvents::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationEvents__ctor_m6405DC5FA653EAE2F23D4985272DBA57AC31B02C (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public WitTranscriptionEvent onPartialTranscription = new WitTranscriptionEvent();
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = (WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E *)il2cpp_codegen_object_new(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E_il2cpp_TypeInfo_var);
		WitTranscriptionEvent__ctor_m4C7714B0A4C8E8682E853DC56033152EC96A10F2(L_0, /*hidden argument*/NULL);
		__this->set_onPartialTranscription_6(L_0);
		// public WitTranscriptionEvent onFullTranscription = new WitTranscriptionEvent();
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_1 = (WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E *)il2cpp_codegen_object_new(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E_il2cpp_TypeInfo_var);
		WitTranscriptionEvent__ctor_m4C7714B0A4C8E8682E853DC56033152EC96A10F2(L_1, /*hidden argument*/NULL);
		__this->set_onFullTranscription_7(L_1);
		// public WitResponseEvent onResponse = new WitResponseEvent();
		WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * L_2 = (WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 *)il2cpp_codegen_object_new(WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4_il2cpp_TypeInfo_var);
		WitResponseEvent__ctor_mD80B232078883242636A37FE1A09C7832E857189(L_2, /*hidden argument*/NULL);
		__this->set_onResponse_8(L_2);
		// public UnityEvent onStart = new UnityEvent();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_3 = (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)il2cpp_codegen_object_new(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6(L_3, /*hidden argument*/NULL);
		__this->set_onStart_9(L_3);
		// public UnityEvent onStopped = new UnityEvent();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 *)il2cpp_codegen_object_new(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4_il2cpp_TypeInfo_var);
		UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6(L_4, /*hidden argument*/NULL);
		__this->set_onStopped_10(L_4);
		// public WitErrorEvent onError = new WitErrorEvent();
		WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * L_5 = (WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 *)il2cpp_codegen_object_new(WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571_il2cpp_TypeInfo_var);
		WitErrorEvent__ctor_mCE476BA3F0DC6EEAA983171C144FA9E3D16CE8F9(L_5, /*hidden argument*/NULL);
		__this->set_onError_11(L_5);
		// public DictationSessionEvent onDictationSessionStarted = new DictationSessionEvent();
		DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * L_6 = (DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F *)il2cpp_codegen_object_new(DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F_il2cpp_TypeInfo_var);
		DictationSessionEvent__ctor_m7768767815824DE024DA764BB9A3FAE03FD90FCF(L_6, /*hidden argument*/NULL);
		__this->set_onDictationSessionStarted_12(L_6);
		// public DictationSessionEvent onDictationSessionStopped = new DictationSessionEvent();
		DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * L_7 = (DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F *)il2cpp_codegen_object_new(DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F_il2cpp_TypeInfo_var);
		DictationSessionEvent__ctor_m7768767815824DE024DA764BB9A3FAE03FD90FCF(L_7, /*hidden argument*/NULL);
		__this->set_onDictationSessionStopped_13(L_7);
		// public WitMicLevelChangedEvent onMicAudioLevel = new WitMicLevelChangedEvent();
		WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * L_8 = (WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B *)il2cpp_codegen_object_new(WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B_il2cpp_TypeInfo_var);
		WitMicLevelChangedEvent__ctor_m8BFD07A7BACC84E34DE90900669265E41CB9DE6F(L_8, /*hidden argument*/NULL);
		__this->set_onMicAudioLevel_14(L_8);
		EventRegistry__ctor_mEABA3809EFD2BAD4526FBCB3297E9D14BAB184CC(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Facebook.WitAi.Dictation.Events.DictationEvents Facebook.WitAi.Dictation.DictationService::get_DictationEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	{
		// get => dictationEvents;
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0 = __this->get_dictationEvents_4();
		return L_0;
	}
}
// System.Void Facebook.WitAi.Dictation.DictationService::set_DictationEvents(Facebook.WitAi.Dictation.Events.DictationEvents)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_set_DictationEvents_m2B7A2222580A404DAF90AF5544D276202DB8E812 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * ___value0, const RuntimeMethod* method)
{
	{
		// set => dictationEvents = value;
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0 = ___value0;
		__this->set_dictationEvents_4(L_0);
		return;
	}
}
// Facebook.WitAi.Interfaces.IAudioInputEvents Facebook.WitAi.Dictation.DictationService::get_AudioEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DictationService_get_AudioEvents_mF0A1F319ECBCF7728A29C24099424829A2601AF6 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	{
		// public IAudioInputEvents AudioEvents => DictationEvents;
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// Facebook.WitAi.Interfaces.ITranscriptionEvent Facebook.WitAi.Dictation.DictationService::get_TranscriptionEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DictationService_get_TranscriptionEvents_m009BE05026EAC78CAFD285A76BECC34E7F7449B2 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	{
		// public ITranscriptionEvent TranscriptionEvents => DictationEvents;
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Facebook.WitAi.Dictation.DictationService::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_Awake_m7D507E8B054AC4BCE0E8AF58AA90887FC1C21749 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_m46F4C63848A93702DBA41F4E4881F698B7A03B01_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m7E226A153B7C6780B3BCDA0C387355914CD83B2B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_mFA61385BD0582696C51D31B3024801A2A801A527_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m6A4BBAA3243EF2BF1E35E4A2C0E1187379E5A35B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var audioEventListener = GetComponent<AudioEventListener>();
		AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78 * L_0;
		L_0 = Component_GetComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_m46F4C63848A93702DBA41F4E4881F698B7A03B01(__this, /*hidden argument*/Component_GetComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_m46F4C63848A93702DBA41F4E4881F698B7A03B01_RuntimeMethod_var);
		// if (!audioEventListener)
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0019;
		}
	}
	{
		// gameObject.AddComponent<AudioEventListener>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		AudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78 * L_3;
		L_3 = GameObject_AddComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_mFA61385BD0582696C51D31B3024801A2A801A527(L_2, /*hidden argument*/GameObject_AddComponent_TisAudioEventListener_t241D7BAAEBD02802559C644DF62839418CB43C78_mFA61385BD0582696C51D31B3024801A2A801A527_RuntimeMethod_var);
	}

IL_0019:
	{
		// var transcriptionEventListener = GetComponent<TranscriptionEventListener>();
		TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5 * L_4;
		L_4 = Component_GetComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m7E226A153B7C6780B3BCDA0C387355914CD83B2B(__this, /*hidden argument*/Component_GetComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m7E226A153B7C6780B3BCDA0C387355914CD83B2B_RuntimeMethod_var);
		// if (!transcriptionEventListener)
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0032;
		}
	}
	{
		// gameObject.AddComponent<TranscriptionEventListener>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6;
		L_6 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		TranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5 * L_7;
		L_7 = GameObject_AddComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m6A4BBAA3243EF2BF1E35E4A2C0E1187379E5A35B(L_6, /*hidden argument*/GameObject_AddComponent_TisTranscriptionEventListener_tA36FD7A0A1C3B4FB079A0BBD20682762C588EBB5_m6A4BBAA3243EF2BF1E35E4A2C0E1187379E5A35B_RuntimeMethod_var);
	}

IL_0032:
	{
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.DictationService::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_OnEnable_mE1A6601A1BD68433EE1FD579AA127E705F419AD8 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.DictationService::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService_OnDisable_m348240DD6548954DB720BD8264667E32A05C2DD7 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.DictationService::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationService__ctor_m311B60E17FA0B01A58A48BA66558C8C8B5EDF216 (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] public DictationEvents dictationEvents = new DictationEvents();
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0 = (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C *)il2cpp_codegen_object_new(DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_il2cpp_TypeInfo_var);
		DictationEvents__ctor_m6405DC5FA653EAE2F23D4985272DBA57AC31B02C(L_0, /*hidden argument*/NULL);
		__this->set_dictationEvents_4(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Facebook.WitAi.Interfaces.IAudioInputEvents Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference::get_AudioEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* DictationServiceAudioEventReference_get_AudioEvents_mF2B87E6D0403F24C7712461CEE2C0B02262F3465 (DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869 * __this, const RuntimeMethod* method)
{
	{
		// _dictationServiceReference.DictationService.AudioEvents;
		DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * L_0 = __this->get_address_of__dictationServiceReference_4();
		DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * L_1;
		L_1 = DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D((DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 *)L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		RuntimeObject* L_2;
		L_2 = DictationService_get_AudioEvents_mF0A1F319ECBCF7728A29C24099424829A2601AF6(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationServiceAudioEventReference__ctor_mEFD316B4490173C3D53880D33AA3C16309482898 (DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869 * __this, const RuntimeMethod* method)
{
	{
		AudioInputServiceReference__ctor_m5E534615D8949DC89AB69E594A3E9D6784716251(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Facebook.WitAi.Utilities.DictationServiceReference
IL2CPP_EXTERN_C void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshal_pinvoke(const DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411& unmarshaled, DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_pinvoke& marshaled)
{
	Exception_t* ___dictationService_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dictationService' of type 'DictationServiceReference': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dictationService_0Exception, NULL);
}
IL2CPP_EXTERN_C void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshal_pinvoke_back(const DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_pinvoke& marshaled, DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411& unmarshaled)
{
	Exception_t* ___dictationService_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dictationService' of type 'DictationServiceReference': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dictationService_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Facebook.WitAi.Utilities.DictationServiceReference
IL2CPP_EXTERN_C void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshal_pinvoke_cleanup(DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Facebook.WitAi.Utilities.DictationServiceReference
IL2CPP_EXTERN_C void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshal_com(const DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411& unmarshaled, DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_com& marshaled)
{
	Exception_t* ___dictationService_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dictationService' of type 'DictationServiceReference': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dictationService_0Exception, NULL);
}
IL2CPP_EXTERN_C void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshal_com_back(const DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_com& marshaled, DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411& unmarshaled)
{
	Exception_t* ___dictationService_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dictationService' of type 'DictationServiceReference': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dictationService_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Facebook.WitAi.Utilities.DictationServiceReference
IL2CPP_EXTERN_C void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshal_com_cleanup(DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_marshaled_com& marshaled)
{
}
// Facebook.WitAi.Dictation.DictationService Facebook.WitAi.Utilities.DictationServiceReference::get_DictationService()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D (DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Find_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mF24CEF963E25E3FBD9911167D4C6AECF3B5F769A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1__ctor_mD497F949F2BED7397F05D0D036703EDCBCBEE67B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_FindObjectsOfTypeAll_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mBEEC450FD16C11556668039B77E33D516147B175_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3Cget_DictationServiceU3Eb__2_0_mCC5F3C71561157B5E4F8EC4E3F1349DD9AFE1AA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* V_0 = NULL;
	Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * G_B4_0 = NULL;
	DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* G_B4_1 = NULL;
	DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * G_B4_2 = NULL;
	Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * G_B3_0 = NULL;
	DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* G_B3_1 = NULL;
	DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * G_B3_2 = NULL;
	{
		// if (!dictationService)
		DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * L_0 = __this->get_dictationService_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0041;
		}
	}
	{
		// DictationService[] services = Resources.FindObjectsOfTypeAll<DictationService>();
		DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* L_2;
		L_2 = Resources_FindObjectsOfTypeAll_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mBEEC450FD16C11556668039B77E33D516147B175(/*hidden argument*/Resources_FindObjectsOfTypeAll_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mBEEC450FD16C11556668039B77E33D516147B175_RuntimeMethod_var);
		V_0 = L_2;
		// if (services != null)
		DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		// dictationService = Array.Find(services, (o) => o.gameObject.scene.rootCount != 0);
		DictationServiceU5BU5D_tFF212647527214D6AB2FFC6D56F1C23CEFE81A36* L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var);
		Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * L_5 = ((U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var))->get_U3CU3E9__2_0_1();
		Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * L_6 = L_5;
		G_B3_0 = L_6;
		G_B3_1 = L_4;
		G_B3_2 = __this;
		if (L_6)
		{
			G_B4_0 = L_6;
			G_B4_1 = L_4;
			G_B4_2 = __this;
			goto IL_0037;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var);
		U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * L_7 = ((U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * L_8 = (Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 *)il2cpp_codegen_object_new(Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421_il2cpp_TypeInfo_var);
		Predicate_1__ctor_mD497F949F2BED7397F05D0D036703EDCBCBEE67B(L_8, L_7, (intptr_t)((intptr_t)U3CU3Ec_U3Cget_DictationServiceU3Eb__2_0_mCC5F3C71561157B5E4F8EC4E3F1349DD9AFE1AA1_RuntimeMethod_var), /*hidden argument*/Predicate_1__ctor_mD497F949F2BED7397F05D0D036703EDCBCBEE67B_RuntimeMethod_var);
		Predicate_1_t6E6ED167D2ACA9C935AF3964B8B43683A9829421 * L_9 = L_8;
		((U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var))->set_U3CU3E9__2_0_1(L_9);
		G_B4_0 = L_9;
		G_B4_1 = G_B3_1;
		G_B4_2 = G_B3_2;
	}

IL_0037:
	{
		DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * L_10;
		L_10 = Array_Find_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mF24CEF963E25E3FBD9911167D4C6AECF3B5F769A(G_B4_1, G_B4_0, /*hidden argument*/Array_Find_TisDictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_mF24CEF963E25E3FBD9911167D4C6AECF3B5F769A_RuntimeMethod_var);
		G_B4_2->set_dictationService_0(L_10);
	}

IL_0041:
	{
		// return dictationService;
		DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * L_11 = __this->get_dictationService_0();
		return L_11;
	}
}
IL2CPP_EXTERN_C  DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 * _thisAdjusted = reinterpret_cast<DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411 *>(__this + _offset);
	DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * _returnValue;
	_returnValue = DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D(_thisAdjusted, method);
	return _returnValue;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.WitAi.Dictation.Data.DictationSession::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationSession__ctor_mBD66C810B4D5E1DABD0D66F6F087888E5FEDCA0B (DictationSession_t5CC00AD98644841C3D6434B14273ADA40D0F6684 * __this, const RuntimeMethod* method)
{
	{
		VoiceSession__ctor_m18C2A045C7547678A82F84FEA47064CD306041E0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.WitAi.Dictation.Events.DictationSessionEvent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DictationSessionEvent__ctor_m7768767815824DE024DA764BB9A3FAE03FD90FCF (DictationSessionEvent_tAC16F833CBC2472E9C119DBCEF09F6DEFC81961F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1__ctor_m09B74417C0662618691C12E55F1FC11718629511_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityEvent_1__ctor_m09B74417C0662618691C12E55F1FC11718629511(__this, /*hidden argument*/UnityEvent_1__ctor_m09B74417C0662618691C12E55F1FC11718629511_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_Awake_mC29CCC23785A10EF8E383502828A5C730CBACC7E (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisWitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_m4A0F4B51FFEF50647C9280DBB5707561E01A1931_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (!witDictation) witDictation = FindObjectOfType<WitDictation>();
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_0 = __this->get_witDictation_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_m3B780F50367611CB9A34F3BF2032585E05DA1BFD(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		// if (!witDictation) witDictation = FindObjectOfType<WitDictation>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_2;
		L_2 = Object_FindObjectOfType_TisWitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_m4A0F4B51FFEF50647C9280DBB5707561E01A1931(/*hidden argument*/Object_FindObjectOfType_TisWitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_m4A0F4B51FFEF50647C9280DBB5707561E01A1931_RuntimeMethod_var);
		__this->set_witDictation_4(L_2);
	}

IL_0018:
	{
		// _text = new StringBuilder();
		StringBuilder_t * L_3 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_3, /*hidden argument*/NULL);
		__this->set__text_8(L_3);
		// _separator = new StringBuilder();
		StringBuilder_t * L_4 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_4, /*hidden argument*/NULL);
		__this->set__separator_11(L_4);
		// for (int i = 0; i < linesBetweenActivations; i++)
		V_0 = 0;
		goto IL_0042;
	}

IL_0032:
	{
		// _separator.AppendLine();
		StringBuilder_t * L_5 = __this->get__separator_11();
		NullCheck(L_5);
		StringBuilder_t * L_6;
		L_6 = StringBuilder_AppendLine_mB5790BC98389118626505708AE683AE9257B91B2(L_5, /*hidden argument*/NULL);
		// for (int i = 0; i < linesBetweenActivations; i++)
		int32_t L_7 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0042:
	{
		// for (int i = 0; i < linesBetweenActivations; i++)
		int32_t L_8 = V_0;
		int32_t L_9 = __this->get_linesBetweenActivations_5();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0032;
		}
	}
	{
		// if (!string.IsNullOrEmpty(activationSeparator))
		String_t* L_10 = __this->get_activationSeparator_6();
		bool L_11;
		L_11 = String_IsNullOrEmpty_m57429705D977ACD5EE7E210A858EED6F348C36B3(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_006a;
		}
	}
	{
		// _separator.Append(activationSeparator);
		StringBuilder_t * L_12 = __this->get__separator_11();
		String_t* L_13 = __this->get_activationSeparator_6();
		NullCheck(L_12);
		StringBuilder_t * L_14;
		L_14 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_12, L_13, /*hidden argument*/NULL);
	}

IL_006a:
	{
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnEnable_m2B85B8E2EF3F747FFB3A37043B50F9760BAEB3B1 (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// witDictation.VoiceEvents.OnFullTranscription.AddListener(OnFullTranscription);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_0 = __this->get_witDictation_4();
		NullCheck(L_0);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_1;
		L_1 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_2;
		L_2 = VoiceEvents_get_OnFullTranscription_m2F3454ABA3D28D8BCA75778AC33D5AC2B93EEA28_inline(L_1, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_3 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_3, __this, (intptr_t)((intptr_t)MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_2);
		UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398(L_2, L_3, /*hidden argument*/UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var);
		// witDictation.VoiceEvents.OnPartialTranscription.AddListener(OnPartialTranscription);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_4 = __this->get_witDictation_4();
		NullCheck(L_4);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_5;
		L_5 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_6;
		L_6 = VoiceEvents_get_OnPartialTranscription_m42A410529487FA1E9A6BA3104E4CE6E2063E82A7_inline(L_5, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_7 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_7, __this, (intptr_t)((intptr_t)MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_6);
		UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398(L_6, L_7, /*hidden argument*/UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var);
		// witDictation.VoiceEvents.OnAborting.AddListener(OnCancelled);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_8 = __this->get_witDictation_4();
		NullCheck(L_8);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_9;
		L_9 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_10 = L_9->get_OnAborting_11();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_11 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_11, __this, (intptr_t)((intptr_t)MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_10, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnDisable_mAAC7E790350E3D250D3447C7F5A9CD857A34CE61 (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _activeText = string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set__activeText_9(L_0);
		// witDictation.VoiceEvents.OnFullTranscription.RemoveListener(OnFullTranscription);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_1 = __this->get_witDictation_4();
		NullCheck(L_1);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_2;
		L_2 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_3;
		L_3 = VoiceEvents_get_OnFullTranscription_m2F3454ABA3D28D8BCA75778AC33D5AC2B93EEA28_inline(L_2, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_4 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_4, __this, (intptr_t)((intptr_t)MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_3);
		UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC(L_3, L_4, /*hidden argument*/UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var);
		// witDictation.VoiceEvents.OnPartialTranscription.RemoveListener(OnPartialTranscription);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_5 = __this->get_witDictation_4();
		NullCheck(L_5);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_6;
		L_6 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_7;
		L_7 = VoiceEvents_get_OnPartialTranscription_m42A410529487FA1E9A6BA3104E4CE6E2063E82A7_inline(L_6, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_8 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_8, __this, (intptr_t)((intptr_t)MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_7);
		UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC(L_7, L_8, /*hidden argument*/UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var);
		// witDictation.VoiceEvents.OnAborting.RemoveListener(OnCancelled);
		WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * L_9 = __this->get_witDictation_4();
		NullCheck(L_9);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_10;
		L_10 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_11 = L_10->get_OnAborting_11();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_12 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_12, __this, (intptr_t)((intptr_t)MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_11);
		UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956(L_11, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnCancelled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _activeText = string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set__activeText_9(L_0);
		// OnTranscriptionUpdated();
		MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnFullTranscription(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _activeText = string.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set__activeText_9(L_0);
		// if (_text.Length > 0)
		StringBuilder_t * L_1 = __this->get__text_8();
		NullCheck(L_1);
		int32_t L_2;
		L_2 = StringBuilder_get_Length_m680500263C59ACFD9582BF2AEEED8E92C87FF5C0(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		// _text.Append(_separator);
		StringBuilder_t * L_3 = __this->get__text_8();
		StringBuilder_t * L_4 = __this->get__separator_11();
		NullCheck(L_3);
		StringBuilder_t * L_5;
		L_5 = StringBuilder_Append_m545FFB72A578320B1D6EA3772160353FD62C344F(L_3, L_4, /*hidden argument*/NULL);
	}

IL_002b:
	{
		// _text.Append(text);
		StringBuilder_t * L_6 = __this->get__text_8();
		String_t* L_7 = ___text0;
		NullCheck(L_6);
		StringBuilder_t * L_8;
		L_8 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_6, L_7, /*hidden argument*/NULL);
		// OnTranscriptionUpdated();
		MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnPartialTranscription(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094 (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, String_t* ___text0, const RuntimeMethod* method)
{
	{
		// _activeText = text;
		String_t* L_0 = ___text0;
		__this->set__activeText_9(L_0);
		// OnTranscriptionUpdated();
		MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_Clear_m82498D8064E434671706AD1582A0B2D9806BCD4C (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _text.Clear();
		StringBuilder_t * L_0 = __this->get__text_8();
		NullCheck(L_0);
		StringBuilder_t * L_1;
		L_1 = StringBuilder_Clear_m3D1F9F2F9EBA938807B7667DC2021D882B9B8FA1(L_0, /*hidden argument*/NULL);
		// onTranscriptionUpdated.Invoke(string.Empty);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_2 = __this->get_onTranscriptionUpdated_7();
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_2);
		UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10(L_2, L_3, /*hidden argument*/UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnTranscriptionUpdated()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3 (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	StringBuilder_t * V_0 = NULL;
	{
		// var transcription = new StringBuilder();
		StringBuilder_t * L_0 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		// transcription.Append(_text);
		StringBuilder_t * L_1 = V_0;
		StringBuilder_t * L_2 = __this->get__text_8();
		NullCheck(L_1);
		StringBuilder_t * L_3;
		L_3 = StringBuilder_Append_m545FFB72A578320B1D6EA3772160353FD62C344F(L_1, L_2, /*hidden argument*/NULL);
		// if (!string.IsNullOrEmpty(_activeText))
		String_t* L_4 = __this->get__activeText_9();
		bool L_5;
		L_5 = String_IsNullOrEmpty_m57429705D977ACD5EE7E210A858EED6F348C36B3(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0050;
		}
	}
	{
		// if (transcription.Length > 0)
		StringBuilder_t * L_6 = V_0;
		NullCheck(L_6);
		int32_t L_7;
		L_7 = StringBuilder_get_Length_m680500263C59ACFD9582BF2AEEED8E92C87FF5C0(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		// transcription.Append(_separator);
		StringBuilder_t * L_8 = V_0;
		StringBuilder_t * L_9 = __this->get__separator_11();
		NullCheck(L_8);
		StringBuilder_t * L_10;
		L_10 = StringBuilder_Append_m545FFB72A578320B1D6EA3772160353FD62C344F(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0036:
	{
		// if (!string.IsNullOrEmpty(_activeText))
		String_t* L_11 = __this->get__activeText_9();
		bool L_12;
		L_12 = String_IsNullOrEmpty_m57429705D977ACD5EE7E210A858EED6F348C36B3(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0050;
		}
	}
	{
		// transcription.Append(_activeText);
		StringBuilder_t * L_13 = V_0;
		String_t* L_14 = __this->get__activeText_9();
		NullCheck(L_13);
		StringBuilder_t * L_15;
		L_15 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_13, L_14, /*hidden argument*/NULL);
	}

IL_0050:
	{
		// onTranscriptionUpdated.Invoke(transcription.ToString());
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_16 = __this->get_onTranscriptionUpdated_7();
		StringBuilder_t * L_17 = V_0;
		NullCheck(L_17);
		String_t* L_18;
		L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		NullCheck(L_16);
		UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10(L_16, L_18, /*hidden argument*/UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultiRequestTranscription__ctor_mE1F7E175755F2366109D59622988F3820E320571 (MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// [SerializeField] private int linesBetweenActivations = 2;
		__this->set_linesBetweenActivations_5(2);
		// [SerializeField] private string activationSeparator = String.Empty;
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_activationSeparator_6(L_0);
		// [SerializeField] private WitTranscriptionEvent onTranscriptionUpdated = new
		//     WitTranscriptionEvent();
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_1 = (WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E *)il2cpp_codegen_object_new(WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E_il2cpp_TypeInfo_var);
		WitTranscriptionEvent__ctor_m4C7714B0A4C8E8682E853DC56033152EC96A10F2(L_1, /*hidden argument*/NULL);
		__this->set_onTranscriptionUpdated_7(L_1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Facebook.WitAi.Configuration.WitRuntimeConfiguration Facebook.WitAi.Dictation.WitDictation::get_RuntimeConfiguration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * WitDictation_get_RuntimeConfiguration_mB7DA19AA1DC42F4A641F1A2046BB21FAD26D5BB1 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// get => witRuntimeConfiguration;
		WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * L_0 = __this->get_witRuntimeConfiguration_5();
		return L_0;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::set_RuntimeConfiguration(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_set_RuntimeConfiguration_mD146662AFF704CBE74D89FD65E5113F4FE67B3E5 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * ___value0, const RuntimeMethod* method)
{
	{
		// set => witRuntimeConfiguration = value;
		WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * L_0 = ___value0;
		__this->set_witRuntimeConfiguration_5(L_0);
		return;
	}
}
// System.Boolean Facebook.WitAi.Dictation.WitDictation::get_Active()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitDictation_get_Active_mF6241F8ABE8B3FBC81338286128EAD2BD2A5475C (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool Active => null != witService && witService.Active;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6((Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_2 = __this->get_witService_6();
		NullCheck(L_2);
		bool L_3;
		L_3 = WitService_get_Active_m2B1C16788ACC3FC51B4132AA3461700F35F64DCF(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.WitAi.Dictation.WitDictation::get_IsRequestActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitDictation_get_IsRequestActive_m45244753B79700CDD582915D98C181529DAAEF79 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool IsRequestActive => null != witService && witService.IsRequestActive;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6((Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_2 = __this->get_witService_6();
		NullCheck(L_2);
		bool L_3;
		L_3 = WitService_get_IsRequestActive_m7DCDC5C172F24DC0E04F70B72A877EF6B228368F(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
// Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.Dictation.WitDictation::get_TranscriptionProvider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WitDictation_get_TranscriptionProvider_m403C21592BF51EE8922F2809D0C822799EE7A3CC (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// get => witService.TranscriptionProvider;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		NullCheck(L_0);
		RuntimeObject* L_1;
		L_1 = WitService_get_TranscriptionProvider_mC46CAEC37C328B150C73681220D20AD06928923E_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_set_TranscriptionProvider_m362EDCAEE6ADF49519B4D4E2C652520179568F17 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// set => witService.TranscriptionProvider = value;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		RuntimeObject* L_1 = ___value0;
		NullCheck(L_0);
		WitService_set_TranscriptionProvider_mEEA1B221B7F0B8DEB40680EF4FE8F0FDFCE7BA60(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.WitAi.Dictation.WitDictation::get_MicActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitDictation_get_MicActive_m37B99F41F78AFF2273C89AC6526F3C6BA6F5BD57 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public override bool MicActive => null != witService && witService.MicActive;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mDCB4E958808E725D0612CCABF340B284085F03D6((Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_2 = __this->get_witService_6();
		NullCheck(L_2);
		bool L_3;
		L_3 = WitService_get_MicActive_m68C2DAFDA763A6951534CC5F32F7CBB166B8C238(L_2, /*hidden argument*/NULL);
		return L_3;
	}

IL_001a:
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.WitAi.Dictation.WitDictation::get_ShouldSendMicData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WitDictation_get_ShouldSendMicData_m1FDC172A7EC463E2D6C6494827B6518CE50A3A34 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// protected override bool ShouldSendMicData => witRuntimeConfiguration.sendAudioToWit ||
		//                                              null == TranscriptionProvider;
		WitRuntimeConfiguration_t0073DA9BCF73DE8F9571FC2BFDDED5B6A686DA24 * L_0 = __this->get_witRuntimeConfiguration_5();
		NullCheck(L_0);
		bool L_1 = L_0->get_sendAudioToWit_9();
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		RuntimeObject* L_2;
		L_2 = VirtFuncInvoker0< RuntimeObject* >::Invoke(20 /* Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.Dictation.DictationService::get_TranscriptionProvider() */, __this);
		return (bool)((((RuntimeObject*)(RuntimeObject*)L_2) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
	}

IL_0017:
	{
		return (bool)1;
	}
}
// Facebook.WitAi.Events.VoiceEvents Facebook.WitAi.Dictation.WitDictation::get_VoiceEvents()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// get => voiceEvents;
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_0 = __this->get_voiceEvents_7();
		return L_0;
	}
}
// Facebook.WitAi.WitRequest Facebook.WitAi.Dictation.WitDictation::CreateWitRequest(Facebook.WitAi.Data.Configuration.WitConfiguration,Facebook.WitAi.Configuration.WitRequestOptions,Facebook.WitAi.Interfaces.IDynamicEntitiesProvider[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 * WitDictation_CreateWitRequest_m2172BF62EF8F120DD3FD4E7DFBFED0F7CED2D19C (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * ___config0, WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ___requestOptions1, IDynamicEntitiesProviderU5BU5D_tDDBA52ABBE7FA2E95A314F6D72D3FB0FDFED2107* ___additionalEntityProviders2, const RuntimeMethod* method)
{
	{
		// return config.DictationRequest(requestOptions);
		WitConfiguration_t30A3D2BE9E62EF8F9ED3CC1658F401F6F2424631 * L_0 = ___config0;
		WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * L_1 = ___requestOptions1;
		WitRequest_t63CAB069D8F10A056F9B4EA23FC2542CCF2B4622 * L_2;
		L_2 = WitRequestFactory_DictationRequest_m004339AFD5A82B9892D53EDA1F2AA30589429BC6(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::Activate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_Activate_m38A18911DB89BE2B4EC9650F5A2A1087F9879A48 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// witService.Activate();
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		NullCheck(L_0);
		WitService_Activate_m43131CA4582F6D98DF18810AAC33D44030DBBA3F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_Activate_m5928BFEEE322F1A6370CDE589815E3BF94C3B2D6 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ___options0, const RuntimeMethod* method)
{
	{
		// witService.Activate(options);
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * L_1 = ___options0;
		NullCheck(L_0);
		WitService_Activate_mDC1F29D19E6286DB385F30758631D14F53232FB4(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::ActivateImmediately()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_ActivateImmediately_m696CC23D681E70B873EC2C1C9E85BC41557387CC (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// witService.ActivateImmediately();
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		NullCheck(L_0);
		WitService_ActivateImmediately_mB89815E8D4F8F369FC268A3BC0B3131559D8F979(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_ActivateImmediately_mDCFD74D65C93ED023E478ADA0C19E4E6798557D3 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * ___options0, const RuntimeMethod* method)
{
	{
		// witService.ActivateImmediately(options);
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		WitRequestOptions_tBFA76B798457A95DA736C6A3A913330739AF9F1F * L_1 = ___options0;
		NullCheck(L_0);
		WitService_ActivateImmediately_mA8E4D8255A1329F05B89619F01D31B7DD15EDAD3(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::Deactivate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_Deactivate_mD7DCBDB73B9902A73230FFBDAA446DEF1535F8D1 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// witService.Deactivate();
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		NullCheck(L_0);
		WitService_Deactivate_mD4C8DAC287BE9F20EFF59C964A8BAF27D141E30E(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::Cancel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_Cancel_m00F83196FBBD6AD43BE6C33778E24205F23A64F4 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// witService.DeactivateAndAbortRequest();
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_0 = __this->get_witService_6();
		NullCheck(L_0);
		WitService_DeactivateAndAbortRequest_mC72612AF886B4C5247F95BD675ACFEA448A1476F(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_Awake_m8060F7C9B2DBC3EE203BA9F826AAF13E2723AC09 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_AddComponent_TisWitService_t860537723698CF0607466342346F3B1FECA68DCB_mD5C2810AD85ACA4F42F27CE5813E283611FE0A9F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Awake();
		DictationService_Awake_m7D507E8B054AC4BCE0E8AF58AA90887FC1C21749(__this, /*hidden argument*/NULL);
		// witService = gameObject.AddComponent<WitService>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_1;
		L_1 = GameObject_AddComponent_TisWitService_t860537723698CF0607466342346F3B1FECA68DCB_mD5C2810AD85ACA4F42F27CE5813E283611FE0A9F(L_0, /*hidden argument*/GameObject_AddComponent_TisWitService_t860537723698CF0607466342346F3B1FECA68DCB_mD5C2810AD85ACA4F42F27CE5813E283611FE0A9F_RuntimeMethod_var);
		__this->set_witService_6(L_1);
		// witService.VoiceEventProvider = this;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_2 = __this->get_witService_6();
		NullCheck(L_2);
		WitService_set_VoiceEventProvider_m3DFFC7C2FD3C5DBB86407D8C7CB0FA678805E9DC_inline(L_2, __this, /*hidden argument*/NULL);
		// witService.ConfigurationProvider = this;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_3 = __this->get_witService_6();
		NullCheck(L_3);
		WitService_set_ConfigurationProvider_m292F41D9B59A9CF42D986ACBBB4FD38569E06195_inline(L_3, __this, /*hidden argument*/NULL);
		// witService.WitRequestProvider = this;
		WitService_t860537723698CF0607466342346F3B1FECA68DCB * L_4 = __this->get_witService_6();
		NullCheck(L_4);
		WitService_set_WitRequestProvider_m08F15095F420CAEB52430245A25B3EA490CF5DF4_inline(L_4, __this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnEnable_m98092E5BAE32BF982AFDF82ED3573100AC5A328A (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_mC862B0487562E93445C65F24FE68CD55D2236A9E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_2_AddListener_m3E022579578FE256EB77167A33F3003A4FC63B06_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnEnable();
		DictationService_OnEnable_mE1A6601A1BD68433EE1FD579AA127E705F419AD8(__this, /*hidden argument*/NULL);
		// VoiceEvents.OnFullTranscription.AddListener(OnFullTranscription);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_0;
		L_0 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_1;
		L_1 = VoiceEvents_get_OnFullTranscription_m2F3454ABA3D28D8BCA75778AC33D5AC2B93EEA28_inline(L_0, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_2 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_2, __this, (intptr_t)((intptr_t)WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398(L_1, L_2, /*hidden argument*/UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var);
		// VoiceEvents.OnPartialTranscription.AddListener(OnPartialTranscription);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_3;
		L_3 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_4;
		L_4 = VoiceEvents_get_OnPartialTranscription_m42A410529487FA1E9A6BA3104E4CE6E2063E82A7_inline(L_3, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_5 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_5, __this, (intptr_t)((intptr_t)WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_4);
		UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398(L_4, L_5, /*hidden argument*/UnityEvent_1_AddListener_m35A8B5EA067599AC8BEA652A1DA4085B8E366398_RuntimeMethod_var);
		// VoiceEvents.OnStartListening.AddListener(OnStartedListening);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_6;
		L_6 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_7 = L_6->get_OnStartListening_17();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_8 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_8, __this, (intptr_t)((intptr_t)WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_7, L_8, /*hidden argument*/NULL);
		// VoiceEvents.OnStoppedListening.AddListener(OnStoppedListening);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_9;
		L_9 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_10 = L_9->get_OnStoppedListening_18();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_11 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_11, __this, (intptr_t)((intptr_t)WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_10, L_11, /*hidden argument*/NULL);
		// VoiceEvents.OnMicLevelChanged.AddListener(OnMicLevelChanged);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_12;
		L_12 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * L_13 = L_12->get_OnMicLevelChanged_14();
		UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * L_14 = (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *)il2cpp_codegen_object_new(UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B(L_14, __this, (intptr_t)((intptr_t)WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var);
		NullCheck(L_13);
		UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C(L_13, L_14, /*hidden argument*/UnityEvent_1_AddListener_mA73838FBF3836695F5183B32B797E9499BA5E59C_RuntimeMethod_var);
		// VoiceEvents.OnError.AddListener(OnError);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_15;
		L_15 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * L_16 = L_15->get_OnError_10();
		UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC * L_17 = (UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC *)il2cpp_codegen_object_new(UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627(L_17, __this, (intptr_t)((intptr_t)WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8_RuntimeMethod_var), /*hidden argument*/UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627_RuntimeMethod_var);
		NullCheck(L_16);
		UnityEvent_2_AddListener_m3E022579578FE256EB77167A33F3003A4FC63B06(L_16, L_17, /*hidden argument*/UnityEvent_2_AddListener_m3E022579578FE256EB77167A33F3003A4FC63B06_RuntimeMethod_var);
		// VoiceEvents.OnResponse.AddListener(OnResponse);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_18;
		L_18 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * L_19 = L_18->get_OnResponse_7();
		UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 * L_20 = (UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 *)il2cpp_codegen_object_new(UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08(L_20, __this, (intptr_t)((intptr_t)WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08_RuntimeMethod_var);
		NullCheck(L_19);
		UnityEvent_1_AddListener_mC862B0487562E93445C65F24FE68CD55D2236A9E(L_19, L_20, /*hidden argument*/UnityEvent_1_AddListener_mC862B0487562E93445C65F24FE68CD55D2236A9E_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnDisable_m12D013019C86FC5477DA9FB7BD2495B7A01221B4 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_RemoveListener_m3AD600DB38F3A6E8D846AEAF3A6127393E209BC4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_2_RemoveListener_m23EE42492565C29932813AA8242EC48E5C5CCF73_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnDisable();
		DictationService_OnDisable_m348240DD6548954DB720BD8264667E32A05C2DD7(__this, /*hidden argument*/NULL);
		// VoiceEvents.OnFullTranscription.RemoveListener(OnFullTranscription);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_0;
		L_0 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_1;
		L_1 = VoiceEvents_get_OnFullTranscription_m2F3454ABA3D28D8BCA75778AC33D5AC2B93EEA28_inline(L_0, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_2 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_2, __this, (intptr_t)((intptr_t)WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_1);
		UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC(L_1, L_2, /*hidden argument*/UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var);
		// VoiceEvents.OnPartialTranscription.RemoveListener(OnPartialTranscription);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_3;
		L_3 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_4;
		L_4 = VoiceEvents_get_OnPartialTranscription_m42A410529487FA1E9A6BA3104E4CE6E2063E82A7_inline(L_3, /*hidden argument*/NULL);
		UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 * L_5 = (UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647 *)il2cpp_codegen_object_new(UnityAction_1_t4A1848C01D99711D0E3D235F1FDBBA96BA48B647_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F(L_5, __this, (intptr_t)((intptr_t)WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_mBB6FF824ECCAE3C08CD8B015E235BBDC15BCF43F_RuntimeMethod_var);
		NullCheck(L_4);
		UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC(L_4, L_5, /*hidden argument*/UnityEvent_1_RemoveListener_m997398435E34B3F6C218236492D6ED145458F0BC_RuntimeMethod_var);
		// VoiceEvents.OnStartListening.RemoveListener(OnStartedListening);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_6;
		L_6 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_7 = L_6->get_OnStartListening_17();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_8 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_8, __this, (intptr_t)((intptr_t)WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956(L_7, L_8, /*hidden argument*/NULL);
		// VoiceEvents.OnStoppedListening.RemoveListener(OnStoppedListening);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_9;
		L_9 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_10 = L_9->get_OnStoppedListening_18();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_11 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_11, __this, (intptr_t)((intptr_t)WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956(L_10, L_11, /*hidden argument*/NULL);
		// VoiceEvents.OnMicLevelChanged.RemoveListener(OnMicLevelChanged);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_12;
		L_12 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * L_13 = L_12->get_OnMicLevelChanged_14();
		UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB * L_14 = (UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB *)il2cpp_codegen_object_new(UnityAction_1_t50101DC7058B3235A520FF57E827D51694843FBB_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B(L_14, __this, (intptr_t)((intptr_t)WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m8CACADCAC18230FB18DF7A6BEC3D9EAD93FEDC3B_RuntimeMethod_var);
		NullCheck(L_13);
		UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3(L_13, L_14, /*hidden argument*/UnityEvent_1_RemoveListener_m3EA4FA20F6DE6E6FC738060875193A99E19AD1C3_RuntimeMethod_var);
		// VoiceEvents.OnError.RemoveListener(OnError);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_15;
		L_15 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * L_16 = L_15->get_OnError_10();
		UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC * L_17 = (UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC *)il2cpp_codegen_object_new(UnityAction_2_tED09E1FE30DBE393EF75B69D9204A4C3541FB4AC_il2cpp_TypeInfo_var);
		UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627(L_17, __this, (intptr_t)((intptr_t)WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8_RuntimeMethod_var), /*hidden argument*/UnityAction_2__ctor_m3A7B071D44178CF5835B482DC594812F0D512627_RuntimeMethod_var);
		NullCheck(L_16);
		UnityEvent_2_RemoveListener_m23EE42492565C29932813AA8242EC48E5C5CCF73(L_16, L_17, /*hidden argument*/UnityEvent_2_RemoveListener_m23EE42492565C29932813AA8242EC48E5C5CCF73_RuntimeMethod_var);
		// VoiceEvents.OnResponse.RemoveListener(OnResponse);
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_18;
		L_18 = WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * L_19 = L_18->get_OnResponse_7();
		UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 * L_20 = (UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001 *)il2cpp_codegen_object_new(UnityAction_1_tB7A7CC3781E21429950CD118D054C59A4EB6C001_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08(L_20, __this, (intptr_t)((intptr_t)WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m8A8631A2985B5422ABFB746C4D6D43FFEBAF3E08_RuntimeMethod_var);
		NullCheck(L_19);
		UnityEvent_1_RemoveListener_m3AD600DB38F3A6E8D846AEAF3A6127393E209BC4(L_19, L_20, /*hidden argument*/UnityEvent_1_RemoveListener_m3AD600DB38F3A6E8D846AEAF3A6127393E209BC4_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnFullTranscription(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, String_t* ___transcription0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * G_B2_0 = NULL;
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * G_B1_0 = NULL;
	{
		// DictationEvents.OnFullTranscription?.Invoke(transcription);
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_1;
		L_1 = DictationEvents_get_OnFullTranscription_m2EAF17EFABFB117FF97347672CEDB24631841E19_inline(L_0, /*hidden argument*/NULL);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		String_t* L_3 = ___transcription0;
		NullCheck(G_B2_0);
		UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10(G_B2_0, L_3, /*hidden argument*/UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnPartialTranscription(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, String_t* ___transcription0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * G_B2_0 = NULL;
	WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * G_B1_0 = NULL;
	{
		// DictationEvents.OnPartialTranscription?.Invoke(transcription);
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_1;
		L_1 = DictationEvents_get_OnPartialTranscription_mABAB3495C693DEBF0D35FE5BA5D43AE0F1521CD2_inline(L_0, /*hidden argument*/NULL);
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		String_t* L_3 = ___transcription0;
		NullCheck(G_B2_0);
		UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10(G_B2_0, L_3, /*hidden argument*/UnityEvent_1_Invoke_m48FFBB804EE21EB2CB49F17413E013F7C6A97E10_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnStartedListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B2_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B1_0 = NULL;
	{
		// DictationEvents.onStart?.Invoke();
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_1 = L_0->get_onStart_9();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		NullCheck(G_B2_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B2_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnStoppedListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B2_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B1_0 = NULL;
	{
		// DictationEvents.onStopped?.Invoke();
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_1 = L_0->get_onStopped_10();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		NullCheck(G_B2_0);
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B2_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnMicLevelChanged(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, float ___level0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * G_B2_0 = NULL;
	WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * G_B1_0 = NULL;
	{
		// DictationEvents.onMicAudioLevel?.Invoke(level);
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * L_1 = L_0->get_onMicAudioLevel_14();
		WitMicLevelChangedEvent_t8B4131C6B21D3887BC9192ABC8DE29B97197480B * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		float L_3 = ___level0;
		NullCheck(G_B2_0);
		UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E(G_B2_0, L_3, /*hidden argument*/UnityEvent_1_Invoke_m1DA4CADD93DA296D31E00A263219A99A9E0AFB0E_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnError(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, String_t* ___error0, String_t* ___message1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_2_Invoke_m5E08B438F5EC94224B4DC570221B05F8CD17ADE1_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * G_B2_0 = NULL;
	WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * G_B1_0 = NULL;
	{
		// DictationEvents.onError?.Invoke(error, message);
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * L_1 = L_0->get_onError_11();
		WitErrorEvent_t75A2B26145BEFDEC6A91E014BBA329E35862E571 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		String_t* L_3 = ___error0;
		String_t* L_4 = ___message1;
		NullCheck(G_B2_0);
		UnityEvent_2_Invoke_m5E08B438F5EC94224B4DC570221B05F8CD17ADE1(G_B2_0, L_3, L_4, /*hidden argument*/UnityEvent_2_Invoke_m5E08B438F5EC94224B4DC570221B05F8CD17ADE1_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::OnResponse(Facebook.WitAi.Lib.WitResponseNode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * ___response0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_Invoke_m65140CE9B1391F17461B85941BAD199745AD67D6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * G_B2_0 = NULL;
	WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * G_B1_0 = NULL;
	{
		// DictationEvents.onResponse?.Invoke(response);
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0;
		L_0 = DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * L_1 = L_0->get_onResponse_8();
		WitResponseEvent_tB6FF10625816530C0B3B946E94F9701FF0FAC6B4 * L_2 = L_1;
		G_B1_0 = L_2;
		if (L_2)
		{
			G_B2_0 = L_2;
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		WitResponseNode_t3F7BEAA45DD487A20B08AAF12B931617C67F3A7F * L_3 = ___response0;
		NullCheck(G_B2_0);
		UnityEvent_1_Invoke_m65140CE9B1391F17461B85941BAD199745AD67D6(G_B2_0, L_3, /*hidden argument*/UnityEvent_1_Invoke_m65140CE9B1391F17461B85941BAD199745AD67D6_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Facebook.WitAi.Dictation.WitDictation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WitDictation__ctor_m2ECD3B2372A9E938ADE0F10BD5B20DA8B0EAFEF9 (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private readonly VoiceEvents voiceEvents = new VoiceEvents();
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_0 = (VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D *)il2cpp_codegen_object_new(VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D_il2cpp_TypeInfo_var);
		VoiceEvents__ctor_mF698C67143D1A2F7EF11D9765D0069510CDC6AF1(L_0, /*hidden argument*/NULL);
		__this->set_voiceEvents_7(L_0);
		DictationService__ctor_m311B60E17FA0B01A58A48BA66558C8C8B5EDF216(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.WitAi.Utilities.DictationServiceReference/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mC2EA6FD9B031A2C0B0185D7684FD50026A055733 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * L_0 = (U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B *)il2cpp_codegen_object_new(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m395CA89863B5EF6D641973FA980A65961C461D4D(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Facebook.WitAi.Utilities.DictationServiceReference/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m395CA89863B5EF6D641973FA980A65961C461D4D (U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.WitAi.Utilities.DictationServiceReference/<>c::<get_DictationService>b__2_0(Facebook.WitAi.Dictation.DictationService)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3Cget_DictationServiceU3Eb__2_0_mCC5F3C71561157B5E4F8EC4E3F1349DD9AFE1AA1 (U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B * __this, DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * ___o0, const RuntimeMethod* method)
{
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// dictationService = Array.Find(services, (o) => o.gameObject.scene.rootCount != 0);
		DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * L_0 = ___o0;
		NullCheck(L_0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_2;
		L_2 = GameObject_get_scene_m7EBF95ABB5037FEE6811928F2E83C769C53F86C2(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3;
		L_3 = Scene_get_rootCount_mB2EDA66F8662B93761648F5E88D9D6B74542E2A8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_0), /*hidden argument*/NULL);
		return (bool)((!(((uint32_t)L_3) <= ((uint32_t)0)))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0_inline (DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA * __this, const RuntimeMethod* method)
{
	{
		// get => dictationEvents;
		DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * L_0 = __this->get_dictationEvents_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C_inline (WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866 * __this, const RuntimeMethod* method)
{
	{
		// get => voiceEvents;
		VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * L_0 = __this->get_voiceEvents_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * VoiceEvents_get_OnFullTranscription_m2F3454ABA3D28D8BCA75778AC33D5AC2B93EEA28_inline (VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * __this, const RuntimeMethod* method)
{
	{
		// public WitTranscriptionEvent OnFullTranscription => onFullTranscription;
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = __this->get_onFullTranscription_25();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * VoiceEvents_get_OnPartialTranscription_m42A410529487FA1E9A6BA3104E4CE6E2063E82A7_inline (VoiceEvents_t10F33F3A5E105E946E0684F5F546D6940B7C8B3D * __this, const RuntimeMethod* method)
{
	{
		// public WitTranscriptionEvent OnPartialTranscription => onPartialTranscription;
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = __this->get_onPartialTranscription_24();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* WitService_get_TranscriptionProvider_mC46CAEC37C328B150C73681220D20AD06928923E_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, const RuntimeMethod* method)
{
	{
		// get => _activeTranscriptionProvider;
		RuntimeObject* L_0 = __this->get__activeTranscriptionProvider_14();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WitService_set_VoiceEventProvider_m3DFFC7C2FD3C5DBB86407D8C7CB0FA678805E9DC_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// set => _voiceEventProvider = value;
		RuntimeObject* L_0 = ___value0;
		__this->set__voiceEventProvider_12(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WitService_set_ConfigurationProvider_m292F41D9B59A9CF42D986ACBBB4FD38569E06195_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// set => _runtimeConfigProvider = value;
		RuntimeObject* L_0 = ___value0;
		__this->set__runtimeConfigProvider_13(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void WitService_set_WitRequestProvider_m08F15095F420CAEB52430245A25B3EA490CF5DF4_inline (WitService_t860537723698CF0607466342346F3B1FECA68DCB * __this, RuntimeObject* ___value0, const RuntimeMethod* method)
{
	{
		// set => _witRequestProvider = value;
		RuntimeObject* L_0 = ___value0;
		__this->set__witRequestProvider_16(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * DictationEvents_get_OnFullTranscription_m2EAF17EFABFB117FF97347672CEDB24631841E19_inline (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public WitTranscriptionEvent OnFullTranscription => onFullTranscription;
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = __this->get_onFullTranscription_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * DictationEvents_get_OnPartialTranscription_mABAB3495C693DEBF0D35FE5BA5D43AE0F1521CD2_inline (DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C * __this, const RuntimeMethod* method)
{
	{
		// public WitTranscriptionEvent OnPartialTranscription => onPartialTranscription;
		WitTranscriptionEvent_t173FC642ECF9BBC8DC629B2978E5EE883F2BFC3E * L_0 = __this->get_onPartialTranscription_6();
		return L_0;
	}
}
