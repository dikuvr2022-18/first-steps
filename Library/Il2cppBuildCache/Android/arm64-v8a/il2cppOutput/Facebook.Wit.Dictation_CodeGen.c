﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 Facebook.WitAi.Dictation.DictationService Facebook.WitAi.Utilities.DictationServiceReference::get_DictationService()
extern void DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D (void);
// 0x00000002 System.Void Facebook.WitAi.Utilities.DictationServiceReference/<>c::.cctor()
extern void U3CU3Ec__cctor_mC2EA6FD9B031A2C0B0185D7684FD50026A055733 (void);
// 0x00000003 System.Void Facebook.WitAi.Utilities.DictationServiceReference/<>c::.ctor()
extern void U3CU3Ec__ctor_m395CA89863B5EF6D641973FA980A65961C461D4D (void);
// 0x00000004 System.Boolean Facebook.WitAi.Utilities.DictationServiceReference/<>c::<get_DictationService>b__2_0(Facebook.WitAi.Dictation.DictationService)
extern void U3CU3Ec_U3Cget_DictationServiceU3Eb__2_0_mCC5F3C71561157B5E4F8EC4E3F1349DD9AFE1AA1 (void);
// 0x00000005 Facebook.WitAi.Interfaces.IAudioInputEvents Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference::get_AudioEvents()
extern void DictationServiceAudioEventReference_get_AudioEvents_mF2B87E6D0403F24C7712461CEE2C0B02262F3465 (void);
// 0x00000006 System.Void Facebook.WitAi.ServiceReferences.DictationServiceAudioEventReference::.ctor()
extern void DictationServiceAudioEventReference__ctor_mEFD316B4490173C3D53880D33AA3C16309482898 (void);
// 0x00000007 System.Boolean Facebook.WitAi.Dictation.DictationService::get_Active()
// 0x00000008 System.Boolean Facebook.WitAi.Dictation.DictationService::get_IsRequestActive()
// 0x00000009 Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.Dictation.DictationService::get_TranscriptionProvider()
// 0x0000000A System.Void Facebook.WitAi.Dictation.DictationService::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
// 0x0000000B System.Boolean Facebook.WitAi.Dictation.DictationService::get_MicActive()
// 0x0000000C Facebook.WitAi.Dictation.Events.DictationEvents Facebook.WitAi.Dictation.DictationService::get_DictationEvents()
extern void DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0 (void);
// 0x0000000D System.Void Facebook.WitAi.Dictation.DictationService::set_DictationEvents(Facebook.WitAi.Dictation.Events.DictationEvents)
extern void DictationService_set_DictationEvents_m2B7A2222580A404DAF90AF5544D276202DB8E812 (void);
// 0x0000000E Facebook.WitAi.Interfaces.IAudioInputEvents Facebook.WitAi.Dictation.DictationService::get_AudioEvents()
extern void DictationService_get_AudioEvents_mF0A1F319ECBCF7728A29C24099424829A2601AF6 (void);
// 0x0000000F Facebook.WitAi.Interfaces.ITranscriptionEvent Facebook.WitAi.Dictation.DictationService::get_TranscriptionEvents()
extern void DictationService_get_TranscriptionEvents_m009BE05026EAC78CAFD285A76BECC34E7F7449B2 (void);
// 0x00000010 System.Boolean Facebook.WitAi.Dictation.DictationService::get_ShouldSendMicData()
// 0x00000011 System.Void Facebook.WitAi.Dictation.DictationService::Activate()
// 0x00000012 System.Void Facebook.WitAi.Dictation.DictationService::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
// 0x00000013 System.Void Facebook.WitAi.Dictation.DictationService::ActivateImmediately()
// 0x00000014 System.Void Facebook.WitAi.Dictation.DictationService::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
// 0x00000015 System.Void Facebook.WitAi.Dictation.DictationService::Deactivate()
// 0x00000016 System.Void Facebook.WitAi.Dictation.DictationService::Cancel()
// 0x00000017 System.Void Facebook.WitAi.Dictation.DictationService::Awake()
extern void DictationService_Awake_m7D507E8B054AC4BCE0E8AF58AA90887FC1C21749 (void);
// 0x00000018 System.Void Facebook.WitAi.Dictation.DictationService::OnEnable()
extern void DictationService_OnEnable_mE1A6601A1BD68433EE1FD579AA127E705F419AD8 (void);
// 0x00000019 System.Void Facebook.WitAi.Dictation.DictationService::OnDisable()
extern void DictationService_OnDisable_m348240DD6548954DB720BD8264667E32A05C2DD7 (void);
// 0x0000001A System.Void Facebook.WitAi.Dictation.DictationService::.ctor()
extern void DictationService__ctor_m311B60E17FA0B01A58A48BA66558C8C8B5EDF216 (void);
// 0x0000001B System.Boolean Facebook.WitAi.Dictation.IDictationService::get_Active()
// 0x0000001C System.Boolean Facebook.WitAi.Dictation.IDictationService::get_IsRequestActive()
// 0x0000001D System.Boolean Facebook.WitAi.Dictation.IDictationService::get_MicActive()
// 0x0000001E Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.Dictation.IDictationService::get_TranscriptionProvider()
// 0x0000001F System.Void Facebook.WitAi.Dictation.IDictationService::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
// 0x00000020 Facebook.WitAi.Dictation.Events.DictationEvents Facebook.WitAi.Dictation.IDictationService::get_DictationEvents()
// 0x00000021 System.Void Facebook.WitAi.Dictation.IDictationService::set_DictationEvents(Facebook.WitAi.Dictation.Events.DictationEvents)
// 0x00000022 System.Void Facebook.WitAi.Dictation.IDictationService::Activate()
// 0x00000023 System.Void Facebook.WitAi.Dictation.IDictationService::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
// 0x00000024 System.Void Facebook.WitAi.Dictation.IDictationService::ActivateImmediately()
// 0x00000025 System.Void Facebook.WitAi.Dictation.IDictationService::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
// 0x00000026 System.Void Facebook.WitAi.Dictation.IDictationService::Deactivate()
// 0x00000027 System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::Awake()
extern void MultiRequestTranscription_Awake_mC29CCC23785A10EF8E383502828A5C730CBACC7E (void);
// 0x00000028 System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnEnable()
extern void MultiRequestTranscription_OnEnable_m2B85B8E2EF3F747FFB3A37043B50F9760BAEB3B1 (void);
// 0x00000029 System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnDisable()
extern void MultiRequestTranscription_OnDisable_mAAC7E790350E3D250D3447C7F5A9CD857A34CE61 (void);
// 0x0000002A System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnCancelled()
extern void MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA (void);
// 0x0000002B System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnFullTranscription(System.String)
extern void MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F (void);
// 0x0000002C System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnPartialTranscription(System.String)
extern void MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094 (void);
// 0x0000002D System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::Clear()
extern void MultiRequestTranscription_Clear_m82498D8064E434671706AD1582A0B2D9806BCD4C (void);
// 0x0000002E System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::OnTranscriptionUpdated()
extern void MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3 (void);
// 0x0000002F System.Void Facebook.WitAi.Dictation.MultiRequestTranscription::.ctor()
extern void MultiRequestTranscription__ctor_mE1F7E175755F2366109D59622988F3820E320571 (void);
// 0x00000030 Facebook.WitAi.Configuration.WitRuntimeConfiguration Facebook.WitAi.Dictation.WitDictation::get_RuntimeConfiguration()
extern void WitDictation_get_RuntimeConfiguration_mB7DA19AA1DC42F4A641F1A2046BB21FAD26D5BB1 (void);
// 0x00000031 System.Void Facebook.WitAi.Dictation.WitDictation::set_RuntimeConfiguration(Facebook.WitAi.Configuration.WitRuntimeConfiguration)
extern void WitDictation_set_RuntimeConfiguration_mD146662AFF704CBE74D89FD65E5113F4FE67B3E5 (void);
// 0x00000032 System.Boolean Facebook.WitAi.Dictation.WitDictation::get_Active()
extern void WitDictation_get_Active_mF6241F8ABE8B3FBC81338286128EAD2BD2A5475C (void);
// 0x00000033 System.Boolean Facebook.WitAi.Dictation.WitDictation::get_IsRequestActive()
extern void WitDictation_get_IsRequestActive_m45244753B79700CDD582915D98C181529DAAEF79 (void);
// 0x00000034 Facebook.WitAi.Interfaces.ITranscriptionProvider Facebook.WitAi.Dictation.WitDictation::get_TranscriptionProvider()
extern void WitDictation_get_TranscriptionProvider_m403C21592BF51EE8922F2809D0C822799EE7A3CC (void);
// 0x00000035 System.Void Facebook.WitAi.Dictation.WitDictation::set_TranscriptionProvider(Facebook.WitAi.Interfaces.ITranscriptionProvider)
extern void WitDictation_set_TranscriptionProvider_m362EDCAEE6ADF49519B4D4E2C652520179568F17 (void);
// 0x00000036 System.Boolean Facebook.WitAi.Dictation.WitDictation::get_MicActive()
extern void WitDictation_get_MicActive_m37B99F41F78AFF2273C89AC6526F3C6BA6F5BD57 (void);
// 0x00000037 System.Boolean Facebook.WitAi.Dictation.WitDictation::get_ShouldSendMicData()
extern void WitDictation_get_ShouldSendMicData_m1FDC172A7EC463E2D6C6494827B6518CE50A3A34 (void);
// 0x00000038 Facebook.WitAi.Events.VoiceEvents Facebook.WitAi.Dictation.WitDictation::get_VoiceEvents()
extern void WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C (void);
// 0x00000039 Facebook.WitAi.WitRequest Facebook.WitAi.Dictation.WitDictation::CreateWitRequest(Facebook.WitAi.Data.Configuration.WitConfiguration,Facebook.WitAi.Configuration.WitRequestOptions,Facebook.WitAi.Interfaces.IDynamicEntitiesProvider[])
extern void WitDictation_CreateWitRequest_m2172BF62EF8F120DD3FD4E7DFBFED0F7CED2D19C (void);
// 0x0000003A System.Void Facebook.WitAi.Dictation.WitDictation::Activate()
extern void WitDictation_Activate_m38A18911DB89BE2B4EC9650F5A2A1087F9879A48 (void);
// 0x0000003B System.Void Facebook.WitAi.Dictation.WitDictation::Activate(Facebook.WitAi.Configuration.WitRequestOptions)
extern void WitDictation_Activate_m5928BFEEE322F1A6370CDE589815E3BF94C3B2D6 (void);
// 0x0000003C System.Void Facebook.WitAi.Dictation.WitDictation::ActivateImmediately()
extern void WitDictation_ActivateImmediately_m696CC23D681E70B873EC2C1C9E85BC41557387CC (void);
// 0x0000003D System.Void Facebook.WitAi.Dictation.WitDictation::ActivateImmediately(Facebook.WitAi.Configuration.WitRequestOptions)
extern void WitDictation_ActivateImmediately_mDCFD74D65C93ED023E478ADA0C19E4E6798557D3 (void);
// 0x0000003E System.Void Facebook.WitAi.Dictation.WitDictation::Deactivate()
extern void WitDictation_Deactivate_mD7DCBDB73B9902A73230FFBDAA446DEF1535F8D1 (void);
// 0x0000003F System.Void Facebook.WitAi.Dictation.WitDictation::Cancel()
extern void WitDictation_Cancel_m00F83196FBBD6AD43BE6C33778E24205F23A64F4 (void);
// 0x00000040 System.Void Facebook.WitAi.Dictation.WitDictation::Awake()
extern void WitDictation_Awake_m8060F7C9B2DBC3EE203BA9F826AAF13E2723AC09 (void);
// 0x00000041 System.Void Facebook.WitAi.Dictation.WitDictation::OnEnable()
extern void WitDictation_OnEnable_m98092E5BAE32BF982AFDF82ED3573100AC5A328A (void);
// 0x00000042 System.Void Facebook.WitAi.Dictation.WitDictation::OnDisable()
extern void WitDictation_OnDisable_m12D013019C86FC5477DA9FB7BD2495B7A01221B4 (void);
// 0x00000043 System.Void Facebook.WitAi.Dictation.WitDictation::OnFullTranscription(System.String)
extern void WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A (void);
// 0x00000044 System.Void Facebook.WitAi.Dictation.WitDictation::OnPartialTranscription(System.String)
extern void WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4 (void);
// 0x00000045 System.Void Facebook.WitAi.Dictation.WitDictation::OnStartedListening()
extern void WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D (void);
// 0x00000046 System.Void Facebook.WitAi.Dictation.WitDictation::OnStoppedListening()
extern void WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8 (void);
// 0x00000047 System.Void Facebook.WitAi.Dictation.WitDictation::OnMicLevelChanged(System.Single)
extern void WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897 (void);
// 0x00000048 System.Void Facebook.WitAi.Dictation.WitDictation::OnError(System.String,System.String)
extern void WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8 (void);
// 0x00000049 System.Void Facebook.WitAi.Dictation.WitDictation::OnResponse(Facebook.WitAi.Lib.WitResponseNode)
extern void WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2 (void);
// 0x0000004A System.Void Facebook.WitAi.Dictation.WitDictation::.ctor()
extern void WitDictation__ctor_m2ECD3B2372A9E938ADE0F10BD5B20DA8B0EAFEF9 (void);
// 0x0000004B Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnPartialTranscription()
extern void DictationEvents_get_OnPartialTranscription_mABAB3495C693DEBF0D35FE5BA5D43AE0F1521CD2 (void);
// 0x0000004C Facebook.WitAi.Events.WitTranscriptionEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnFullTranscription()
extern void DictationEvents_get_OnFullTranscription_m2EAF17EFABFB117FF97347672CEDB24631841E19 (void);
// 0x0000004D Facebook.WitAi.Events.WitMicLevelChangedEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnMicAudioLevelChanged()
extern void DictationEvents_get_OnMicAudioLevelChanged_m885ADF7E17D42AF2A7769AB43B772BB7427DE659 (void);
// 0x0000004E UnityEngine.Events.UnityEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnMicStartedListening()
extern void DictationEvents_get_OnMicStartedListening_m61B9505971AFE7C78995D888E1947A245AFA8788 (void);
// 0x0000004F UnityEngine.Events.UnityEvent Facebook.WitAi.Dictation.Events.DictationEvents::get_OnMicStoppedListening()
extern void DictationEvents_get_OnMicStoppedListening_m932AB2EF24DEB7081F275B6E88B81CFAED2A06AC (void);
// 0x00000050 System.Void Facebook.WitAi.Dictation.Events.DictationEvents::.ctor()
extern void DictationEvents__ctor_m6405DC5FA653EAE2F23D4985272DBA57AC31B02C (void);
// 0x00000051 System.Void Facebook.WitAi.Dictation.Events.DictationSessionEvent::.ctor()
extern void DictationSessionEvent__ctor_m7768767815824DE024DA764BB9A3FAE03FD90FCF (void);
// 0x00000052 System.Void Facebook.WitAi.Dictation.Data.DictationSession::.ctor()
extern void DictationSession__ctor_mBD66C810B4D5E1DABD0D66F6F087888E5FEDCA0B (void);
static Il2CppMethodPointer s_methodPointers[82] = 
{
	DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D,
	U3CU3Ec__cctor_mC2EA6FD9B031A2C0B0185D7684FD50026A055733,
	U3CU3Ec__ctor_m395CA89863B5EF6D641973FA980A65961C461D4D,
	U3CU3Ec_U3Cget_DictationServiceU3Eb__2_0_mCC5F3C71561157B5E4F8EC4E3F1349DD9AFE1AA1,
	DictationServiceAudioEventReference_get_AudioEvents_mF2B87E6D0403F24C7712461CEE2C0B02262F3465,
	DictationServiceAudioEventReference__ctor_mEFD316B4490173C3D53880D33AA3C16309482898,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DictationService_get_DictationEvents_mEA60DD718DFD7F8AD265959787E27F4943F57BC0,
	DictationService_set_DictationEvents_m2B7A2222580A404DAF90AF5544D276202DB8E812,
	DictationService_get_AudioEvents_mF0A1F319ECBCF7728A29C24099424829A2601AF6,
	DictationService_get_TranscriptionEvents_m009BE05026EAC78CAFD285A76BECC34E7F7449B2,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DictationService_Awake_m7D507E8B054AC4BCE0E8AF58AA90887FC1C21749,
	DictationService_OnEnable_mE1A6601A1BD68433EE1FD579AA127E705F419AD8,
	DictationService_OnDisable_m348240DD6548954DB720BD8264667E32A05C2DD7,
	DictationService__ctor_m311B60E17FA0B01A58A48BA66558C8C8B5EDF216,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MultiRequestTranscription_Awake_mC29CCC23785A10EF8E383502828A5C730CBACC7E,
	MultiRequestTranscription_OnEnable_m2B85B8E2EF3F747FFB3A37043B50F9760BAEB3B1,
	MultiRequestTranscription_OnDisable_mAAC7E790350E3D250D3447C7F5A9CD857A34CE61,
	MultiRequestTranscription_OnCancelled_mD072F2475571151D9ECF4828E743F0C8E42248BA,
	MultiRequestTranscription_OnFullTranscription_mFB244C98BAA3C82689E48308A618A34F6E7E228F,
	MultiRequestTranscription_OnPartialTranscription_m3FEB17C225EA5824EEDC243BC5F94955B8378094,
	MultiRequestTranscription_Clear_m82498D8064E434671706AD1582A0B2D9806BCD4C,
	MultiRequestTranscription_OnTranscriptionUpdated_m81AAE7B097BC6B0FA7444945BDC924808E4D2BC3,
	MultiRequestTranscription__ctor_mE1F7E175755F2366109D59622988F3820E320571,
	WitDictation_get_RuntimeConfiguration_mB7DA19AA1DC42F4A641F1A2046BB21FAD26D5BB1,
	WitDictation_set_RuntimeConfiguration_mD146662AFF704CBE74D89FD65E5113F4FE67B3E5,
	WitDictation_get_Active_mF6241F8ABE8B3FBC81338286128EAD2BD2A5475C,
	WitDictation_get_IsRequestActive_m45244753B79700CDD582915D98C181529DAAEF79,
	WitDictation_get_TranscriptionProvider_m403C21592BF51EE8922F2809D0C822799EE7A3CC,
	WitDictation_set_TranscriptionProvider_m362EDCAEE6ADF49519B4D4E2C652520179568F17,
	WitDictation_get_MicActive_m37B99F41F78AFF2273C89AC6526F3C6BA6F5BD57,
	WitDictation_get_ShouldSendMicData_m1FDC172A7EC463E2D6C6494827B6518CE50A3A34,
	WitDictation_get_VoiceEvents_mD4EE3FF69CD67E0A29082572B880AB881A61C48C,
	WitDictation_CreateWitRequest_m2172BF62EF8F120DD3FD4E7DFBFED0F7CED2D19C,
	WitDictation_Activate_m38A18911DB89BE2B4EC9650F5A2A1087F9879A48,
	WitDictation_Activate_m5928BFEEE322F1A6370CDE589815E3BF94C3B2D6,
	WitDictation_ActivateImmediately_m696CC23D681E70B873EC2C1C9E85BC41557387CC,
	WitDictation_ActivateImmediately_mDCFD74D65C93ED023E478ADA0C19E4E6798557D3,
	WitDictation_Deactivate_mD7DCBDB73B9902A73230FFBDAA446DEF1535F8D1,
	WitDictation_Cancel_m00F83196FBBD6AD43BE6C33778E24205F23A64F4,
	WitDictation_Awake_m8060F7C9B2DBC3EE203BA9F826AAF13E2723AC09,
	WitDictation_OnEnable_m98092E5BAE32BF982AFDF82ED3573100AC5A328A,
	WitDictation_OnDisable_m12D013019C86FC5477DA9FB7BD2495B7A01221B4,
	WitDictation_OnFullTranscription_m6632984B5D560D6C849644FDBCAE6835A14CF42A,
	WitDictation_OnPartialTranscription_m2CE9F425C007BFAD90E94021E38A78A360E0E5A4,
	WitDictation_OnStartedListening_m83E890287038B7722A4A7E0B75700C664ACA280D,
	WitDictation_OnStoppedListening_mEB6554416D4E8FF6709D855BEA7989A53DA559B8,
	WitDictation_OnMicLevelChanged_m2CC2A69750C0DF4D7EB39AC2E28B1210EA48F897,
	WitDictation_OnError_mFB1365F958F48B27E2A5F7B78DF41F5AB67DC4B8,
	WitDictation_OnResponse_m394905538E8A639C41E9F0570AD23F430A1700A2,
	WitDictation__ctor_m2ECD3B2372A9E938ADE0F10BD5B20DA8B0EAFEF9,
	DictationEvents_get_OnPartialTranscription_mABAB3495C693DEBF0D35FE5BA5D43AE0F1521CD2,
	DictationEvents_get_OnFullTranscription_m2EAF17EFABFB117FF97347672CEDB24631841E19,
	DictationEvents_get_OnMicAudioLevelChanged_m885ADF7E17D42AF2A7769AB43B772BB7427DE659,
	DictationEvents_get_OnMicStartedListening_m61B9505971AFE7C78995D888E1947A245AFA8788,
	DictationEvents_get_OnMicStoppedListening_m932AB2EF24DEB7081F275B6E88B81CFAED2A06AC,
	DictationEvents__ctor_m6405DC5FA653EAE2F23D4985272DBA57AC31B02C,
	DictationSessionEvent__ctor_m7768767815824DE024DA764BB9A3FAE03FD90FCF,
	DictationSession__ctor_mBD66C810B4D5E1DABD0D66F6F087888E5FEDCA0B,
};
extern void DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000001, DictationServiceReference_get_DictationService_m3E7F942FA0BB5AE83ABCB22F0C2B160708D3177D_AdjustorThunk },
};
static const int32_t s_InvokerIndices[82] = 
{
	3645,
	5676,
	3709,
	2275,
	3645,
	3709,
	3583,
	3583,
	3645,
	3049,
	3583,
	3645,
	3049,
	3645,
	3645,
	3583,
	3709,
	3049,
	3709,
	3049,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	3583,
	3583,
	3583,
	3645,
	3049,
	3645,
	3049,
	3709,
	3049,
	3709,
	3049,
	3709,
	3709,
	3709,
	3709,
	3709,
	3049,
	3049,
	3709,
	3709,
	3709,
	3645,
	3049,
	3583,
	3583,
	3645,
	3049,
	3583,
	3583,
	3645,
	1038,
	3709,
	3049,
	3709,
	3049,
	3709,
	3709,
	3709,
	3709,
	3709,
	3049,
	3049,
	3709,
	3709,
	3079,
	1864,
	3049,
	3709,
	3645,
	3645,
	3645,
	3645,
	3645,
	3709,
	3709,
	3709,
};
extern const CustomAttributesCacheGenerator g_Facebook_Wit_Dictation_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Facebook_Wit_Dictation_CodeGenModule;
const Il2CppCodeGenModule g_Facebook_Wit_Dictation_CodeGenModule = 
{
	"Facebook.Wit.Dictation.dll",
	82,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Facebook_Wit_Dictation_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
