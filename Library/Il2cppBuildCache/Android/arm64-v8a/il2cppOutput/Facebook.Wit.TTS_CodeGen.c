﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 Facebook.WitAi.TTS.TTSService Facebook.WitAi.TTS.TTSService::get_Instance()
extern void TTSService_get_Instance_mFF25B835D15F14F05A55DABDDCA273C81AAD16B1 (void);
// 0x00000002 Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler Facebook.WitAi.TTS.TTSService::get_RuntimeCacheHandler()
// 0x00000003 Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler Facebook.WitAi.TTS.TTSService::get_DiskCacheHandler()
// 0x00000004 Facebook.WitAi.TTS.Interfaces.ITTSWebHandler Facebook.WitAi.TTS.TTSService::get_WebHandler()
// 0x00000005 Facebook.WitAi.TTS.Interfaces.ITTSVoiceProvider Facebook.WitAi.TTS.TTSService::get_VoiceProvider()
// 0x00000006 System.String Facebook.WitAi.TTS.TTSService::IsValid()
extern void TTSService_IsValid_mCB28A08413A001CCC38AD6098C25666CD9FD5F04 (void);
// 0x00000007 Facebook.WitAi.TTS.Events.TTSServiceEvents Facebook.WitAi.TTS.TTSService::get_Events()
extern void TTSService_get_Events_m06F1D1284B100A3D0B4CBD95D3CA7CE64F1BCC81 (void);
// 0x00000008 System.Void Facebook.WitAi.TTS.TTSService::Awake()
extern void TTSService_Awake_mCA6CBAB8A7E4C9605F84D2E5F05F2BD19C858E5F (void);
// 0x00000009 System.Void Facebook.WitAi.TTS.TTSService::OnEnable()
extern void TTSService_OnEnable_m303E34DE93E3BC544732BA8FB3BE6EF3B65FEC93 (void);
// 0x0000000A System.Void Facebook.WitAi.TTS.TTSService::OnDisable()
extern void TTSService_OnDisable_m99847B634FC577734D55BFF83A1823B48319FA4D (void);
// 0x0000000B System.Void Facebook.WitAi.TTS.TTSService::AddDelegates()
extern void TTSService_AddDelegates_mF7F3A2E37714EA73829F76E9EA6023ED9DE8DA04 (void);
// 0x0000000C System.Void Facebook.WitAi.TTS.TTSService::RemoveDelegates()
extern void TTSService_RemoveDelegates_mA9F83505A87A38ED1A6C78FF3E83B7A21BB0CBA0 (void);
// 0x0000000D System.Void Facebook.WitAi.TTS.TTSService::OnDestroy()
extern void TTSService_OnDestroy_mDE1865B45CBF08A52C3C3D8F270854ACA3C4E4F3 (void);
// 0x0000000E System.Void Facebook.WitAi.TTS.TTSService::LogClip(System.String,Facebook.WitAi.TTS.Data.TTSClipData,Facebook.WitAi.TTS.TTSLogType)
extern void TTSService_LogClip_m49E7DFD2892430E83C9CC199FD861F53EEC50161 (void);
// 0x0000000F System.Void Facebook.WitAi.TTS.TTSService::Log(System.String,Facebook.WitAi.TTS.TTSLogType)
extern void TTSService_Log_m2B43940259CF73FCA2AC4147BE06ACC71F7D1325 (void);
// 0x00000010 System.String Facebook.WitAi.TTS.TTSService::GetClipID(System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings)
extern void TTSService_GetClipID_m6707CAA0C811216B41A7F9BB0C592937540856A6 (void);
// 0x00000011 System.String Facebook.WitAi.TTS.TTSService::GetSha256Hash(System.Security.Cryptography.SHA256,System.String)
extern void TTSService_GetSha256Hash_m87AB463FB0920DAAD4CF29D9CF02B5AD43FD5BAF (void);
// 0x00000012 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::CreateClipData(System.String,System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings)
extern void TTSService_CreateClipData_mBC31777A8AA35B1FF84222F80D11A7096A118F73 (void);
// 0x00000013 System.Void Facebook.WitAi.TTS.TTSService::SetClipLoadState(Facebook.WitAi.TTS.Data.TTSClipData,Facebook.WitAi.TTS.Data.TTSClipLoadState)
extern void TTSService_SetClipLoadState_m6A22818651434B42865C1A00B8397423196E06F7 (void);
// 0x00000014 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::Load(System.String,System.Action`2<Facebook.WitAi.TTS.Data.TTSClipData,System.String>)
extern void TTSService_Load_m0B82325D8FAC910AEC705C602362F42432BBD9E3 (void);
// 0x00000015 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::Load(System.String,System.String,System.Action`2<Facebook.WitAi.TTS.Data.TTSClipData,System.String>)
extern void TTSService_Load_mDC143748A29EACDF490D0D8AACD467DB4DB32F0C (void);
// 0x00000016 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::Load(System.String,System.String,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings,System.Action`2<Facebook.WitAi.TTS.Data.TTSClipData,System.String>)
extern void TTSService_Load_mB2ADF9F4811B036A8A2CF31F6B92DD302649A1A2 (void);
// 0x00000017 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::Load(System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings,System.Action`2<Facebook.WitAi.TTS.Data.TTSClipData,System.String>)
extern void TTSService_Load_m7238951240C68523A2FCCCE6E673B57416EB18AE (void);
// 0x00000018 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::Load(System.String,System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings,System.Action`2<Facebook.WitAi.TTS.Data.TTSClipData,System.String>)
extern void TTSService_Load_m7A6268734204791B7466AFC5A9F566FF8557811F (void);
// 0x00000019 System.Collections.IEnumerator Facebook.WitAi.TTS.TTSService::CallAfterAMoment(System.Action)
extern void TTSService_CallAfterAMoment_mC5A015778B9EFBDAA46F6932CAB9C2DA70F5B7F6 (void);
// 0x0000001A System.Void Facebook.WitAi.TTS.TTSService::OnLoadBegin(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnLoadBegin_m30DEA102388D8683CBDA0EA5944989A3D068976D (void);
// 0x0000001B System.Void Facebook.WitAi.TTS.TTSService::OnDiskStreamBegin(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnDiskStreamBegin_m8FD56D611C4A327BCCA2576AC2F6362BC76B4728 (void);
// 0x0000001C System.Void Facebook.WitAi.TTS.TTSService::OnWebStreamBegin(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnWebStreamBegin_mC499477878F00B8E4BADDC83892F28B311BB63FB (void);
// 0x0000001D System.Void Facebook.WitAi.TTS.TTSService::OnStreamBegin(Facebook.WitAi.TTS.Data.TTSClipData,System.Boolean)
extern void TTSService_OnStreamBegin_mDDD197372F8311D0BF9E9CC6C961FB4CD28972E5 (void);
// 0x0000001E System.Void Facebook.WitAi.TTS.TTSService::OnDiskStreamReady(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnDiskStreamReady_m4BBB310C572C3061E90CBC11D9F30A7B41D6398F (void);
// 0x0000001F System.Void Facebook.WitAi.TTS.TTSService::OnWebStreamReady(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnWebStreamReady_m5B74839229E7106A49A6F69C2ABA9548CFE819E3 (void);
// 0x00000020 System.Void Facebook.WitAi.TTS.TTSService::OnStreamReady(Facebook.WitAi.TTS.Data.TTSClipData,System.Boolean)
extern void TTSService_OnStreamReady_m5AF2064DF672361B7B63EA5394A783F2AB5AD751 (void);
// 0x00000021 System.Void Facebook.WitAi.TTS.TTSService::OnDiskStreamCancel(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnDiskStreamCancel_mDBF70537B475AEC80A9C07BE7BD6125C357A8A07 (void);
// 0x00000022 System.Void Facebook.WitAi.TTS.TTSService::OnWebStreamCancel(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnWebStreamCancel_m4E77C61A8E8F19460CC8929506D7D68B8D68A91C (void);
// 0x00000023 System.Void Facebook.WitAi.TTS.TTSService::OnStreamCancel(Facebook.WitAi.TTS.Data.TTSClipData,System.Boolean)
extern void TTSService_OnStreamCancel_m4812ABEC439B80CA4D7E829A59583DEECDDE071E (void);
// 0x00000024 System.Void Facebook.WitAi.TTS.TTSService::OnDiskStreamError(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSService_OnDiskStreamError_m224C948AB1CAAB8DB6C32C8FB09709A30C0357F0 (void);
// 0x00000025 System.Void Facebook.WitAi.TTS.TTSService::OnWebStreamError(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSService_OnWebStreamError_m50F4199E983FB36D9493AC865D30985A6F8D9AFE (void);
// 0x00000026 System.Void Facebook.WitAi.TTS.TTSService::OnStreamError(Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.Boolean)
extern void TTSService_OnStreamError_mFFCC21254211A971E119518446BBA2435CB7E2AC (void);
// 0x00000027 System.Void Facebook.WitAi.TTS.TTSService::UnloadAll()
extern void TTSService_UnloadAll_mDA8015DE04D69DC150D07B4B930683D12D52C3D9 (void);
// 0x00000028 System.Void Facebook.WitAi.TTS.TTSService::Unload(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_Unload_mD31B7917459D317584984D983966F5B3A520DA5C (void);
// 0x00000029 System.Void Facebook.WitAi.TTS.TTSService::OnUnloadBegin(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnUnloadBegin_m4CB978ABFBB236665F1D2C3FA0B19A4FDB850D23 (void);
// 0x0000002A Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::GetRuntimeCachedClip(System.String)
extern void TTSService_GetRuntimeCachedClip_mB10D0AC1EDA8C132BEDA1B39F9DB8C6CB230ED4B (void);
// 0x0000002B Facebook.WitAi.TTS.Data.TTSClipData[] Facebook.WitAi.TTS.TTSService::GetAllRuntimeCachedClips()
extern void TTSService_GetAllRuntimeCachedClips_m9905899547D3621A4C9AB8C812F43B988B24B7E2 (void);
// 0x0000002C System.Void Facebook.WitAi.TTS.TTSService::OnRuntimeClipAdded(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnRuntimeClipAdded_m9770E74F7E5E67E1A6F49BD74DF3F4E353380E14 (void);
// 0x0000002D System.Void Facebook.WitAi.TTS.TTSService::OnRuntimeClipRemoved(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_OnRuntimeClipRemoved_m5BD6FD06EBBED3746F08C08B9E580FDCA5370CDD (void);
// 0x0000002E System.Boolean Facebook.WitAi.TTS.TTSService::ShouldCacheToDisk(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSService_ShouldCacheToDisk_m9A5100F671847D4C41226D9FE0ADCAAC517AD43D (void);
// 0x0000002F System.String Facebook.WitAi.TTS.TTSService::GetDiskCachePath(System.String,System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings)
extern void TTSService_GetDiskCachePath_mF8B642C7534848070A1A2342D6686D594115D9E0 (void);
// 0x00000030 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::DownloadToDiskCache(System.String,System.Action`3<Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String>)
extern void TTSService_DownloadToDiskCache_m16BDCFDA5D4C5A4397567F771B97AD3A8D299A70 (void);
// 0x00000031 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::DownloadToDiskCache(System.String,System.String,System.Action`3<Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String>)
extern void TTSService_DownloadToDiskCache_m867C5502F21D5F42D926343AE03FEC2B79DB7080 (void);
// 0x00000032 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::DownloadToDiskCache(System.String,System.String,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings,System.Action`3<Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String>)
extern void TTSService_DownloadToDiskCache_mEA9B629448D0D52296C166E93391B8AD989A9B77 (void);
// 0x00000033 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::DownloadToDiskCache(System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings,System.Action`3<Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String>)
extern void TTSService_DownloadToDiskCache_mE574FE9787ECE69C8486AB1CA278A1DD8F3E5CB8 (void);
// 0x00000034 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.TTSService::DownloadToDiskCache(System.String,System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings,System.Action`3<Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String>)
extern void TTSService_DownloadToDiskCache_m409E83A72C3FC9C7BEE35569D7EA1A863C916496 (void);
// 0x00000035 System.Void Facebook.WitAi.TTS.TTSService::DownloadToDiskCache(Facebook.WitAi.TTS.Data.TTSClipData,System.Action`3<Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String>)
extern void TTSService_DownloadToDiskCache_m976123174CAABE1D3825D6FBB1131652FF0B4BD7 (void);
// 0x00000036 System.Void Facebook.WitAi.TTS.TTSService::OnWebDownloadBegin(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSService_OnWebDownloadBegin_m51DF3010BB349B03FEAD91DE4C9F7109289B2B57 (void);
// 0x00000037 System.Void Facebook.WitAi.TTS.TTSService::OnWebDownloadSuccess(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSService_OnWebDownloadSuccess_mF070E2AA86FE8E4320BFD5FAEAF54078B46CF605 (void);
// 0x00000038 System.Void Facebook.WitAi.TTS.TTSService::OnWebDownloadCancel(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSService_OnWebDownloadCancel_m6931C3695EFBA8A729786E3019602C1334B6DD14 (void);
// 0x00000039 System.Void Facebook.WitAi.TTS.TTSService::OnWebDownloadError(Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String)
extern void TTSService_OnWebDownloadError_mE133A65B687CA7E5DCF695A5249AF1AB0971AC9A (void);
// 0x0000003A Facebook.WitAi.TTS.Data.TTSVoiceSettings[] Facebook.WitAi.TTS.TTSService::GetAllPresetVoiceSettings()
extern void TTSService_GetAllPresetVoiceSettings_mDA814D5DA0824C02AAF76486E2E485606211E16A (void);
// 0x0000003B Facebook.WitAi.TTS.Data.TTSVoiceSettings Facebook.WitAi.TTS.TTSService::GetPresetVoiceSettings(System.String)
extern void TTSService_GetPresetVoiceSettings_mB4B6468F6CED75C7E62AD33A6A6BACE788937880 (void);
// 0x0000003C System.Void Facebook.WitAi.TTS.TTSService::.ctor()
extern void TTSService__ctor_mD1388977DC2D00CFEFBE3879E43E5FC48A04C178 (void);
// 0x0000003D System.Void Facebook.WitAi.TTS.TTSService/<>c::.cctor()
extern void U3CU3Ec__cctor_m1F50EAE30DC688E644DE334AD4284A140C825F56 (void);
// 0x0000003E System.Void Facebook.WitAi.TTS.TTSService/<>c::.ctor()
extern void U3CU3Ec__ctor_m52305550C25D7AE01F4D45D8D53E2ADD3C35B028 (void);
// 0x0000003F System.Boolean Facebook.WitAi.TTS.TTSService/<>c::<get_Instance>b__1_0(Facebook.WitAi.TTS.TTSService)
extern void U3CU3Ec_U3Cget_InstanceU3Eb__1_0_mE8FBDC870108A4AA2F93AC1AEAD86A2714D6E1BD (void);
// 0x00000040 System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m4A14532CB34A98EAA3ED24F80238369A689D426F (void);
// 0x00000041 System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass36_0::<Load>b__2(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__2_mB529AFD4F19D81C0EE4B6AFAB8C96546B330AA46 (void);
// 0x00000042 System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass36_0::<Load>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__0_m4CF30280CA7D1D86D7FAF263E2960AFAC40B1A19 (void);
// 0x00000043 System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass36_0::<Load>b__3(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__3_m9B7E2E07E49AE52F612B1EEAC8C5585B327CAC37 (void);
// 0x00000044 System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass36_0::<Load>b__1()
extern void U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__1_mF702814DD580D25CA27A2A2496267C48E57CE2D0 (void);
// 0x00000045 System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass36_0::<Load>b__4(Facebook.WitAi.TTS.Data.TTSClipData,System.String,System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__4_m4459DFB80164C2288FCC04BCEB7D021520CD5D69 (void);
// 0x00000046 System.Void Facebook.WitAi.TTS.TTSService/<CallAfterAMoment>d__37::.ctor(System.Int32)
extern void U3CCallAfterAMomentU3Ed__37__ctor_m37777D07ACA2996D4A4E7BE126F4EB2A4A191D34 (void);
// 0x00000047 System.Void Facebook.WitAi.TTS.TTSService/<CallAfterAMoment>d__37::System.IDisposable.Dispose()
extern void U3CCallAfterAMomentU3Ed__37_System_IDisposable_Dispose_m72FB1A36C609A7F7825CD558A775650B2EE81D4F (void);
// 0x00000048 System.Boolean Facebook.WitAi.TTS.TTSService/<CallAfterAMoment>d__37::MoveNext()
extern void U3CCallAfterAMomentU3Ed__37_MoveNext_mECE2F604E8E761334DE92B586A36C19F285AF07F (void);
// 0x00000049 System.Object Facebook.WitAi.TTS.TTSService/<CallAfterAMoment>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallAfterAMomentU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F19C5C2C93E68EF06097AC36049C9A7EFEB9966 (void);
// 0x0000004A System.Void Facebook.WitAi.TTS.TTSService/<CallAfterAMoment>d__37::System.Collections.IEnumerator.Reset()
extern void U3CCallAfterAMomentU3Ed__37_System_Collections_IEnumerator_Reset_m11D596EBE43F72BE456EA459C938348A9B3C4F95 (void);
// 0x0000004B System.Object Facebook.WitAi.TTS.TTSService/<CallAfterAMoment>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CCallAfterAMomentU3Ed__37_System_Collections_IEnumerator_get_Current_m225AF082AE7459DBE9EB574827E6043B94CD2F3E (void);
// 0x0000004C System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass65_0::.ctor()
extern void U3CU3Ec__DisplayClass65_0__ctor_m4F3314CFEE3914DF79682DCC56E275DA16F025F8 (void);
// 0x0000004D System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass65_0::<DownloadToDiskCache>b__0(System.String)
extern void U3CU3Ec__DisplayClass65_0_U3CDownloadToDiskCacheU3Eb__0_mB7A1CF9E4DCEBB9907BA8FDCD7D95BFBBF53FC73 (void);
// 0x0000004E System.Void Facebook.WitAi.TTS.TTSService/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_m0314885970A29FB27439C4A3510D8CC235E7E01C (void);
// 0x0000004F System.Boolean Facebook.WitAi.TTS.TTSService/<>c__DisplayClass71_0::<GetPresetVoiceSettings>b__0(Facebook.WitAi.TTS.Data.TTSVoiceSettings)
extern void U3CU3Ec__DisplayClass71_0_U3CGetPresetVoiceSettingsU3Eb__0_mF34D72B8BC07D929DFDD88C8EDBAFF8ABFECC982 (void);
// 0x00000050 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeakerEvent::.ctor()
extern void TTSSpeakerEvent__ctor_m91F1A08BD8CFDF029E7411BCADC437852DCF3296 (void);
// 0x00000051 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeakerEvents::.ctor()
extern void TTSSpeakerEvents__ctor_m8B8134DF7FD6D64CBA3FA78AFFAED0EEBDDA9C85 (void);
// 0x00000052 Facebook.WitAi.TTS.Data.TTSVoiceSettings Facebook.WitAi.TTS.Utilities.TTSSpeaker::get_VoiceSettings()
extern void TTSSpeaker_get_VoiceSettings_mF88416F754DD9F11FD0CEAA20B5B15E3F02054BB (void);
// 0x00000053 Facebook.WitAi.TTS.Utilities.TTSSpeakerEvents Facebook.WitAi.TTS.Utilities.TTSSpeaker::get_Events()
extern void TTSSpeaker_get_Events_m583EAB517C1DD194A0B2AA54BC4087D21E27D920 (void);
// 0x00000054 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::Awake()
extern void TTSSpeaker_Awake_mABE45A26A4A2B217FDBC3CE5A792C8AC213C37D5 (void);
// 0x00000055 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnDestroy()
extern void TTSSpeaker_OnDestroy_m70344F01EB42CF636BE91FC57C7DD566AD0F3915 (void);
// 0x00000056 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnClipUnload(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSSpeaker_OnClipUnload_mD1270333BED971CF92AC67A4CDE6892CBAF137C4 (void);
// 0x00000057 System.Boolean Facebook.WitAi.TTS.Utilities.TTSSpeaker::IsClipSame(System.String,Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSSpeaker_IsClipSame_m2AA68F06E86FC6981F28531B30341362C17F3F67 (void);
// 0x00000058 System.Boolean Facebook.WitAi.TTS.Utilities.TTSSpeaker::get_IsSpeaking()
extern void TTSSpeaker_get_IsSpeaking_m985AC30CAC623382DCE67EE529979F5CDDB6D903 (void);
// 0x00000059 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::Speak(System.String,System.String[])
extern void TTSSpeaker_Speak_mBB84CF1C9F67F53ABF505BDC745ECF83288A0BCE (void);
// 0x0000005A System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::Speak(System.String)
extern void TTSSpeaker_Speak_m7105B1F4C4D2BD4DA9A6FC2579BDA95662C0EE2C (void);
// 0x0000005B System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::Speak(System.String,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings)
extern void TTSSpeaker_Speak_m6916A94C1DF27FF23C7F0F0AF866D2AE9DFEE3E9 (void);
// 0x0000005C System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::Stop()
extern void TTSSpeaker_Stop_mAE3291B82D82E0F0AC5E034A3152C5AF41DBA8FE (void);
// 0x0000005D System.Boolean Facebook.WitAi.TTS.Utilities.TTSSpeaker::get_IsLoading()
extern void TTSSpeaker_get_IsLoading_mA08DA3689E8137CF0BFA4F64155F27CDE488D526 (void);
// 0x0000005E System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnLoadBegin(System.String,System.String,Facebook.WitAi.TTS.Data.TTSVoiceSettings,Facebook.WitAi.TTS.Data.TTSDiskCacheSettings)
extern void TTSSpeaker_OnLoadBegin_m08C6688B2B01CA97319044058E20D58705E17D3E (void);
// 0x0000005F System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnLoadComplete(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSSpeaker_OnLoadComplete_m48D044FEA678A7884B64C0B24271271BA95C4A33 (void);
// 0x00000060 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnLoadAbort()
extern void TTSSpeaker_OnLoadAbort_m598127914818098D441BEEC6BA6FD3BEE1B399FC (void);
// 0x00000061 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnPlaybackBegin(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSSpeaker_OnPlaybackBegin_m310AEC19BCF11E94DFCE2068C24DBF94D6731A95 (void);
// 0x00000062 System.Collections.IEnumerator Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnPlaybackWait()
extern void TTSSpeaker_OnPlaybackWait_mD7EA1E586D2C1BF68655B2F8D65F70CA96E26754 (void);
// 0x00000063 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnPlaybackComplete()
extern void TTSSpeaker_OnPlaybackComplete_m914AB1CF8323823A6F230985C432C1A14943A8C4 (void);
// 0x00000064 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::OnPlaybackCancel()
extern void TTSSpeaker_OnPlaybackCancel_mF495D1E33974E1D092DD6A7404D65864C12FB7BF (void);
// 0x00000065 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker::.ctor()
extern void TTSSpeaker__ctor_mFFCAF4AE3B4340275D39C6EDFA67A69D826D9E89 (void);
// 0x00000066 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker/<OnPlaybackWait>d__27::.ctor(System.Int32)
extern void U3COnPlaybackWaitU3Ed__27__ctor_mA6F7D3F3D6CDD566D3805B8010D4F429E2C79F41 (void);
// 0x00000067 System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker/<OnPlaybackWait>d__27::System.IDisposable.Dispose()
extern void U3COnPlaybackWaitU3Ed__27_System_IDisposable_Dispose_mFC9D7965A33B1A24EBD5363D2AD2A68A749C82C5 (void);
// 0x00000068 System.Boolean Facebook.WitAi.TTS.Utilities.TTSSpeaker/<OnPlaybackWait>d__27::MoveNext()
extern void U3COnPlaybackWaitU3Ed__27_MoveNext_mB58A7ECC7372DD7FBAC28E89B858BD3377BB36CA (void);
// 0x00000069 System.Object Facebook.WitAi.TTS.Utilities.TTSSpeaker/<OnPlaybackWait>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COnPlaybackWaitU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF3ABA74E1BB23E06E23B9AC3D32B56C0C57D947 (void);
// 0x0000006A System.Void Facebook.WitAi.TTS.Utilities.TTSSpeaker/<OnPlaybackWait>d__27::System.Collections.IEnumerator.Reset()
extern void U3COnPlaybackWaitU3Ed__27_System_Collections_IEnumerator_Reset_m5EADB8420EA6DB8952D80F3BE2EC1FC91C4D96A4 (void);
// 0x0000006B System.Object Facebook.WitAi.TTS.Utilities.TTSSpeaker/<OnPlaybackWait>d__27::System.Collections.IEnumerator.get_Current()
extern void U3COnPlaybackWaitU3Ed__27_System_Collections_IEnumerator_get_Current_m57682E8213AF013E824849D8548608D9B608AD44 (void);
// 0x0000006C Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::RequestAudioClip(System.String,System.Action`3<System.String,UnityEngine.AudioClip,System.String>)
extern void VoiceUnityRequest_RequestAudioClip_m8DCA4C5E6230A872E2E4068FD96E1B68316186A3 (void);
// 0x0000006D Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::RequestAudioClip(System.String,System.Action`2<System.String,System.Single>,System.Action`3<System.String,UnityEngine.AudioClip,System.String>)
extern void VoiceUnityRequest_RequestAudioClip_m613AAEE7AA46AB40615549A82BE2ABA081387785 (void);
// 0x0000006E Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::RequestAudioClip(System.String,UnityEngine.AudioType,System.Action`3<System.String,UnityEngine.AudioClip,System.String>)
extern void VoiceUnityRequest_RequestAudioClip_m92A838E7A81D3973785416FDDF95C7C295418E1A (void);
// 0x0000006F Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::RequestAudioClip(System.String,UnityEngine.AudioType,System.Action`2<System.String,System.Single>,System.Action`3<System.String,UnityEngine.AudioClip,System.String>)
extern void VoiceUnityRequest_RequestAudioClip_m7673C148E75F6964C41165E5252B1E066ABB7561 (void);
// 0x00000070 Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::RequestFile(System.String,System.Action`2<System.String,UnityEngine.Networking.UnityWebRequest>)
extern void VoiceUnityRequest_RequestFile_m4BA24586EA0AF5FE9A1F3ECF9B09E47F587462B0 (void);
// 0x00000071 Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::RequestFile(System.String,System.Action`2<System.String,System.Single>,System.Action`2<System.String,UnityEngine.Networking.UnityWebRequest>)
extern void VoiceUnityRequest_RequestFile_m194F6996C286AE8CF71C30D5B1AA9A5D43E14AF7 (void);
// 0x00000072 Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::Request(UnityEngine.Networking.UnityWebRequest,System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void VoiceUnityRequest_Request_m14FA8140411D3FC641D27601F6DB46CD2C3EB137 (void);
// 0x00000073 Facebook.WitAi.TTS.Utilities.VoiceUnityRequest Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::Request(UnityEngine.Networking.UnityWebRequest,System.Action`1<System.Single>,System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void VoiceUnityRequest_Request_m13FD11371E047AD2238370D5D552B51B876A2C4C (void);
// 0x00000074 System.Boolean Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::get_IsTransmitting()
extern void VoiceUnityRequest_get_IsTransmitting_m3459E362B29EC8C0F86E95D83AAE0CD0D4368DC4 (void);
// 0x00000075 System.Single Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::get_Progress()
extern void VoiceUnityRequest_get_Progress_m817D73C8CCE755D13E1AEC3D13DD03F48678C4A8 (void);
// 0x00000076 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::Setup(UnityEngine.Networking.UnityWebRequest,System.Action`1<System.Single>,System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void VoiceUnityRequest_Setup_m432C9422D7063C3329370B06B5D247C417E123B8 (void);
// 0x00000077 System.Collections.IEnumerator Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::PerformUpdate()
extern void VoiceUnityRequest_PerformUpdate_mB458EB05238436A59D00324232D2C6383AC0049D (void);
// 0x00000078 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::Begin()
extern void VoiceUnityRequest_Begin_mC3C14BFD664CE3D7B991B16B24BC1D52FAC98E69 (void);
// 0x00000079 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::Complete()
extern void VoiceUnityRequest_Complete_mDBC751DC6A0CD36274ED0B025BA264C9B271D5A0 (void);
// 0x0000007A System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::Unload()
extern void VoiceUnityRequest_Unload_m9D48336082E16B65322763BF76874A5743C6CB65 (void);
// 0x0000007B System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::.ctor()
extern void VoiceUnityRequest__ctor_mDA87B87E9C02DFFD969870F803D7B9AF7EF0A603 (void);
// 0x0000007C System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest::.cctor()
extern void VoiceUnityRequest__cctor_m720BB52EDDA6FAABF1C86C0E7F2D1A1D06BA56B7 (void);
// 0x0000007D System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m4E2895C1EBAF4745BCAD79C5AA0F2A8C6B5D1652 (void);
// 0x0000007E System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<>c__DisplayClass3_0::<RequestAudioClip>b__0(System.Single)
extern void U3CU3Ec__DisplayClass3_0_U3CRequestAudioClipU3Eb__0_mC3F732AE56425726A710788B02AA7E307D2FE70F (void);
// 0x0000007F System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<>c__DisplayClass3_0::<RequestAudioClip>b__1(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec__DisplayClass3_0_U3CRequestAudioClipU3Eb__1_m48E6D14096C1E116D332F15B6A7464B01627B8A5 (void);
// 0x00000080 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mC47301F0ACDC7E8F330BE88D139F3CCD7E010C52 (void);
// 0x00000081 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<>c__DisplayClass5_0::<RequestFile>b__0(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CRequestFileU3Eb__0_m73E5A1AC02EBD21B61D2E1A39DC250DF21A12BAE (void);
// 0x00000082 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<>c__DisplayClass5_0::<RequestFile>b__1(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec__DisplayClass5_0_U3CRequestFileU3Eb__1_m7BDC6993EB33E1A6F51A346CDEB74835786F226B (void);
// 0x00000083 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<PerformUpdate>d__22::.ctor(System.Int32)
extern void U3CPerformUpdateU3Ed__22__ctor_m1463A408184EC083E98C0FC4236CE9202101AFFA (void);
// 0x00000084 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<PerformUpdate>d__22::System.IDisposable.Dispose()
extern void U3CPerformUpdateU3Ed__22_System_IDisposable_Dispose_m87302484AFF457DFE1FE15C10982716826136235 (void);
// 0x00000085 System.Boolean Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<PerformUpdate>d__22::MoveNext()
extern void U3CPerformUpdateU3Ed__22_MoveNext_m45A25A11DF59B6DEED9114B1D3607A06765FFEC5 (void);
// 0x00000086 System.Object Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<PerformUpdate>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPerformUpdateU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4167B21B2B70ABD2C137EE40549B13BF9DBF185A (void);
// 0x00000087 System.Void Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<PerformUpdate>d__22::System.Collections.IEnumerator.Reset()
extern void U3CPerformUpdateU3Ed__22_System_Collections_IEnumerator_Reset_m12EA9A30426BF3DE3470F4580B54E8368DF6D8F8 (void);
// 0x00000088 System.Object Facebook.WitAi.TTS.Utilities.VoiceUnityRequest/<PerformUpdate>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CPerformUpdateU3Ed__22_System_Collections_IEnumerator_get_Current_m33C70FFDB7CD4A2483449AE0819F2566D09B2A72 (void);
// 0x00000089 System.String Facebook.WitAi.TTS.Utilities.WitUnityRequest::IsTextValid(System.String)
extern void WitUnityRequest_IsTextValid_m33DF9C50AF44B1025D2A1BAEC384889A0488CF35 (void);
// 0x0000008A Facebook.WitAi.TTS.Utilities.WitUnityRequest Facebook.WitAi.TTS.Utilities.WitUnityRequest::RequestTTSStream(Facebook.WitAi.Data.Configuration.WitConfiguration,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Action`1<System.Single>,System.Action`2<UnityEngine.AudioClip,System.String>)
extern void WitUnityRequest_RequestTTSStream_m1E3F6346FF7665A72833944BAC09ACA59F6D43C4 (void);
// 0x0000008B Facebook.WitAi.TTS.Utilities.WitUnityRequest Facebook.WitAi.TTS.Utilities.WitUnityRequest::RequestTTSDownload(System.String,Facebook.WitAi.Data.Configuration.WitConfiguration,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Action`1<System.Single>,System.Action`1<System.String>)
extern void WitUnityRequest_RequestTTSDownload_m5E8292F4B56A623242E8D3D7A0B35D9F8D1EB6CF (void);
// 0x0000008C Facebook.WitAi.TTS.Utilities.WitUnityRequest Facebook.WitAi.TTS.Utilities.WitUnityRequest::RequestTTS(Facebook.WitAi.Data.Configuration.WitConfiguration,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>,System.Action`2<UnityEngine.Networking.UnityWebRequest,System.Uri>,System.Action`1<System.Single>,System.Action`2<UnityEngine.Networking.UnityWebRequest,System.String>)
extern void WitUnityRequest_RequestTTS_m905686D3CB7B73D11029ECFE22B2E0736662E765 (void);
// 0x0000008D Facebook.WitAi.TTS.Utilities.WitUnityRequest Facebook.WitAi.TTS.Utilities.WitUnityRequest::RequestTTSVoices(Facebook.WitAi.Data.Configuration.WitConfiguration,System.Action`1<System.Single>,System.Action`2<System.String,System.String>)
extern void WitUnityRequest_RequestTTSVoices_m43E737F242DBD0711540E1D0985C812CEC322E34 (void);
// 0x0000008E System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::add_OnProvideCustomUri(System.Func`2<System.UriBuilder,System.Uri>)
extern void WitUnityRequest_add_OnProvideCustomUri_mEF29CDB91F1DDA5DC69BB689DDA28E50925002A2 (void);
// 0x0000008F System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::remove_OnProvideCustomUri(System.Func`2<System.UriBuilder,System.Uri>)
extern void WitUnityRequest_remove_OnProvideCustomUri_m84F4757D227D02AEA6EEF84B275C198B9DEC041F (void);
// 0x00000090 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::add_OnProvideCustomHeaders(System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern void WitUnityRequest_add_OnProvideCustomHeaders_m2AA5C0F16B8DDD8B8A11CAB7317EFAEB275A2C1A (void);
// 0x00000091 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::remove_OnProvideCustomHeaders(System.Func`1<System.Collections.Generic.Dictionary`2<System.String,System.String>>)
extern void WitUnityRequest_remove_OnProvideCustomHeaders_m894949D05F2CE99E3BDFC2EA6965A2C70C27D0F2 (void);
// 0x00000092 System.Uri Facebook.WitAi.TTS.Utilities.WitUnityRequest::GetUri(Facebook.WitAi.Data.Configuration.WitConfiguration,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void WitUnityRequest_GetUri_mE223EB4C4C1DFF896D4EC447DBFD32E03BED9180 (void);
// 0x00000093 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::ApplyHeaders(Facebook.WitAi.Data.Configuration.WitConfiguration,UnityEngine.Networking.UnityWebRequest)
extern void WitUnityRequest_ApplyHeaders_m66A3CD0A060491A1B1199E74FC727E8D36EA9539 (void);
// 0x00000094 System.String Facebook.WitAi.TTS.Utilities.WitUnityRequest::GetAuthorization(Facebook.WitAi.Data.Configuration.WitConfiguration)
extern void WitUnityRequest_GetAuthorization_mD94836A0E01AAB8C6FBD3F120EBF0EF5505E52A8 (void);
// 0x00000095 Facebook.WitAi.TTS.Utilities.WitUnityRequest Facebook.WitAi.TTS.Utilities.WitUnityRequest::RequestWit(Facebook.WitAi.Data.Configuration.WitConfiguration,UnityEngine.Networking.UnityWebRequest,System.Action`1<System.Single>,System.Action`1<UnityEngine.Networking.UnityWebRequest>)
extern void WitUnityRequest_RequestWit_mE3D28982EDDF840FDF3B3ED41068831F4D29BFC8 (void);
// 0x00000096 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::.ctor()
extern void WitUnityRequest__ctor_m846947D250A727B50EFB1554F3FD16E10DCAA9CE (void);
// 0x00000097 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest::.cctor()
extern void WitUnityRequest__cctor_mA342096E18D6C2FECC39DD1642E662636E9CDE57 (void);
// 0x00000098 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m1FA2008BC9ADAFB60568CFBB421FF5C8FA05BF11 (void);
// 0x00000099 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass4_0::<RequestTTSStream>b__1(UnityEngine.Networking.UnityWebRequest,System.String)
extern void U3CU3Ec__DisplayClass4_0_U3CRequestTTSStreamU3Eb__1_mAB685831368A054FA8E58FAE02A631C6097DED0B (void);
// 0x0000009A System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c::.cctor()
extern void U3CU3Ec__cctor_mE50E997F9A1B5C756360C2A8AA560F9FFC37CCFB (void);
// 0x0000009B System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c::.ctor()
extern void U3CU3Ec__ctor_m6D7160312322CB80311B3E4CF0C1450FFE310C44 (void);
// 0x0000009C System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c::<RequestTTSStream>b__4_0(UnityEngine.Networking.UnityWebRequest,System.Uri)
extern void U3CU3Ec_U3CRequestTTSStreamU3Eb__4_0_mDEEB0E1EFBA1E4AF999C3D875397CA2F69112816 (void);
// 0x0000009D System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m29E0695E357DABC58B786182070B058C6CEDE0D9 (void);
// 0x0000009E System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass5_0::<RequestTTSDownload>b__0(UnityEngine.Networking.UnityWebRequest,System.Uri)
extern void U3CU3Ec__DisplayClass5_0_U3CRequestTTSDownloadU3Eb__0_m5E393456DA4968BFE99E55D000BF9F05579899B7 (void);
// 0x0000009F System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass5_0::<RequestTTSDownload>b__1(UnityEngine.Networking.UnityWebRequest,System.String)
extern void U3CU3Ec__DisplayClass5_0_U3CRequestTTSDownloadU3Eb__1_mE5296949D0DB166ABF0125D21CD4EFE033AF8C42 (void);
// 0x000000A0 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mADE1745BE4A2B61081CB495CCDDAB8282318F8A2 (void);
// 0x000000A1 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass6_0::<RequestTTS>b__0(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec__DisplayClass6_0_U3CRequestTTSU3Eb__0_m54DAEB3A49FBF231F3AA154FAEDA70A445A3FA94 (void);
// 0x000000A2 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mF2B650E7BEBE7A40F3F9FCD956977083A79E85DB (void);
// 0x000000A3 System.Void Facebook.WitAi.TTS.Utilities.WitUnityRequest/<>c__DisplayClass8_0::<RequestTTSVoices>b__0(UnityEngine.Networking.UnityWebRequest)
extern void U3CU3Ec__DisplayClass8_0_U3CRequestTTSVoicesU3Eb__0_mF907D203DE92EB45D593C696D2D78C10FF48357A (void);
// 0x000000A4 Facebook.WitAi.TTS.Events.TTSStreamEvents Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::get_DiskStreamEvents()
// 0x000000A5 System.Void Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::set_DiskStreamEvents(Facebook.WitAi.TTS.Events.TTSStreamEvents)
// 0x000000A6 Facebook.WitAi.TTS.Data.TTSDiskCacheSettings Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::get_DiskCacheDefaultSettings()
// 0x000000A7 System.String Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::GetDiskCachePath(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000A8 System.Boolean Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::ShouldCacheToDisk(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000A9 System.Boolean Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::IsCachedToDisk(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000AA System.Void Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::StreamFromDiskCache(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000AB System.Void Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler::CancelDiskCacheStream(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000AC Facebook.WitAi.TTS.Events.TTSClipEvent Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::get_OnClipAdded()
// 0x000000AD System.Void Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::set_OnClipAdded(Facebook.WitAi.TTS.Events.TTSClipEvent)
// 0x000000AE Facebook.WitAi.TTS.Events.TTSClipEvent Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::get_OnClipRemoved()
// 0x000000AF System.Void Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::set_OnClipRemoved(Facebook.WitAi.TTS.Events.TTSClipEvent)
// 0x000000B0 Facebook.WitAi.TTS.Data.TTSClipData[] Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::GetClips()
// 0x000000B1 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::GetClip(System.String)
// 0x000000B2 System.Void Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::AddClip(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000B3 System.Void Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler::RemoveClip(System.String)
// 0x000000B4 Facebook.WitAi.TTS.Data.TTSVoiceSettings Facebook.WitAi.TTS.Interfaces.ITTSVoiceProvider::get_VoiceDefaultSettings()
// 0x000000B5 Facebook.WitAi.TTS.Data.TTSVoiceSettings[] Facebook.WitAi.TTS.Interfaces.ITTSVoiceProvider::get_PresetVoiceSettings()
// 0x000000B6 System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.WitAi.TTS.Interfaces.ITTSVoiceProvider::EncodeVoiceSettings(Facebook.WitAi.TTS.Data.TTSVoiceSettings)
// 0x000000B7 Facebook.WitAi.TTS.Events.TTSStreamEvents Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::get_WebStreamEvents()
// 0x000000B8 System.Void Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::set_WebStreamEvents(Facebook.WitAi.TTS.Events.TTSStreamEvents)
// 0x000000B9 System.String Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::IsTextValid(System.String)
// 0x000000BA System.Void Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::RequestStreamFromWeb(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000BB System.Boolean Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::CancelWebStream(Facebook.WitAi.TTS.Data.TTSClipData)
// 0x000000BC Facebook.WitAi.TTS.Events.TTSDownloadEvents Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::get_WebDownloadEvents()
// 0x000000BD System.Void Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::set_WebDownloadEvents(Facebook.WitAi.TTS.Events.TTSDownloadEvents)
// 0x000000BE System.Void Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::RequestDownloadFromWeb(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
// 0x000000BF System.Boolean Facebook.WitAi.TTS.Interfaces.ITTSWebHandler::CancelWebDownload(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
// 0x000000C0 System.String Facebook.WitAi.TTS.Integrations.TTSDiskCache::get_DiskPath()
extern void TTSDiskCache_get_DiskPath_m9C4400B751977975C2B5EAF0CEFE051BDD379A4E (void);
// 0x000000C1 Facebook.WitAi.TTS.Data.TTSDiskCacheSettings Facebook.WitAi.TTS.Integrations.TTSDiskCache::get_DiskCacheDefaultSettings()
extern void TTSDiskCache_get_DiskCacheDefaultSettings_m3034BA67F5BFB8A7E299A6362ED1595D5F386A0B (void);
// 0x000000C2 Facebook.WitAi.TTS.Events.TTSStreamEvents Facebook.WitAi.TTS.Integrations.TTSDiskCache::get_DiskStreamEvents()
extern void TTSDiskCache_get_DiskStreamEvents_mE71C389875BD8935ACE37608675C06C94A4F8F44 (void);
// 0x000000C3 System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache::set_DiskStreamEvents(Facebook.WitAi.TTS.Events.TTSStreamEvents)
extern void TTSDiskCache_set_DiskStreamEvents_m47AC9C092D037AD0581C7B10D779E9CB1CBC35F3 (void);
// 0x000000C4 System.String Facebook.WitAi.TTS.Integrations.TTSDiskCache::GetDiskCachePath(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSDiskCache_GetDiskCachePath_m2C3F22163D49C272A1B17FD8F1E931D452BA881F (void);
// 0x000000C5 System.Boolean Facebook.WitAi.TTS.Integrations.TTSDiskCache::ShouldCacheToDisk(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSDiskCache_ShouldCacheToDisk_mFD87A13D48BA79EBAC073C0C4FB30754C2A51698 (void);
// 0x000000C6 System.Boolean Facebook.WitAi.TTS.Integrations.TTSDiskCache::IsCachedToDisk(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSDiskCache_IsCachedToDisk_mB7B83E14F04AD85686D4394FBEE5CAC1941F9AFB (void);
// 0x000000C7 System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache::StreamFromDiskCache(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSDiskCache_StreamFromDiskCache_m5FDBDFB3C1B78AFB0E3059649538F0D1A32699BD (void);
// 0x000000C8 System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache::CancelDiskCacheStream(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSDiskCache_CancelDiskCacheStream_m2A49D1062FD79617717A0E123FAF963F13750EB4 (void);
// 0x000000C9 System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache::OnStreamComplete(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSDiskCache_OnStreamComplete_m78386E49C23F45D748CF3B0B59E30600344D665A (void);
// 0x000000CA System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache::.ctor()
extern void TTSDiskCache__ctor_mA82FB16B2B84DE93989222FA45923BDC72AC1F6B (void);
// 0x000000CB System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_mDED57EC8D6923E0B32C9AE390B86109071A75A45 (void);
// 0x000000CC System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache/<>c__DisplayClass14_0::<StreamFromDiskCache>b__0(System.String,System.Single)
extern void U3CU3Ec__DisplayClass14_0_U3CStreamFromDiskCacheU3Eb__0_m04394DDD8B89261395395CB77C418FAA4B5FC86B (void);
// 0x000000CD System.Void Facebook.WitAi.TTS.Integrations.TTSDiskCache/<>c__DisplayClass14_0::<StreamFromDiskCache>b__1(System.String,UnityEngine.AudioClip,System.String)
extern void U3CU3Ec__DisplayClass14_0_U3CStreamFromDiskCacheU3Eb__1_m5D033B5BC8E803223797D4EAADEDB10DEF3D9D3F (void);
// 0x000000CE Facebook.WitAi.TTS.Events.TTSClipEvent Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::get_OnClipAdded()
extern void TTSRuntimeCache_get_OnClipAdded_m7AA3587047D184609CAFA096802209B676784B40 (void);
// 0x000000CF System.Void Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::set_OnClipAdded(Facebook.WitAi.TTS.Events.TTSClipEvent)
extern void TTSRuntimeCache_set_OnClipAdded_m10E837E5B7E270462879D43E412043E646E21B94 (void);
// 0x000000D0 Facebook.WitAi.TTS.Events.TTSClipEvent Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::get_OnClipRemoved()
extern void TTSRuntimeCache_get_OnClipRemoved_m33EFB809EC5749E7202343D42380BEA2B615596C (void);
// 0x000000D1 System.Void Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::set_OnClipRemoved(Facebook.WitAi.TTS.Events.TTSClipEvent)
extern void TTSRuntimeCache_set_OnClipRemoved_m3DE1A59BDD3E520BD5E09957A828B7EC25B0C6D1 (void);
// 0x000000D2 Facebook.WitAi.TTS.Data.TTSClipData[] Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::GetClips()
extern void TTSRuntimeCache_GetClips_mA8067C4BC4AC988EB79AEF88EF64F3CB03705E14 (void);
// 0x000000D3 Facebook.WitAi.TTS.Data.TTSClipData Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::GetClip(System.String)
extern void TTSRuntimeCache_GetClip_m31C8155AB897C4EB633B204FF79D4DBBFB50FFBC (void);
// 0x000000D4 System.Void Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::AddClip(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSRuntimeCache_AddClip_m8F231C0C4A6351459D386AD7C08DECACCBD5AFA0 (void);
// 0x000000D5 System.Void Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::RemoveClip(System.String)
extern void TTSRuntimeCache_RemoveClip_m65B9C104281646C3E258B89C3976099C78CA9984 (void);
// 0x000000D6 System.Boolean Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::IsCacheFull()
extern void TTSRuntimeCache_IsCacheFull_mE5F1943B9BF6D45CBBF6CA3883BA7FB89691422F (void);
// 0x000000D7 System.Int32 Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::GetCacheDiskSize()
extern void TTSRuntimeCache_GetCacheDiskSize_m527CEC70E430910EA93F0A02395DC61DB658CEBB (void);
// 0x000000D8 System.Int64 Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::GetClipBytes(UnityEngine.AudioClip)
extern void TTSRuntimeCache_GetClipBytes_m73681FFE79FC8F8C86914CA5AB11CABD94FE9AB5 (void);
// 0x000000D9 System.Void Facebook.WitAi.TTS.Integrations.TTSRuntimeCache::.ctor()
extern void TTSRuntimeCache__ctor_m60ADC850E585A92CB44B2FDE9BC2BE6EE4EBF2A2 (void);
// 0x000000DA System.Void Facebook.WitAi.TTS.Integrations.TTSWitVoiceSettings::.ctor()
extern void TTSWitVoiceSettings__ctor_m35D5DD2388877CFED6571271F44DD351D27CEA74 (void);
// 0x000000DB Facebook.WitAi.TTS.Interfaces.ITTSVoiceProvider Facebook.WitAi.TTS.Integrations.TTSWit::get_VoiceProvider()
extern void TTSWit_get_VoiceProvider_m4BDDDBF8F69A5AD3C1B922E4E61FDE7DE3139773 (void);
// 0x000000DC Facebook.WitAi.TTS.Interfaces.ITTSWebHandler Facebook.WitAi.TTS.Integrations.TTSWit::get_WebHandler()
extern void TTSWit_get_WebHandler_mED5B3C139BE568EDAD0C0955A48AE86E4DE8889F (void);
// 0x000000DD Facebook.WitAi.TTS.Interfaces.ITTSRuntimeCacheHandler Facebook.WitAi.TTS.Integrations.TTSWit::get_RuntimeCacheHandler()
extern void TTSWit_get_RuntimeCacheHandler_mA73622A3193507273BB8B38751622B81BAD99431 (void);
// 0x000000DE Facebook.WitAi.TTS.Interfaces.ITTSDiskCacheHandler Facebook.WitAi.TTS.Integrations.TTSWit::get_DiskCacheHandler()
extern void TTSWit_get_DiskCacheHandler_mCAAFAA16FAC465A1F5027779F1B949CCED6E9B87 (void);
// 0x000000DF Facebook.WitAi.TTS.Events.TTSStreamEvents Facebook.WitAi.TTS.Integrations.TTSWit::get_WebStreamEvents()
extern void TTSWit_get_WebStreamEvents_m685D56BBE02FBDEFEAA2D783F86BD8FCD937EAC6 (void);
// 0x000000E0 System.Void Facebook.WitAi.TTS.Integrations.TTSWit::set_WebStreamEvents(Facebook.WitAi.TTS.Events.TTSStreamEvents)
extern void TTSWit_set_WebStreamEvents_mE560A154D99E9327D15BC61FE6D1F23918CB59FC (void);
// 0x000000E1 System.String Facebook.WitAi.TTS.Integrations.TTSWit::IsValid()
extern void TTSWit_IsValid_m110DF51345C5D00E691222A34BA51460B086FC06 (void);
// 0x000000E2 System.String Facebook.WitAi.TTS.Integrations.TTSWit::IsTextValid(System.String)
extern void TTSWit_IsTextValid_m552FDF15F6417F9CF5A8EFAADB3EBF9775C10BA9 (void);
// 0x000000E3 System.Void Facebook.WitAi.TTS.Integrations.TTSWit::RequestStreamFromWeb(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSWit_RequestStreamFromWeb_m7AA9EEA1F2EE4DDE1B47FBA80433EA2C9CC72E7D (void);
// 0x000000E4 System.Boolean Facebook.WitAi.TTS.Integrations.TTSWit::CancelWebStream(Facebook.WitAi.TTS.Data.TTSClipData)
extern void TTSWit_CancelWebStream_m02F93805C304D674D37971ED7FADEF11A648D9A7 (void);
// 0x000000E5 Facebook.WitAi.TTS.Events.TTSDownloadEvents Facebook.WitAi.TTS.Integrations.TTSWit::get_WebDownloadEvents()
extern void TTSWit_get_WebDownloadEvents_m0731373305D8E749FDEF1C457D9CDC180749DBD4 (void);
// 0x000000E6 System.Void Facebook.WitAi.TTS.Integrations.TTSWit::set_WebDownloadEvents(Facebook.WitAi.TTS.Events.TTSDownloadEvents)
extern void TTSWit_set_WebDownloadEvents_m734517551A589A9FAEBF0C40A884E003C99B8332 (void);
// 0x000000E7 System.Void Facebook.WitAi.TTS.Integrations.TTSWit::RequestDownloadFromWeb(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSWit_RequestDownloadFromWeb_m6004459FDCA40CC99E47FBBB7842E9C464E2FBE4 (void);
// 0x000000E8 System.Boolean Facebook.WitAi.TTS.Integrations.TTSWit::CancelWebDownload(Facebook.WitAi.TTS.Data.TTSClipData,System.String)
extern void TTSWit_CancelWebDownload_mE0969FA25D7C03B95898258314C1B95E20ECA9D1 (void);
// 0x000000E9 Facebook.WitAi.TTS.Integrations.TTSWitVoiceSettings[] Facebook.WitAi.TTS.Integrations.TTSWit::get_PresetWitVoiceSettings()
extern void TTSWit_get_PresetWitVoiceSettings_mCD3F6DD05CBC7E8E1285CED0EDDE3E59273C5263 (void);
// 0x000000EA Facebook.WitAi.TTS.Data.TTSVoiceSettings[] Facebook.WitAi.TTS.Integrations.TTSWit::get_PresetVoiceSettings()
extern void TTSWit_get_PresetVoiceSettings_m1AAFC53B98A94D44E4E0E74726994938DF73D867 (void);
// 0x000000EB Facebook.WitAi.TTS.Data.TTSVoiceSettings Facebook.WitAi.TTS.Integrations.TTSWit::get_VoiceDefaultSettings()
extern void TTSWit_get_VoiceDefaultSettings_mB2ECFE50C9BC4B89EDE76AC046D39490704352B1 (void);
// 0x000000EC System.Collections.Generic.Dictionary`2<System.String,System.String> Facebook.WitAi.TTS.Integrations.TTSWit::EncodeVoiceSettings(Facebook.WitAi.TTS.Data.TTSVoiceSettings)
extern void TTSWit_EncodeVoiceSettings_mEE5544B9E5B5210986014E641FAE3FF828430A3E (void);
// 0x000000ED System.String Facebook.WitAi.TTS.Integrations.TTSWit::IsRequestValid(Facebook.WitAi.TTS.Data.TTSClipData,Facebook.WitAi.Data.Configuration.WitConfiguration)
extern void TTSWit_IsRequestValid_m3FA936DBF067799773ED5B794C9455F479CD9198 (void);
// 0x000000EE System.Void Facebook.WitAi.TTS.Integrations.TTSWit::.ctor()
extern void TTSWit__ctor_mD407B22FA9765954625FD41A43C22E3750402043 (void);
// 0x000000EF System.Void Facebook.WitAi.TTS.Integrations.TTSWit/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mBC280B4D314F199CB114F96BEBA7D84561FA09EE (void);
// 0x000000F0 System.Void Facebook.WitAi.TTS.Integrations.TTSWit/<>c__DisplayClass18_0::<RequestStreamFromWeb>b__0(System.Single)
extern void U3CU3Ec__DisplayClass18_0_U3CRequestStreamFromWebU3Eb__0_m8A9093DD342F84BD3DE1F144A80B6471A2D0876A (void);
// 0x000000F1 System.Void Facebook.WitAi.TTS.Integrations.TTSWit/<>c__DisplayClass18_0::<RequestStreamFromWeb>b__1(UnityEngine.AudioClip,System.String)
extern void U3CU3Ec__DisplayClass18_0_U3CRequestStreamFromWebU3Eb__1_mC0907E0380462047052FEC9F3FDD14A845F7C505 (void);
// 0x000000F2 System.Void Facebook.WitAi.TTS.Integrations.TTSWit/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m61BD8F0095E1A015EECCAB768ADFF7F0BD2714BA (void);
// 0x000000F3 System.Void Facebook.WitAi.TTS.Integrations.TTSWit/<>c__DisplayClass25_0::<RequestDownloadFromWeb>b__0(System.Single)
extern void U3CU3Ec__DisplayClass25_0_U3CRequestDownloadFromWebU3Eb__0_m06086CA191F903D491BDE3021823896FD6AA0507 (void);
// 0x000000F4 System.Void Facebook.WitAi.TTS.Integrations.TTSWit/<>c__DisplayClass25_0::<RequestDownloadFromWeb>b__1(System.String)
extern void U3CU3Ec__DisplayClass25_0_U3CRequestDownloadFromWebU3Eb__1_m25CB686A5E9124EB5ED9BFC300A96A9D749B9CD6 (void);
// 0x000000F5 System.Void Facebook.WitAi.TTS.Events.TTSClipDownloadEvent::.ctor()
extern void TTSClipDownloadEvent__ctor_m0FF9C9C861D86F863440F59BEAEE31EE24EFA23A (void);
// 0x000000F6 System.Void Facebook.WitAi.TTS.Events.TTSClipDownloadErrorEvent::.ctor()
extern void TTSClipDownloadErrorEvent__ctor_mF308F9F521E9CC7D1EA7CDAD55065E6AE238B198 (void);
// 0x000000F7 System.Void Facebook.WitAi.TTS.Events.TTSDownloadEvents::.ctor()
extern void TTSDownloadEvents__ctor_mB383C095406B4D49E1231B5B71D4BF14D3D67523 (void);
// 0x000000F8 System.Void Facebook.WitAi.TTS.Events.TTSServiceEvents::.ctor()
extern void TTSServiceEvents__ctor_mF1F7E1215CFC576E08BE41B1C54F85F7E705C4E8 (void);
// 0x000000F9 System.Void Facebook.WitAi.TTS.Events.TTSClipEvent::.ctor()
extern void TTSClipEvent__ctor_m97D0F65C82A7A5E77A25472DB1DEADDA29958940 (void);
// 0x000000FA System.Void Facebook.WitAi.TTS.Events.TTSClipErrorEvent::.ctor()
extern void TTSClipErrorEvent__ctor_mC3046E7A60078D591FAECD0D4ACEF29F66BA4F2E (void);
// 0x000000FB System.Void Facebook.WitAi.TTS.Events.TTSStreamEvents::.ctor()
extern void TTSStreamEvents__ctor_m9CD2197E633D20FC9314BF7143B77907E55FF291 (void);
// 0x000000FC System.Void Facebook.WitAi.TTS.Data.TTSClipData::.ctor()
extern void TTSClipData__ctor_m6DC9BB9B8F6D73BA1A52A46124657FED3764A5DB (void);
// 0x000000FD System.Void Facebook.WitAi.TTS.Data.TTSDiskCacheSettings::.ctor()
extern void TTSDiskCacheSettings__ctor_mB9E32B2A88A37127C265FC0012E946EF22E9D83F (void);
// 0x000000FE System.Void Facebook.WitAi.TTS.Data.TTSVoiceSettings::.ctor()
extern void TTSVoiceSettings__ctor_m5BFC493C5C4BA07CA40D963B59E3BEBAE63BA8D0 (void);
static Il2CppMethodPointer s_methodPointers[254] = 
{
	TTSService_get_Instance_mFF25B835D15F14F05A55DABDDCA273C81AAD16B1,
	NULL,
	NULL,
	NULL,
	NULL,
	TTSService_IsValid_mCB28A08413A001CCC38AD6098C25666CD9FD5F04,
	TTSService_get_Events_m06F1D1284B100A3D0B4CBD95D3CA7CE64F1BCC81,
	TTSService_Awake_mCA6CBAB8A7E4C9605F84D2E5F05F2BD19C858E5F,
	TTSService_OnEnable_m303E34DE93E3BC544732BA8FB3BE6EF3B65FEC93,
	TTSService_OnDisable_m99847B634FC577734D55BFF83A1823B48319FA4D,
	TTSService_AddDelegates_mF7F3A2E37714EA73829F76E9EA6023ED9DE8DA04,
	TTSService_RemoveDelegates_mA9F83505A87A38ED1A6C78FF3E83B7A21BB0CBA0,
	TTSService_OnDestroy_mDE1865B45CBF08A52C3C3D8F270854ACA3C4E4F3,
	TTSService_LogClip_m49E7DFD2892430E83C9CC199FD861F53EEC50161,
	TTSService_Log_m2B43940259CF73FCA2AC4147BE06ACC71F7D1325,
	TTSService_GetClipID_m6707CAA0C811216B41A7F9BB0C592937540856A6,
	TTSService_GetSha256Hash_m87AB463FB0920DAAD4CF29D9CF02B5AD43FD5BAF,
	TTSService_CreateClipData_mBC31777A8AA35B1FF84222F80D11A7096A118F73,
	TTSService_SetClipLoadState_m6A22818651434B42865C1A00B8397423196E06F7,
	TTSService_Load_m0B82325D8FAC910AEC705C602362F42432BBD9E3,
	TTSService_Load_mDC143748A29EACDF490D0D8AACD467DB4DB32F0C,
	TTSService_Load_mB2ADF9F4811B036A8A2CF31F6B92DD302649A1A2,
	TTSService_Load_m7238951240C68523A2FCCCE6E673B57416EB18AE,
	TTSService_Load_m7A6268734204791B7466AFC5A9F566FF8557811F,
	TTSService_CallAfterAMoment_mC5A015778B9EFBDAA46F6932CAB9C2DA70F5B7F6,
	TTSService_OnLoadBegin_m30DEA102388D8683CBDA0EA5944989A3D068976D,
	TTSService_OnDiskStreamBegin_m8FD56D611C4A327BCCA2576AC2F6362BC76B4728,
	TTSService_OnWebStreamBegin_mC499477878F00B8E4BADDC83892F28B311BB63FB,
	TTSService_OnStreamBegin_mDDD197372F8311D0BF9E9CC6C961FB4CD28972E5,
	TTSService_OnDiskStreamReady_m4BBB310C572C3061E90CBC11D9F30A7B41D6398F,
	TTSService_OnWebStreamReady_m5B74839229E7106A49A6F69C2ABA9548CFE819E3,
	TTSService_OnStreamReady_m5AF2064DF672361B7B63EA5394A783F2AB5AD751,
	TTSService_OnDiskStreamCancel_mDBF70537B475AEC80A9C07BE7BD6125C357A8A07,
	TTSService_OnWebStreamCancel_m4E77C61A8E8F19460CC8929506D7D68B8D68A91C,
	TTSService_OnStreamCancel_m4812ABEC439B80CA4D7E829A59583DEECDDE071E,
	TTSService_OnDiskStreamError_m224C948AB1CAAB8DB6C32C8FB09709A30C0357F0,
	TTSService_OnWebStreamError_m50F4199E983FB36D9493AC865D30985A6F8D9AFE,
	TTSService_OnStreamError_mFFCC21254211A971E119518446BBA2435CB7E2AC,
	TTSService_UnloadAll_mDA8015DE04D69DC150D07B4B930683D12D52C3D9,
	TTSService_Unload_mD31B7917459D317584984D983966F5B3A520DA5C,
	TTSService_OnUnloadBegin_m4CB978ABFBB236665F1D2C3FA0B19A4FDB850D23,
	TTSService_GetRuntimeCachedClip_mB10D0AC1EDA8C132BEDA1B39F9DB8C6CB230ED4B,
	TTSService_GetAllRuntimeCachedClips_m9905899547D3621A4C9AB8C812F43B988B24B7E2,
	TTSService_OnRuntimeClipAdded_m9770E74F7E5E67E1A6F49BD74DF3F4E353380E14,
	TTSService_OnRuntimeClipRemoved_m5BD6FD06EBBED3746F08C08B9E580FDCA5370CDD,
	TTSService_ShouldCacheToDisk_m9A5100F671847D4C41226D9FE0ADCAAC517AD43D,
	TTSService_GetDiskCachePath_mF8B642C7534848070A1A2342D6686D594115D9E0,
	TTSService_DownloadToDiskCache_m16BDCFDA5D4C5A4397567F771B97AD3A8D299A70,
	TTSService_DownloadToDiskCache_m867C5502F21D5F42D926343AE03FEC2B79DB7080,
	TTSService_DownloadToDiskCache_mEA9B629448D0D52296C166E93391B8AD989A9B77,
	TTSService_DownloadToDiskCache_mE574FE9787ECE69C8486AB1CA278A1DD8F3E5CB8,
	TTSService_DownloadToDiskCache_m409E83A72C3FC9C7BEE35569D7EA1A863C916496,
	TTSService_DownloadToDiskCache_m976123174CAABE1D3825D6FBB1131652FF0B4BD7,
	TTSService_OnWebDownloadBegin_m51DF3010BB349B03FEAD91DE4C9F7109289B2B57,
	TTSService_OnWebDownloadSuccess_mF070E2AA86FE8E4320BFD5FAEAF54078B46CF605,
	TTSService_OnWebDownloadCancel_m6931C3695EFBA8A729786E3019602C1334B6DD14,
	TTSService_OnWebDownloadError_mE133A65B687CA7E5DCF695A5249AF1AB0971AC9A,
	TTSService_GetAllPresetVoiceSettings_mDA814D5DA0824C02AAF76486E2E485606211E16A,
	TTSService_GetPresetVoiceSettings_mB4B6468F6CED75C7E62AD33A6A6BACE788937880,
	TTSService__ctor_mD1388977DC2D00CFEFBE3879E43E5FC48A04C178,
	U3CU3Ec__cctor_m1F50EAE30DC688E644DE334AD4284A140C825F56,
	U3CU3Ec__ctor_m52305550C25D7AE01F4D45D8D53E2ADD3C35B028,
	U3CU3Ec_U3Cget_InstanceU3Eb__1_0_mE8FBDC870108A4AA2F93AC1AEAD86A2714D6E1BD,
	U3CU3Ec__DisplayClass36_0__ctor_m4A14532CB34A98EAA3ED24F80238369A689D426F,
	U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__2_mB529AFD4F19D81C0EE4B6AFAB8C96546B330AA46,
	U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__0_m4CF30280CA7D1D86D7FAF263E2960AFAC40B1A19,
	U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__3_m9B7E2E07E49AE52F612B1EEAC8C5585B327CAC37,
	U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__1_mF702814DD580D25CA27A2A2496267C48E57CE2D0,
	U3CU3Ec__DisplayClass36_0_U3CLoadU3Eb__4_m4459DFB80164C2288FCC04BCEB7D021520CD5D69,
	U3CCallAfterAMomentU3Ed__37__ctor_m37777D07ACA2996D4A4E7BE126F4EB2A4A191D34,
	U3CCallAfterAMomentU3Ed__37_System_IDisposable_Dispose_m72FB1A36C609A7F7825CD558A775650B2EE81D4F,
	U3CCallAfterAMomentU3Ed__37_MoveNext_mECE2F604E8E761334DE92B586A36C19F285AF07F,
	U3CCallAfterAMomentU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F19C5C2C93E68EF06097AC36049C9A7EFEB9966,
	U3CCallAfterAMomentU3Ed__37_System_Collections_IEnumerator_Reset_m11D596EBE43F72BE456EA459C938348A9B3C4F95,
	U3CCallAfterAMomentU3Ed__37_System_Collections_IEnumerator_get_Current_m225AF082AE7459DBE9EB574827E6043B94CD2F3E,
	U3CU3Ec__DisplayClass65_0__ctor_m4F3314CFEE3914DF79682DCC56E275DA16F025F8,
	U3CU3Ec__DisplayClass65_0_U3CDownloadToDiskCacheU3Eb__0_mB7A1CF9E4DCEBB9907BA8FDCD7D95BFBBF53FC73,
	U3CU3Ec__DisplayClass71_0__ctor_m0314885970A29FB27439C4A3510D8CC235E7E01C,
	U3CU3Ec__DisplayClass71_0_U3CGetPresetVoiceSettingsU3Eb__0_mF34D72B8BC07D929DFDD88C8EDBAFF8ABFECC982,
	TTSSpeakerEvent__ctor_m91F1A08BD8CFDF029E7411BCADC437852DCF3296,
	TTSSpeakerEvents__ctor_m8B8134DF7FD6D64CBA3FA78AFFAED0EEBDDA9C85,
	TTSSpeaker_get_VoiceSettings_mF88416F754DD9F11FD0CEAA20B5B15E3F02054BB,
	TTSSpeaker_get_Events_m583EAB517C1DD194A0B2AA54BC4087D21E27D920,
	TTSSpeaker_Awake_mABE45A26A4A2B217FDBC3CE5A792C8AC213C37D5,
	TTSSpeaker_OnDestroy_m70344F01EB42CF636BE91FC57C7DD566AD0F3915,
	TTSSpeaker_OnClipUnload_mD1270333BED971CF92AC67A4CDE6892CBAF137C4,
	TTSSpeaker_IsClipSame_m2AA68F06E86FC6981F28531B30341362C17F3F67,
	TTSSpeaker_get_IsSpeaking_m985AC30CAC623382DCE67EE529979F5CDDB6D903,
	TTSSpeaker_Speak_mBB84CF1C9F67F53ABF505BDC745ECF83288A0BCE,
	TTSSpeaker_Speak_m7105B1F4C4D2BD4DA9A6FC2579BDA95662C0EE2C,
	TTSSpeaker_Speak_m6916A94C1DF27FF23C7F0F0AF866D2AE9DFEE3E9,
	TTSSpeaker_Stop_mAE3291B82D82E0F0AC5E034A3152C5AF41DBA8FE,
	TTSSpeaker_get_IsLoading_mA08DA3689E8137CF0BFA4F64155F27CDE488D526,
	TTSSpeaker_OnLoadBegin_m08C6688B2B01CA97319044058E20D58705E17D3E,
	TTSSpeaker_OnLoadComplete_m48D044FEA678A7884B64C0B24271271BA95C4A33,
	TTSSpeaker_OnLoadAbort_m598127914818098D441BEEC6BA6FD3BEE1B399FC,
	TTSSpeaker_OnPlaybackBegin_m310AEC19BCF11E94DFCE2068C24DBF94D6731A95,
	TTSSpeaker_OnPlaybackWait_mD7EA1E586D2C1BF68655B2F8D65F70CA96E26754,
	TTSSpeaker_OnPlaybackComplete_m914AB1CF8323823A6F230985C432C1A14943A8C4,
	TTSSpeaker_OnPlaybackCancel_mF495D1E33974E1D092DD6A7404D65864C12FB7BF,
	TTSSpeaker__ctor_mFFCAF4AE3B4340275D39C6EDFA67A69D826D9E89,
	U3COnPlaybackWaitU3Ed__27__ctor_mA6F7D3F3D6CDD566D3805B8010D4F429E2C79F41,
	U3COnPlaybackWaitU3Ed__27_System_IDisposable_Dispose_mFC9D7965A33B1A24EBD5363D2AD2A68A749C82C5,
	U3COnPlaybackWaitU3Ed__27_MoveNext_mB58A7ECC7372DD7FBAC28E89B858BD3377BB36CA,
	U3COnPlaybackWaitU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAF3ABA74E1BB23E06E23B9AC3D32B56C0C57D947,
	U3COnPlaybackWaitU3Ed__27_System_Collections_IEnumerator_Reset_m5EADB8420EA6DB8952D80F3BE2EC1FC91C4D96A4,
	U3COnPlaybackWaitU3Ed__27_System_Collections_IEnumerator_get_Current_m57682E8213AF013E824849D8548608D9B608AD44,
	VoiceUnityRequest_RequestAudioClip_m8DCA4C5E6230A872E2E4068FD96E1B68316186A3,
	VoiceUnityRequest_RequestAudioClip_m613AAEE7AA46AB40615549A82BE2ABA081387785,
	VoiceUnityRequest_RequestAudioClip_m92A838E7A81D3973785416FDDF95C7C295418E1A,
	VoiceUnityRequest_RequestAudioClip_m7673C148E75F6964C41165E5252B1E066ABB7561,
	VoiceUnityRequest_RequestFile_m4BA24586EA0AF5FE9A1F3ECF9B09E47F587462B0,
	VoiceUnityRequest_RequestFile_m194F6996C286AE8CF71C30D5B1AA9A5D43E14AF7,
	VoiceUnityRequest_Request_m14FA8140411D3FC641D27601F6DB46CD2C3EB137,
	VoiceUnityRequest_Request_m13FD11371E047AD2238370D5D552B51B876A2C4C,
	VoiceUnityRequest_get_IsTransmitting_m3459E362B29EC8C0F86E95D83AAE0CD0D4368DC4,
	VoiceUnityRequest_get_Progress_m817D73C8CCE755D13E1AEC3D13DD03F48678C4A8,
	VoiceUnityRequest_Setup_m432C9422D7063C3329370B06B5D247C417E123B8,
	VoiceUnityRequest_PerformUpdate_mB458EB05238436A59D00324232D2C6383AC0049D,
	VoiceUnityRequest_Begin_mC3C14BFD664CE3D7B991B16B24BC1D52FAC98E69,
	VoiceUnityRequest_Complete_mDBC751DC6A0CD36274ED0B025BA264C9B271D5A0,
	VoiceUnityRequest_Unload_m9D48336082E16B65322763BF76874A5743C6CB65,
	VoiceUnityRequest__ctor_mDA87B87E9C02DFFD969870F803D7B9AF7EF0A603,
	VoiceUnityRequest__cctor_m720BB52EDDA6FAABF1C86C0E7F2D1A1D06BA56B7,
	U3CU3Ec__DisplayClass3_0__ctor_m4E2895C1EBAF4745BCAD79C5AA0F2A8C6B5D1652,
	U3CU3Ec__DisplayClass3_0_U3CRequestAudioClipU3Eb__0_mC3F732AE56425726A710788B02AA7E307D2FE70F,
	U3CU3Ec__DisplayClass3_0_U3CRequestAudioClipU3Eb__1_m48E6D14096C1E116D332F15B6A7464B01627B8A5,
	U3CU3Ec__DisplayClass5_0__ctor_mC47301F0ACDC7E8F330BE88D139F3CCD7E010C52,
	U3CU3Ec__DisplayClass5_0_U3CRequestFileU3Eb__0_m73E5A1AC02EBD21B61D2E1A39DC250DF21A12BAE,
	U3CU3Ec__DisplayClass5_0_U3CRequestFileU3Eb__1_m7BDC6993EB33E1A6F51A346CDEB74835786F226B,
	U3CPerformUpdateU3Ed__22__ctor_m1463A408184EC083E98C0FC4236CE9202101AFFA,
	U3CPerformUpdateU3Ed__22_System_IDisposable_Dispose_m87302484AFF457DFE1FE15C10982716826136235,
	U3CPerformUpdateU3Ed__22_MoveNext_m45A25A11DF59B6DEED9114B1D3607A06765FFEC5,
	U3CPerformUpdateU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4167B21B2B70ABD2C137EE40549B13BF9DBF185A,
	U3CPerformUpdateU3Ed__22_System_Collections_IEnumerator_Reset_m12EA9A30426BF3DE3470F4580B54E8368DF6D8F8,
	U3CPerformUpdateU3Ed__22_System_Collections_IEnumerator_get_Current_m33C70FFDB7CD4A2483449AE0819F2566D09B2A72,
	WitUnityRequest_IsTextValid_m33DF9C50AF44B1025D2A1BAEC384889A0488CF35,
	WitUnityRequest_RequestTTSStream_m1E3F6346FF7665A72833944BAC09ACA59F6D43C4,
	WitUnityRequest_RequestTTSDownload_m5E8292F4B56A623242E8D3D7A0B35D9F8D1EB6CF,
	WitUnityRequest_RequestTTS_m905686D3CB7B73D11029ECFE22B2E0736662E765,
	WitUnityRequest_RequestTTSVoices_m43E737F242DBD0711540E1D0985C812CEC322E34,
	WitUnityRequest_add_OnProvideCustomUri_mEF29CDB91F1DDA5DC69BB689DDA28E50925002A2,
	WitUnityRequest_remove_OnProvideCustomUri_m84F4757D227D02AEA6EEF84B275C198B9DEC041F,
	WitUnityRequest_add_OnProvideCustomHeaders_m2AA5C0F16B8DDD8B8A11CAB7317EFAEB275A2C1A,
	WitUnityRequest_remove_OnProvideCustomHeaders_m894949D05F2CE99E3BDFC2EA6965A2C70C27D0F2,
	WitUnityRequest_GetUri_mE223EB4C4C1DFF896D4EC447DBFD32E03BED9180,
	WitUnityRequest_ApplyHeaders_m66A3CD0A060491A1B1199E74FC727E8D36EA9539,
	WitUnityRequest_GetAuthorization_mD94836A0E01AAB8C6FBD3F120EBF0EF5505E52A8,
	WitUnityRequest_RequestWit_mE3D28982EDDF840FDF3B3ED41068831F4D29BFC8,
	WitUnityRequest__ctor_m846947D250A727B50EFB1554F3FD16E10DCAA9CE,
	WitUnityRequest__cctor_mA342096E18D6C2FECC39DD1642E662636E9CDE57,
	U3CU3Ec__DisplayClass4_0__ctor_m1FA2008BC9ADAFB60568CFBB421FF5C8FA05BF11,
	U3CU3Ec__DisplayClass4_0_U3CRequestTTSStreamU3Eb__1_mAB685831368A054FA8E58FAE02A631C6097DED0B,
	U3CU3Ec__cctor_mE50E997F9A1B5C756360C2A8AA560F9FFC37CCFB,
	U3CU3Ec__ctor_m6D7160312322CB80311B3E4CF0C1450FFE310C44,
	U3CU3Ec_U3CRequestTTSStreamU3Eb__4_0_mDEEB0E1EFBA1E4AF999C3D875397CA2F69112816,
	U3CU3Ec__DisplayClass5_0__ctor_m29E0695E357DABC58B786182070B058C6CEDE0D9,
	U3CU3Ec__DisplayClass5_0_U3CRequestTTSDownloadU3Eb__0_m5E393456DA4968BFE99E55D000BF9F05579899B7,
	U3CU3Ec__DisplayClass5_0_U3CRequestTTSDownloadU3Eb__1_mE5296949D0DB166ABF0125D21CD4EFE033AF8C42,
	U3CU3Ec__DisplayClass6_0__ctor_mADE1745BE4A2B61081CB495CCDDAB8282318F8A2,
	U3CU3Ec__DisplayClass6_0_U3CRequestTTSU3Eb__0_m54DAEB3A49FBF231F3AA154FAEDA70A445A3FA94,
	U3CU3Ec__DisplayClass8_0__ctor_mF2B650E7BEBE7A40F3F9FCD956977083A79E85DB,
	U3CU3Ec__DisplayClass8_0_U3CRequestTTSVoicesU3Eb__0_mF907D203DE92EB45D593C696D2D78C10FF48357A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TTSDiskCache_get_DiskPath_m9C4400B751977975C2B5EAF0CEFE051BDD379A4E,
	TTSDiskCache_get_DiskCacheDefaultSettings_m3034BA67F5BFB8A7E299A6362ED1595D5F386A0B,
	TTSDiskCache_get_DiskStreamEvents_mE71C389875BD8935ACE37608675C06C94A4F8F44,
	TTSDiskCache_set_DiskStreamEvents_m47AC9C092D037AD0581C7B10D779E9CB1CBC35F3,
	TTSDiskCache_GetDiskCachePath_m2C3F22163D49C272A1B17FD8F1E931D452BA881F,
	TTSDiskCache_ShouldCacheToDisk_mFD87A13D48BA79EBAC073C0C4FB30754C2A51698,
	TTSDiskCache_IsCachedToDisk_mB7B83E14F04AD85686D4394FBEE5CAC1941F9AFB,
	TTSDiskCache_StreamFromDiskCache_m5FDBDFB3C1B78AFB0E3059649538F0D1A32699BD,
	TTSDiskCache_CancelDiskCacheStream_m2A49D1062FD79617717A0E123FAF963F13750EB4,
	TTSDiskCache_OnStreamComplete_m78386E49C23F45D748CF3B0B59E30600344D665A,
	TTSDiskCache__ctor_mA82FB16B2B84DE93989222FA45923BDC72AC1F6B,
	U3CU3Ec__DisplayClass14_0__ctor_mDED57EC8D6923E0B32C9AE390B86109071A75A45,
	U3CU3Ec__DisplayClass14_0_U3CStreamFromDiskCacheU3Eb__0_m04394DDD8B89261395395CB77C418FAA4B5FC86B,
	U3CU3Ec__DisplayClass14_0_U3CStreamFromDiskCacheU3Eb__1_m5D033B5BC8E803223797D4EAADEDB10DEF3D9D3F,
	TTSRuntimeCache_get_OnClipAdded_m7AA3587047D184609CAFA096802209B676784B40,
	TTSRuntimeCache_set_OnClipAdded_m10E837E5B7E270462879D43E412043E646E21B94,
	TTSRuntimeCache_get_OnClipRemoved_m33EFB809EC5749E7202343D42380BEA2B615596C,
	TTSRuntimeCache_set_OnClipRemoved_m3DE1A59BDD3E520BD5E09957A828B7EC25B0C6D1,
	TTSRuntimeCache_GetClips_mA8067C4BC4AC988EB79AEF88EF64F3CB03705E14,
	TTSRuntimeCache_GetClip_m31C8155AB897C4EB633B204FF79D4DBBFB50FFBC,
	TTSRuntimeCache_AddClip_m8F231C0C4A6351459D386AD7C08DECACCBD5AFA0,
	TTSRuntimeCache_RemoveClip_m65B9C104281646C3E258B89C3976099C78CA9984,
	TTSRuntimeCache_IsCacheFull_mE5F1943B9BF6D45CBBF6CA3883BA7FB89691422F,
	TTSRuntimeCache_GetCacheDiskSize_m527CEC70E430910EA93F0A02395DC61DB658CEBB,
	TTSRuntimeCache_GetClipBytes_m73681FFE79FC8F8C86914CA5AB11CABD94FE9AB5,
	TTSRuntimeCache__ctor_m60ADC850E585A92CB44B2FDE9BC2BE6EE4EBF2A2,
	TTSWitVoiceSettings__ctor_m35D5DD2388877CFED6571271F44DD351D27CEA74,
	TTSWit_get_VoiceProvider_m4BDDDBF8F69A5AD3C1B922E4E61FDE7DE3139773,
	TTSWit_get_WebHandler_mED5B3C139BE568EDAD0C0955A48AE86E4DE8889F,
	TTSWit_get_RuntimeCacheHandler_mA73622A3193507273BB8B38751622B81BAD99431,
	TTSWit_get_DiskCacheHandler_mCAAFAA16FAC465A1F5027779F1B949CCED6E9B87,
	TTSWit_get_WebStreamEvents_m685D56BBE02FBDEFEAA2D783F86BD8FCD937EAC6,
	TTSWit_set_WebStreamEvents_mE560A154D99E9327D15BC61FE6D1F23918CB59FC,
	TTSWit_IsValid_m110DF51345C5D00E691222A34BA51460B086FC06,
	TTSWit_IsTextValid_m552FDF15F6417F9CF5A8EFAADB3EBF9775C10BA9,
	TTSWit_RequestStreamFromWeb_m7AA9EEA1F2EE4DDE1B47FBA80433EA2C9CC72E7D,
	TTSWit_CancelWebStream_m02F93805C304D674D37971ED7FADEF11A648D9A7,
	TTSWit_get_WebDownloadEvents_m0731373305D8E749FDEF1C457D9CDC180749DBD4,
	TTSWit_set_WebDownloadEvents_m734517551A589A9FAEBF0C40A884E003C99B8332,
	TTSWit_RequestDownloadFromWeb_m6004459FDCA40CC99E47FBBB7842E9C464E2FBE4,
	TTSWit_CancelWebDownload_mE0969FA25D7C03B95898258314C1B95E20ECA9D1,
	TTSWit_get_PresetWitVoiceSettings_mCD3F6DD05CBC7E8E1285CED0EDDE3E59273C5263,
	TTSWit_get_PresetVoiceSettings_m1AAFC53B98A94D44E4E0E74726994938DF73D867,
	TTSWit_get_VoiceDefaultSettings_mB2ECFE50C9BC4B89EDE76AC046D39490704352B1,
	TTSWit_EncodeVoiceSettings_mEE5544B9E5B5210986014E641FAE3FF828430A3E,
	TTSWit_IsRequestValid_m3FA936DBF067799773ED5B794C9455F479CD9198,
	TTSWit__ctor_mD407B22FA9765954625FD41A43C22E3750402043,
	U3CU3Ec__DisplayClass18_0__ctor_mBC280B4D314F199CB114F96BEBA7D84561FA09EE,
	U3CU3Ec__DisplayClass18_0_U3CRequestStreamFromWebU3Eb__0_m8A9093DD342F84BD3DE1F144A80B6471A2D0876A,
	U3CU3Ec__DisplayClass18_0_U3CRequestStreamFromWebU3Eb__1_mC0907E0380462047052FEC9F3FDD14A845F7C505,
	U3CU3Ec__DisplayClass25_0__ctor_m61BD8F0095E1A015EECCAB768ADFF7F0BD2714BA,
	U3CU3Ec__DisplayClass25_0_U3CRequestDownloadFromWebU3Eb__0_m06086CA191F903D491BDE3021823896FD6AA0507,
	U3CU3Ec__DisplayClass25_0_U3CRequestDownloadFromWebU3Eb__1_m25CB686A5E9124EB5ED9BFC300A96A9D749B9CD6,
	TTSClipDownloadEvent__ctor_m0FF9C9C861D86F863440F59BEAEE31EE24EFA23A,
	TTSClipDownloadErrorEvent__ctor_mF308F9F521E9CC7D1EA7CDAD55065E6AE238B198,
	TTSDownloadEvents__ctor_mB383C095406B4D49E1231B5B71D4BF14D3D67523,
	TTSServiceEvents__ctor_mF1F7E1215CFC576E08BE41B1C54F85F7E705C4E8,
	TTSClipEvent__ctor_m97D0F65C82A7A5E77A25472DB1DEADDA29958940,
	TTSClipErrorEvent__ctor_mC3046E7A60078D591FAECD0D4ACEF29F66BA4F2E,
	TTSStreamEvents__ctor_m9CD2197E633D20FC9314BF7143B77907E55FF291,
	TTSClipData__ctor_m6DC9BB9B8F6D73BA1A52A46124657FED3764A5DB,
	TTSDiskCacheSettings__ctor_mB9E32B2A88A37127C265FC0012E946EF22E9D83F,
	TTSVoiceSettings__ctor_m5BFC493C5C4BA07CA40D963B59E3BEBAE63BA8D0,
};
static const int32_t s_InvokerIndices[254] = 
{
	5654,
	3645,
	3645,
	3645,
	3645,
	3645,
	3645,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	1203,
	1860,
	1499,
	1499,
	636,
	1860,
	1499,
	1038,
	636,
	636,
	333,
	2764,
	3049,
	3049,
	3049,
	1853,
	3049,
	3049,
	1853,
	3049,
	3049,
	1853,
	1864,
	1864,
	1202,
	3709,
	3049,
	3049,
	2764,
	3645,
	3049,
	3049,
	2275,
	636,
	1499,
	1038,
	636,
	636,
	333,
	1864,
	1864,
	1864,
	1864,
	1204,
	3645,
	2764,
	3709,
	5676,
	3709,
	2275,
	3709,
	3049,
	3709,
	3049,
	3709,
	1204,
	3027,
	3709,
	3583,
	3645,
	3709,
	3645,
	3709,
	3049,
	3709,
	2275,
	3709,
	3709,
	3645,
	3645,
	3709,
	3709,
	3049,
	1308,
	3583,
	1864,
	3049,
	1864,
	3709,
	3583,
	821,
	1864,
	3709,
	3049,
	3645,
	3709,
	3709,
	3709,
	3027,
	3709,
	3583,
	3645,
	3709,
	3645,
	5048,
	4618,
	4612,
	4334,
	5048,
	4618,
	5048,
	4618,
	3583,
	3677,
	1204,
	3645,
	3709,
	3709,
	3709,
	3709,
	5676,
	3709,
	3079,
	3049,
	3709,
	3079,
	3049,
	3027,
	3709,
	3583,
	3645,
	3709,
	3645,
	5445,
	4079,
	3936,
	3936,
	4618,
	5583,
	5583,
	5583,
	5583,
	4618,
	5215,
	5445,
	4342,
	3709,
	5676,
	3709,
	1864,
	5676,
	3709,
	1864,
	3709,
	1864,
	1864,
	3709,
	3049,
	3709,
	3049,
	3645,
	3049,
	3645,
	2764,
	2275,
	2275,
	3049,
	3049,
	3645,
	3049,
	3645,
	3049,
	3645,
	2764,
	3049,
	3049,
	3645,
	3645,
	2764,
	3645,
	3049,
	2764,
	3049,
	2275,
	3645,
	3049,
	1864,
	1308,
	3645,
	3645,
	3645,
	3049,
	2764,
	2275,
	2275,
	3049,
	3049,
	1864,
	3709,
	3709,
	1870,
	1204,
	3645,
	3049,
	3645,
	3049,
	3645,
	2764,
	3049,
	3049,
	3583,
	3624,
	5390,
	3709,
	3709,
	3645,
	3645,
	3645,
	3645,
	3645,
	3049,
	3645,
	2764,
	3049,
	2275,
	3645,
	3049,
	1864,
	1308,
	3645,
	3645,
	3645,
	2764,
	1499,
	3709,
	3709,
	3079,
	1864,
	3709,
	3079,
	3049,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
	3709,
};
extern const CustomAttributesCacheGenerator g_Facebook_Wit_TTS_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Facebook_Wit_TTS_CodeGenModule;
const Il2CppCodeGenModule g_Facebook_Wit_TTS_CodeGenModule = 
{
	"Facebook.Wit.TTS.dll",
	254,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Facebook_Wit_TTS_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
