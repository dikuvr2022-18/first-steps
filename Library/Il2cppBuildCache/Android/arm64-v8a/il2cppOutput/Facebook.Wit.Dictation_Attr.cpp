﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// Facebook.WitAi.Events.EventCategoryAttribute
struct EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// Facebook.WitAi.Events.EventCategoryAttribute
struct EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String Facebook.WitAi.Events.EventCategoryAttribute::Category
	String_t* ___Category_0;

public:
	inline static int32_t get_offset_of_Category_0() { return static_cast<int32_t>(offsetof(EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936, ___Category_0)); }
	inline String_t* get_Category_0() const { return ___Category_0; }
	inline String_t** get_address_of_Category_0() { return &___Category_0; }
	inline void set_Category_0(String_t* value)
	{
		___Category_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Category_0), (void*)value);
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void Facebook.WitAi.Events.EventCategoryAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * __this, String_t* ___category0, const RuntimeMethod* method);
static void Facebook_Wit_Dictation_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_CustomAttributesCacheGenerator_dictationService(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869_CustomAttributesCacheGenerator__dictationServiceReference(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_CustomAttributesCacheGenerator_dictationEvents(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x66\x69\x72\x65\x20\x62\x65\x66\x6F\x72\x65\x2C\x20\x64\x75\x72\x69\x6E\x67\x20\x61\x6E\x64\x20\x61\x66\x74\x65\x72\x20\x61\x6E\x20\x61\x63\x74\x69\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_witDictation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_linesBetweenActivations(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_activationSeparator(CustomAttributesCache* cache)
{
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[0];
		MultilineAttribute__ctor_m353DC377C95D4C341F5DDD0F9894878F64E5703E(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_onTranscriptionUpdated(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_CustomAttributesCacheGenerator_witRuntimeConfiguration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onPartialTranscription(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x73\x73\x61\x67\x65\x20\x66\x69\x72\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x70\x61\x72\x74\x69\x61\x6C\x20\x74\x72\x61\x6E\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x72\x65\x63\x65\x69\x76\x65\x64\x2E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x50\x61\x72\x74\x69\x61\x6C\x54\x72\x61\x6E\x73\x63\x72\x69\x70\x74\x69\x6F\x6E"), NULL);
	}
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[2];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onFullTranscription(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x73\x73\x61\x67\x65\x20\x72\x65\x63\x65\x69\x76\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x20\x74\x72\x61\x6E\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x69\x73\x20\x72\x65\x63\x65\x69\x76\x65\x64\x2E"), NULL);
	}
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[1];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x6E\x46\x75\x6C\x6C\x54\x72\x61\x6E\x73\x63\x72\x69\x70\x74\x69\x6F\x6E"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onResponse(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x20\x52\x65\x73\x75\x6C\x74\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6C\x6C\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x72\x65\x73\x70\x6F\x6E\x73\x65\x20\x66\x72\x6F\x6D\x20\x57\x69\x74\x2E\x61\x69\x20\x68\x61\x73\x20\x62\x65\x65\x6E\x20\x72\x65\x63\x65\x69\x76\x65\x64"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onStart(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x20\x52\x65\x73\x75\x6C\x74\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onStopped(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x20\x52\x65\x73\x75\x6C\x74\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onError(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x61\x74\x69\x6F\x6E\x20\x52\x65\x73\x75\x6C\x74\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onDictationSessionStarted(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x63\x74\x61\x74\x69\x6F\x6E\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onDictationSessionStopped(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x63\x74\x61\x74\x69\x6F\x6E\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
static void DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onMicAudioLevel(CustomAttributesCache* cache)
{
	{
		EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 * tmp = (EventCategoryAttribute_t1AAD9816220F12C7C6D616321ACA7671EF49E936 *)cache->attributes[0];
		EventCategoryAttribute__ctor_mBC6F22A009300C9AE040D13F6A57BC0675405DDB(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x20\x45\x76\x65\x6E\x74\x73"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_Facebook_Wit_Dictation_AttributeGenerators[];
const CustomAttributesCacheGenerator g_Facebook_Wit_Dictation_AttributeGenerators[19] = 
{
	U3CU3Ec_tB668374A5908A9690B7A384BFE4E68D58279A95B_CustomAttributesCacheGenerator,
	DictationServiceReference_t06E9490C8746C8A76D49D21D78BC1820D03CE411_CustomAttributesCacheGenerator_dictationService,
	DictationServiceAudioEventReference_tA40654924BA0BAD672E7E12C485465FEC5EE8869_CustomAttributesCacheGenerator__dictationServiceReference,
	DictationService_t39963C8498DC956FAD7FEFA48B21F34500E6C8BA_CustomAttributesCacheGenerator_dictationEvents,
	MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_witDictation,
	MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_linesBetweenActivations,
	MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_activationSeparator,
	MultiRequestTranscription_t5A89136E04C3247963C1751CD07C99F68772E293_CustomAttributesCacheGenerator_onTranscriptionUpdated,
	WitDictation_t91C055CDDDE452A01FF25B74ACEA85DA2F35D866_CustomAttributesCacheGenerator_witRuntimeConfiguration,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onPartialTranscription,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onFullTranscription,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onResponse,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onStart,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onStopped,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onError,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onDictationSessionStarted,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onDictationSessionStopped,
	DictationEvents_tBE2C75F69FB8D9F1616FA94F6EEB5AD5C4202C7C_CustomAttributesCacheGenerator_onMicAudioLevel,
	Facebook_Wit_Dictation_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
