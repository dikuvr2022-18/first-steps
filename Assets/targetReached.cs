using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class targetReached : MonoBehaviour
{
    Rigidbody _rb;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void OnTriggerEnter(Collider collision) {
        if (collision.gameObject.name == "TargetCube1") {
            //Destroy(collision.gameObject);
            collision.gameObject.SetActive(false);
        }
        if (collision.gameObject.name == "TargetCube2") {
            collision.gameObject.SetActive(false);
        }
        if (collision.gameObject.name == "TargetCube3") {
            collision.gameObject.SetActive(false);
        }
    }
    
}
