using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Punch : MonoBehaviour
{
    public float forceMultiplier = 1.0f; // this can be changed in the inspector

    private void OnTriggerEnter(Collider other)
    {
        // Check if the other collider is the hand
        if (other.gameObject.tag == "Hand")
        {
           // Get the hand's transform
            Transform handTransform = other.gameObject.transform;

            // Use the forward direction of the hand's transform as the direction for the force
            Vector3 forceDirection = handTransform.forward;

            // Apply the force to the rigidbody
            GetComponent<Rigidbody>().AddForce(forceDirection * forceMultiplier, ForceMode.Impulse);
        }
    }
}