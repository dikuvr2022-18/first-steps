using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeVersion : MonoBehaviour
{
    public float moveDistance = 0.5f; // the distance to move the object when a button is pressed

    // Update is called once per frame
    void Update()
    {
        // check if the X button is pressed
        if (OVRInput.Get(OVRInput.Button.Three))
        {
            // move the object backwards
            transform.position -= transform.up * moveDistance;
        }

        // check if the Y button is pressed
        if (OVRInput.Get(OVRInput.Button.Four)) 
        {
            // move the object forwards
            transform.position += transform.up * moveDistance;
        }
    }
}
