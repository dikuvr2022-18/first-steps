using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager Instance { get; private set; } // singleton instance of the ScoreManager script
    public int score = 0; // current score
    public TextMeshPro scoreText; // reference to the TextMeshPro object for displaying the score

    // Start is called before the first frame update
    void Start()
    {
        // set the initial score text
        scoreText.text = "Score: " + score;
    }

    // Increase the score by the specified amount
    public void IncreaseScore(int amount)
    {
        score += amount;
        scoreText.text = "Score: " + score;
    }
}
