using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TargetScore : MonoBehaviour
{
    public static ScoreManager Instance { get; private set; } // singleton instance of the ScoreManager script
    public int score = 0; // current score
    public TextMeshPro scoreText; // reference to the TextMeshPro object for displaying the score

    void Start()
    {
        // set the initial score text
        scoreText.text = "Score: " + score;
    }
    // Increase the score by the specified amount
    public void IncreaseScore(int amount)
    {
        score += amount;
        scoreText.text = "Score: " + score;
    }

    private void OnTriggerEnter(Collider other)
    {
        // check if the object that entered the trigger is one of the table objects
        if (other.gameObject.tag == "Grabbable")
        {
             other.gameObject.transform.position =  new Vector3(360.5f, 1.5f, 323.88f);
             IncreaseScore(1);
        }
    }
}
