Student IDs:
- Christian Riisberg: srw812
- Jonas Larsen: hrl495
- Sebastian Krogh-Walker: flc564

How to run:
- pull the code
- open project in Unity Hub
- Go to Build Settings and add scene
- Build and Run

Alternatively:
- use the build.apk application from the Build/ folder
